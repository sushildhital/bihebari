//
//  UserDefaultManager.swift
//  mDabali
//
//  Created by Prakash Shahi on 7/7/20.
//  Copyright © 2020 info. All rights reserved.
//

import Foundation
import UIKit

enum StorageType {
    case userDefaults
    case fileSystem
}

class UserDefaultManager {
    
    static let shared = UserDefaultManager()
    
    private let userDefault = UserDefaults.standard
    
    //UserDefault Constants
    
    private let IMAGE_PATH = "IMAGEPATH"
    private let CASTE = "SELECTED_CASTE"
    private let GENDER = "SELECTED_GENDER"
    private let MIN_AGE = "SELECTED_MIN_AGE"
    private let MAX_AGE = "SELECTED_MAX_AGE"
    private let MARITAL_STATUS = "SELECTED_MARITAL_STATUS"
    private let ACCESS_TOKEN = "ACCESS_TOKEN"
    private let IS_VERIFIED = "IS_VERIFIED"
    private let USER_MODEL = "USER_MODEL"
    private let MOBILE_NOTIFICATION_ID = "MOBILE_NOTIFICATION_ID"
    
    var mobileNotificationID : String {
            get {
                return userDefault.string(forKey: MOBILE_NOTIFICATION_ID) ?? ""
            }set {
                userDefault.set(newValue, forKey: MOBILE_NOTIFICATION_ID)
            }
        }


    var accessToken : String? {
        get {
            return userDefault.string(forKey: ACCESS_TOKEN)
        }set {
            userDefault.set(newValue, forKey: ACCESS_TOKEN)
        }
    }
    
    var isVerified : Bool? {
        get {
            return userDefault.bool(forKey: IS_VERIFIED)
        }set {
            userDefault.set(newValue, forKey: IS_VERIFIED)
        }
    }
    
    var redirectToBuild : Bool? {
        get {
            return userDefault.bool(forKey: IS_VERIFIED)
        }set {
            userDefault.set(newValue, forKey: IS_VERIFIED)
        }
    }
    
    var imagePath : String? {
        get {
            return userDefault.string(forKey: IMAGE_PATH)
        }set {
            userDefault.set(newValue, forKey: IMAGE_PATH)
        }
    }
    
    var selectedGender : String? {
        get {
            return userDefault.string(forKey: GENDER)
        }set {
            userDefault.set(newValue, forKey: GENDER)
        }
    }
    
    var selectedCaste : String? {
        get {
            return userDefault.string(forKey: CASTE)
        }set {
            userDefault.set(newValue, forKey: CASTE)
        }
    }
    
    var selectedMinAge : String? {
        get {
            return userDefault.string(forKey: MIN_AGE)
        }set {
            userDefault.set(newValue, forKey: MIN_AGE)
        }
    }
    
    var selectedMaxAge : String? {
        get {
            return userDefault.string(forKey: MAX_AGE)
        }set {
            userDefault.set(newValue, forKey: MAX_AGE)
        }
    }
    
    var selectedMaritalStatus : String? {
        get {
            return userDefault.string(forKey: MARITAL_STATUS)
        }set {
            userDefault.set(newValue, forKey: MARITAL_STATUS)
        }
    }
    
    var userDetailsModel : LoginUserModal? {
           get {
               if let model: LoginUserModal = getModel(key: USER_MODEL) {
                   return model
               }
               return nil
           }set {
               saveModel(model: newValue, key: USER_MODEL)
           }
       }

    var isLoggedIn: Bool {
        return self.accessToken != nil
    }
    
    func saveNewStory(story: LoginStory_image?) {
        guard let story = story else {return}
        let newModel = userDetailsModel
        newModel?.story_image?.append(story)
        userDetailsModel = newModel
    }
    
    func saveNewPhoto(photo: Other_image?) {
        guard let photo = photo else {return}
        let newModel = userDetailsModel
        newModel?.other_image?.append(photo)
        userDetailsModel = newModel
    }
    
    func handleLogout() {
        accessToken = nil
        userDetailsModel = nil
    }
    
    func handleCast(){
        selectedGender = nil
    }
    
    //MARK:- Saving and retrieving model
    private func saveModel<T: Codable> (model : T, key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(model) {
            userDefault.set(encoded, forKey: key)
        }
    }

    private func getModel<T: Codable> (key: String) -> T? {
        if let model = userDefault.object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            if let model = try? decoder.decode(T.self, from: model) {
                return model
            }
        }
        return nil
    }
    
}
