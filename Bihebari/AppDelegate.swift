//
//  AppDelegate.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseMessaging
import Firebase
import Purchases
import netfox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    
    let userID = "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)"
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IAPManager.shared.fetchProducts()
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        startVC()
        notificationCenter.delegate = self
        NFX.sharedInstance().start()
        Purchases.debugLogsEnabled = true
        Purchases.configure(withAPIKey: "AkYgZkJQRQgCCxUVQPFWijYDmhwQpKsx")
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        return true
    }
    
    func setupRootController(){
        let vc = BuildProfileVC.instantiate(fromAppStoryboard: .BuildProfile)
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = nav
    }
    
    func startVC() {
        if self.window == nil{
            self.window = UIWindow(frame: UIScreen.main.bounds)
        }
        self.appCoordinator = AppCoordinator.init(w: self.window!)
        self.appCoordinator.start()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let token = Messaging.messaging().fcmToken
        UserDefaultManager.shared.mobileNotificationID = token ?? ""
        //        print("Fcm token\(String(describing: token))")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    //        print("executed")
    //    }
}
