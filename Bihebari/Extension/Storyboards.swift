//
//  Storyboards.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import Foundation
import UIKit

enum AppStoryboard : String {
    case Login, Register, ForgotPassword, UpdatePassword, FindSomeone, Home, Notification, PaymentOption, Matches, Unlock, MatchInfo, Settings, Gallery, BuildProfile, MaritalStatus, Education, Kundali, UploadPhoto, Profile, ProfileSettings, AppSettings, ViewProfile, PartnerPreferences, NotificationMatchedInfo, PhoneVerification, OTPVerification, UpgradePackage, NoProfileAvailable, MyInterests, InterestedToMe, YourPlan, BlockedList, ReportProfile, ReportOther, UpdateAccountDetail
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard")
        }
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }

}

extension UIViewController {

    class var storyboardID : String {
        return "\(self)"
    }
 
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
}


extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

