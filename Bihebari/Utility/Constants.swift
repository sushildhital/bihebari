//
//  Constants.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import Foundation
import UIKit

/***************************************************************************************************/
/*Sekeleton For ViewController*/

//// MARK: -
//// MARK: Private Utility Methods
//
//fileprivate func configureView(){
//
//}
//
//fileprivate func loadData() {
//
//}
//
//fileprivate func isValid()->Bool {
//    return true
//}
//
//// MARK: -
//// MARK: Public Utility Methods
//
//
//// MARK: -
//// MARK: IBAction Methods
//
//
//// MARK: -
//// MARK: Object Methods
//
//override func awakeFromNib() {
//    super.awakeFromNib()
//}
//
//override func loadView() {
//    super.loadView()
//}
//
//override func viewDidLayoutSubviews() {
//    super.viewDidLayoutSubviews()
//}
//
//override func viewDidLoad() {
//    super.viewDidLoad()
//}
//
//override func viewWillAppear(_ animated: Bool) {
//    super.viewWillAppear(animated)
//}
//
//override func viewDidAppear(_ animated: Bool) {
//    super.viewDidAppear(animated);
//}
//
//override func viewWillDisappear(_ animated: Bool) {
//    super.viewWillDisappear(animated)
//}
//
//override func viewDidDisappear(_ animated: Bool) {
//    super.viewDidDisappear(animated)
//}
//
//override func didReceiveMemoryWarning() {
//    super.didReceiveMemoryWarning()
//    // Dispose of any resources that can be recreated.
//}
//
//override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//}
//
//// MARK: -
//// MARK: Delegate Methods

//************************************************************************************************
//
// MARK:-
// MARK:- Private Utility Methods
//
// MARK:-
// MARK:- Public Utility Methods
//
// MARK:-
// MARK:- IBAction Methods
//
// MARK:-
// MARK:- Object Methods
//
// MARK:-
// MARK:- Delegate Methods

// API Links

class Constants{
//    public static let BASE_URL                  = "http://54.251.8.37"
    public static let BASE_URL                  = "https://bihebari.com"
    
    public static let Login                     = "/api/v1/login"
    public static let Register                  = "/api/v1/register"
    public static let findSomeone               = "/api/v1/serach/constant"
    public static let Home                      = "/api/v1/search"
    public static let Country                   = "/api/v1/country"
    public static let GeneralProfile            = "/api/v1/general/profile/constant"
    public static let EducationProfile          = "/api/v1/education/career/constant"
    public static let Kundali                   = "/api/v1/kundali/constant"
    public static let MyProfile                 = "/api/v1/my-profile"
    public static let UpdateGeneral             = "/api/v1/update/general"
    public static let UpdateEducation           = "/api/v1/update/education/career"
    public static let UpdateKundali             = "/api/v1/update/kundali"
    public static let interested_in_accept      = "/api/v1/interest/"
    public static let interested_in_reject      = "/api/v1/interest/"
    public static let interested_user_profile   = "/api/v1/user-profile/"
    public static let paymentOption             = "/api/v1/payment/methods"
    public static let upgradePackage            = "/api/v1/packages"
    public static let markShortlist             = "/api/v1/add/to/shortlist"
    public static let verifyYourself            = "/api/v1/verify/yourself"
    public static let verifyOTP                 = "/api/v1/verify/otp"
    public static let getShortlist              = "/api/v1/shortlists"
    public static let changePassword            = "/api/v1/change/password"
    public static let addStory                  = "/api/v1/story/post"
    public static let addPhoto                  = "/api/v1/update/other/photo"
    public static let markInterested            = "/api/v1/i-am-interested"
    public static let removeFromShortlist       = "/api/v1/remove/"
    public static let getPartnerPreferences     = "/api/v1/parnter/preference/constant"
    public static let updatePartnerPreferences  = "/api/v1/update/parner/preference"
    public static let deleteAccount             = "/api/v1/delete/account"
    public static let blockUser                 = "/api/v1/"
    public static let unblockUser               = "/api/v1/"
    public static let getBlockedList            = "/api/v1/blocked/list"
    public static let notificationToken         = "/api/v1/notification/token/update"
    public static let cashOnOfficePayment       = "/api/v1/package/payment"
    public static let hideStory                 = "/api/v1/block/story"
    public static let forgotPassword            = "/api/v1/password/email"
    public static let addUplaodPic              = "/api/v1/update/profile/photo"
    public static let reportProfile             = "/api/v1/report"
    public static let removeProfilePhoto        = "/api/v1/remove/"
    public static let removeStoryPhoto          = "/api/v1/remove/"
    public static let updateDetail              = "/api/v1/update/detail"
    public static let applePay                  = "/api/v1/initiate/apple/payment"
}

//NO INTERNET MESSAGE
let NO_INTERNET_TITLE = "Whoops!"
let NO_INTERNET_MSG = "Your internet connection appears to be offline. Connect to the internet and try again."
