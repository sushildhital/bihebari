//
//  Constants.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit 
import SwiftMessages
import SocketIO

class MyTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    static let shared = MyTabBarController()
    
    var navController : UINavigationController?

    lazy var manager = SocketManager(socketURL:URL(string: Constants.BASE_URL + "?id=\(userID)")!, config: [.log(true), .forceNew(true)])
    let userID = "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)"
    var interestedData: [Interested_to_me]?
    var chatList = [ChatListModel]()

    
    lazy var tabBarItemControllers: [UIViewController] = {
        
        let homeVC = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let notificationVC = UIStoryboard(name: "Notification", bundle: nil).instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        let messageVC = UIStoryboard(name: "Message", bundle: nil).instantiateViewController(withIdentifier: "ChatListViewController") as! ChatListViewController
        //  let navController = UINavigationController(rootViewController: messageVC)
        let profileVC = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        homeVC.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "imgHome"), selectedImage: UIImage(named: "imgHomeS"))
        notificationVC.tabBarItem = UITabBarItem(title: "Notification", image: UIImage(named: "imgNotification"), selectedImage: UIImage(named: "imgNotification"))
        messageVC.tabBarItem = UITabBarItem(title: "Message", image: UIImage(named: "imgMessage"), selectedImage: UIImage(named: "imgMessageS"))
        profileVC.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "imgProfile"), selectedImage: UIImage(named: "imgProfileS"))
        
        if interestedData?.count == 0 {
            notificationVC.tabBarItem.badgeValue = nil
        }else{
            notificationVC.tabBarItem.badgeColor = UIColor(named: "Bihebari-Red")
            notificationVC.tabBarItem.badgeValue = "\(interestedData?.count ?? 0)"
        }
        
        messageVC.tabBarItem.badgeColor = UIColor(named: "Bihebari-Red")
        

        return [homeVC, notificationVC, messageVC, profileVC]
          }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupSocket()
        self.delegate = self
        self.interestedData = UserDefaultManager.shared.userDetailsModel?.interested_to_me?.filter{ $0.status == "approach" }
        setViewControllers(tabBarItemControllers, animated: true)
        tabBar.tintColor = .systemRed
        tabBar.isTranslucent = true
//        tabBar.backgroundImage = UIImage()
//        tabBar.shadowImage = UIImage()
//        tabBar.barTintColor = .clear
//        tabBar.backgroundColor = .clear
//        tabBar.layer.backgroundColor = UIColor.clear.cgColor
//        let blurEffect = UIBlurEffect(style: .regular)
//        let blurView = UIVisualEffectView(effect: blurEffect)
//        blurView.frame = tabBar.bounds
//        blurView.autoresizingMask = .flexibleWidth
//        tabBar.insertSubview(blurView, at: 0)
    }
//    func setupSocket() {
//
//            Spinner.start()
//            let socket = manager.defaultSocket
//            socket.on(clientEvent: .connect) {data, ack in
//                print("socket connected")
//                socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
//            }
//            socket.on(clientEvent: .disconnect) {data, ack in
//                print("socket disconnected")
//                socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
//            }
//            socket.on("chatListRes") { data, ack in
//                print("*********************** DATA RECEIVED ***************************")
//                socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
//                self.decodeResponse(data: data)
//                socket.disconnect()
//                Spinner.stop()
//            }
//            socket.connect()
//
//    }
//    func decodeResponse(data: [Any]) {
//        do {
//            let jsonData = try JSONSerialization.data(withJSONObject: data)
//            let decoder = JSONDecoder()
//            let chatList = try decoder.decode([ChatResponseModel].self, from: jsonData)
//
//            if let model = chatList[0].chatList {
//                self.chatList = model
//                let msgCount = model.reduce(0) {$0 + $1.msgCount!}
//
//                let vcs = tabBarController?.viewControllers
//                if msgCount == 0 {
//                    vcs?[2].tabBarItem.badgeValue = nil
//                }else{
//                    vcs?[2].tabBarItem.badgeColor = UIColor(named: "Bihebari-Red")
//                    vcs?[2].tabBarItem.badgeValue = "\(msgCount)"
//                }
//            }
//        }catch{
//            print(error)
//        }
//    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController)!
        if selectedIndex == 0 {
            ViewHandler.shared.isFromShortList = false
            ViewHandler.shared.isFromMatchesVC = false
            ViewHandler.shared.isFromMyInterests = false
            ViewHandler.shared.isFromInterestedToMe = false
        }
    }
}


