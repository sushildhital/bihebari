//
//  Helper.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/20/21.
//

import Foundation
import UIKit

class Helper {
    class func getDashboardWithNavBar() -> UINavigationController {
        let tabBarController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MyTabBarController")
        let navigationController = UINavigationController(rootViewController: tabBarController)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        return navigationController
    }
    
}
