//
//  AppCoordinator.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/16/21.
//

import Foundation
import UIKit

protocol Coordinator: class {
    func start()
}

let IS_LOGGED_IN_USER = "IS_LOGGED_IN_USER"
let IS_CAST_SELECTED = "IS_CAST_SELECTED"
let IS_MARITAL_SELECTED = "IS_MARITAL_SELECTED"

class AppCoordinator: Coordinator {
    
    let window: UIWindow!
    
    init(w: UIWindow) {
        self.window = w
    }
    
    func start() {
        let isLoggedInUser = UserDefaults.standard.bool(forKey: IS_LOGGED_IN_USER)
        
        if isLoggedInUser{
            
            let isCastCheked = UserDefaults.standard.bool(forKey: IS_CAST_SELECTED)
            let isMaritalChecked = UserDefaults.standard.bool(forKey: IS_MARITAL_SELECTED)
            
            if UserDefaultManager.shared.userDetailsModel?.otp_status == "active" {
                if UserDefaultManager.shared.redirectToBuild == true {
                    self.moveToBuildProfile()
                }else {
                    if isCastCheked && isMaritalChecked{
                        self.moveToHome()
                    }else{
                        self.findSomeOne()
                    }
                }
            }else {
                self.moveToOTP()
            }
        } else {
            self.moveToLogin()
        }
    }
    
    func moveToLogin() {
        let vc = LoginVC.instantiate(fromAppStoryboard: .Login)
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = nav
        
    }
    
    func findSomeOne(){
        let vc = FindSomeoneVC.instantiate(fromAppStoryboard: .FindSomeone)
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = nav
    }
    
    func moveToHome(){
        let resultViewController = Helper.getDashboardWithNavBar()
        window.rootViewController = resultViewController
        window.makeKeyAndVisible()
    }
    func moveToOTP(){
        let vc = PhoneVerificationVC.instantiate(fromAppStoryboard: .PhoneVerification)
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = nav
    }
    func moveToBuildProfile(){
        let vc = BuildProfileVC.instantiate(fromAppStoryboard: .BuildProfile)
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = nav
    }
    
}
