//
//  ViewHandler.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/9/21.
//

import Foundation

class ViewHandler: NSObject {
    static let shared = ViewHandler()
    var isFromMatchesVC = false
    var isFromShortList = false
    var isFromMyInterests = false
    var isFromInterestedToMe = false
}

class BuildProfileHandler: NSObject {
    static let shared = BuildProfileHandler()
    var isFromBuildProfile = false
    var isFromEditProfile = false
}

class KundaliHandleAfterLogin: NSObject {
    static let shared = KundaliHandleAfterLogin()
    var isFromLogin = false
    var isFromKundali = false
    var isFromProfileEdit = false
}
