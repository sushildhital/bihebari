//
//  ReportOtherVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/15/21.
//

import UIKit
import IBAnimatable

class ReportOtherVC: UIViewController {
    
    var reportingData : UserMoreDetailsModel?
    var reportingDataID = 0
    @IBOutlet weak var txtReason: AnimatableTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("INTERESTED DATA User Code:-\(reportingData?.data?.user?.user_code ?? "")")
        print("INTERESTED DATA USER ID:-\(reportingData?.data?.user?.id ?? 0)")
        print("getUserID:-\(reportingDataID)")
    }
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        reqReportProfile()
    }
}

extension ReportOtherVC {
    func reqReportProfile() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : String] = ["user_code": self.reportingData?.data?.user?.user_code ?? "", "id": "\(reportingDataID)", "reason": self.txtReason.text]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.reportProfile(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<ReportProfileModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Profile Reported Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.dismiss(animated: true, completion: nil)
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}
