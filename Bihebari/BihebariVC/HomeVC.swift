//
//  HomeVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import FittedSheets
import SDWebImage
import IBAnimatable
import SocketIO


class HomeVC: UIViewController{
    

    @IBOutlet weak var collectionHome: UICollectionView!
    @IBOutlet weak var btnUpgradeNow: AnimatableButton!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var imgArrowUp: UIImageView!
    @IBOutlet weak var lblSelect: UILabel!
    @IBOutlet weak var searchHome: UISearchBar!
    
    var searchedUser = [SearchProfile]()
    var searching = false
    
    var chatListModel = [ChatListModel]()
    let userID = "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)"
    lazy var manager = SocketManager(socketURL:URL(string: Constants.BASE_URL + "?id=\(userID)")!, config: [.log(true), .forceNew(true)])
    var isSocketConnected = false
    
    var loginModel: LoginResponseModal?
    var isBlurView = true
    var responseArray : HomeModal?
    var searchedArray: [SearchProfile]?
    var searchArray : [SearchProfile]?
    var minAge = ""
    var maxAge = ""
    var caste = ""
    var gender = ""
    var maritalStatus = ""
    
    private let refreshControl = UIRefreshControl()
    
    let NUMBER_OF_COLUMN : CGFloat = 2
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 0
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnFilterAction(_ sender: Any) {
        let vc = FindSomeoneVC.instantiate(fromAppStoryboard: .FindSomeone)
        UserDefaultManager.shared.handleCast()
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnUpgradeNowAction(_ sender: Any) {
        let vc = UpgradePackageVC.instantiate(fromAppStoryboard: .UpgradePackage)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        home()
        notificationCentreCalled()
        collectionHome.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        refreshCollectionView()
        checkPackageValid()
//        if !isSocketConnected{
            setupSocket()
//        }
    }

    
    func refreshCollectionView(){
        if #available(iOS 10.0, *) {
            collectionHome.refreshControl = refreshControl
        } else {
            collectionHome.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor(named: "Bihebari-Red")
    }
    
    @objc private func refreshHomeData(_ sender: Any) {
        reloadHomeDataOnPullDown()
    }
    
    func checkPackageValid() {
        let pkgStatus = UserDefaultManager.shared.userDetailsModel?.package
        if pkgStatus == nil {
            btnUpgradeNow.isHidden = false
            isBlurView = true
        }else {
            if pkgStatus?.status == 0 {
                btnUpgradeNow.isHidden = false
                isBlurView = true
            }else {
                isBlurView = false
                btnUpgradeNow.isHidden = true
            }
        }
    }
    
    func notificationCentreCalled(){
        NotificationCenter.default.addObserver(self, selector: #selector(isCheckBlurView), name: NSNotification.Name("PaymentSuccess"), object: nil)
    }
    
    @objc func isCheckBlurView(){
        btnUpgradeNow.isHidden = true
        isBlurView = false
        collectionHome.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imgArrowUp.transform = CGAffineTransform(translationX: 0, y: -8)
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: [.repeat,.autoreverse], animations: {
                    self.imgArrowUp.transform = CGAffineTransform.identity
                }, completion: nil)
//        let animation = CABasicAnimation(keyPath: "position")
//        animation.duration = 0.5
//        animation.repeatCount = 10
//        animation.autoreverses = true
//        animation.fromValue = NSValue(cgPoint: CGPoint(x: imgArrowUp.center.x , y: imgArrowUp.center.y - 5))
//        animation.toValue = NSValue(cgPoint: CGPoint(x: imgArrowUp.center.x, y: imgArrowUp.center.y + 5))
//        imgArrowUp.layer.add(animation, forKey: "position")
//
//        let navigationBar = self.navigationController?.navigationBar
//        navigationBar?.setBackgroundImage(UIImage(), for: .default)
//        navigationBar?.shadowImage = UIImage()
//        navigationBar?.isTranslucent = true
        navigationController?.navigationBar.isHidden = true
        reloadHomeData()
    }
    
    func setupSocket() {
        let socket = manager.defaultSocket
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
        }
        socket.on(clientEvent: .disconnect) {data, ack in
            print("socket disconnected")
            socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
        }
        socket.on("chatListRes") { data, ack in
            print("*********************** DATA RECEIVED IN HOME ***************************")
            socket.off("chatListRes")
            self.isSocketConnected = true
            socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
            self.decodeResponse(data: data)
        }
        socket.connect()
    }
    func decodeResponse(data: [Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data)
            let decoder = JSONDecoder()
            let chatList = try decoder.decode([ChatResponseModel].self, from: jsonData)

            if let model = chatList[0].chatList {
                self.chatListModel = model
                
                let msgCount = chatListModel.reduce(0) {$0 + $1.msgCount!}
                
                let vcs = tabBarController?.viewControllers
                if msgCount == 0 {
                    vcs?[2].tabBarItem.badgeValue = nil
                }else{
                    vcs?[2].tabBarItem.badgeColor = UIColor(named: "Bihebari-Red")
                    vcs?[2].tabBarItem.badgeValue = "\(msgCount)"
                }
            }
        }catch{
            print(error)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        let socket = manager.defaultSocket
        socket.disconnect()
//        isSocketConnected = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching{
            return searchedUser.count
        }else{
            return searchArray?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        if searching{
            cell.lblName.text = searchedUser[indexPath.row].user_code
            cell.lblDate.text = "\(searchedUser[indexPath.row].age ?? 0) Years"
            cell.lblLocation.text = searchedUser[indexPath.row].address
            
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (searchedUser[indexPath.row].profile_image ?? "")
            
            if searchedUser[indexPath.row].profile_image == nil{
                cell.imgProfile.image = UIImage(named: "imgNoImage")
            }else{
                cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: nil, options: [.avoidAutoSetImage], completed: { image, _, _, _ in
                    cell.imgProfile.image =  self.isBlurView ? image?.blurImage(radius: 50) : image
                })
            }
          
            if searchArray?[indexPath.item].online_status == "N"{
                cell.imgStatus.image = UIImage(named: "imgNotActive")
            }else if searchArray?[indexPath.item].online_status == "Y"{
                cell.imgStatus.image = UIImage(named: "imgActive")
            }
            
        }else{
            cell.lblName.text = searchArray?[indexPath.row].user_code
            cell.lblDate.text = "\(searchArray?[indexPath.row].age ?? 0) Years"
            cell.lblLocation.text = searchArray?[indexPath.row].address
            
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (searchArray?[indexPath.row].profile_image ?? "")
            
            if searchArray?[indexPath.row].profile_image == nil{
                cell.imgProfile.image = UIImage(named: "imgNoImage")
            }else{
                cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: nil, options: [.avoidAutoSetImage], completed: { image, _, _, _ in
                    cell.imgProfile.image =  self.isBlurView ? image?.blurImage(radius: 50) : image
                })
            }
          
            if searchArray?[indexPath.item].online_status == "N"{
                cell.imgStatus.image = UIImage(named: "imgNotActive")
            }else if searchArray?[indexPath.item].online_status == "Y"{
                cell.imgStatus.image = UIImage(named: "imgActive")
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemWidth = (collectionViewWidth - LEFT_INSET - RIGHT_INSET - (INTER_ITEM_INSET * (NUMBER_OF_COLUMN - 1))) / NUMBER_OF_COLUMN
        let itemHeight : CGFloat = 260
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: TOP_INSET, left: LEFT_INSET, bottom: BOTTOM_INSET, right: RIGHT_INSET)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSET
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isBlurView {
            moveToPaymentData()
        }else{
            if searching{
                moveToProfileData(data: searchedUser, indexPath: indexPath)
            }else{
                moveToProfileData(data: searchArray ?? [], indexPath: indexPath)
            }
        }
    }
    
    func moveToProfileData(data: [SearchProfile], indexPath: IndexPath){
        let vc = MatchesVC.instantiate(fromAppStoryboard: .Matches)
        vc.searchedArray = data
        vc.indexPath = indexPath
        ViewHandler.shared.isFromShortList = false
        ViewHandler.shared.isFromMatchesVC = true
        ViewHandler.shared.isFromMyInterests = false
        ViewHandler.shared.isFromInterestedToMe = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func moveToPaymentData(){
        let vc = UpgradePackageVC.instantiate(fromAppStoryboard: .UpgradePackage)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeVC {
    func home() {
        if Reachability.isConnectedToNetwork(){
            let params : [String  : String] =
                [
                    "gender": UserDefaultManager.shared.selectedGender ?? "",
                    "age_from": UserDefaultManager.shared.selectedMinAge ?? "",
                    "age_to": UserDefaultManager.shared.selectedMaxAge ?? "",
                    "caste" : UserDefaultManager.shared.selectedCaste ?? "",
                    "marital_status" : UserDefaultManager.shared.selectedMaritalStatus ?? ""
                ]
            self.viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.Home(body: params).request, objectType: .general) { [weak self] (result: Result<SearchProfileList?, APIError>) in
                UIView.transition(with: self!.viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self?.viewActivity.alpha = 0
                })
                switch result {
                case .success(let data):
                    if data?.data?.profileList?.count == 0 {
                        self?.lblSelect.isHidden = false
                        self?.imgArrowUp.isHidden = false
                    }else{
                        self?.imgArrowUp.isHidden = true
                        self?.lblSelect.isHidden = true
                        self?.searchedArray = data?.data?.profileList?.filter{$0.user_code != UserDefaultManager.shared.userDetailsModel?.user_code }
                        self?.searchArray = data?.data?.profileList
                        self?.collectionHome.reloadData()
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
}

extension HomeVC {
    func reloadProfile() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            viewActivity.alpha = 1
            
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<LoginUserModal?, APIError>) in
                UIView.transition(with: self.viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.viewActivity.alpha = 0
                })
                switch result {
                
                case .success(let data):
        
                    UserDefaultManager.shared.userDetailsModel  = data
                    
                case .failure(let error):
                    UIView.transition(with: self.viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.viewActivity.alpha = 0
                    })
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            UIView.transition(with: viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.viewActivity.alpha = 0
            })
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension HomeVC {
    func reloadHomeData() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<ProfileSuccessModal?, APIError>) in
                UIView.transition(with: self.viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.viewActivity.alpha = 0
                })
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.userDetailsModel  = data?.data?.user
                    self.collectionHome.reloadData()
                    self.notificationCentreCalled()
                    self.checkPackageValid()
                    self.refreshControl.endRefreshing()
                case .failure(let error):
                    UIView.transition(with: self.viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.viewActivity.alpha = 0
                    })
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            UIView.transition(with: self.viewActivity, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.viewActivity.alpha = 0
            })
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func reloadHomeDataOnPullDown() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<ProfileSuccessModal?, APIError>) in
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.userDetailsModel  = data?.data?.user
                    self.collectionHome.reloadData()
                    self.notificationCentreCalled()
                    self.checkPackageValid()
                    self.refreshControl.endRefreshing()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension HomeVC: UISearchBarDelegate{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

       searchBar.setShowsCancelButton(true, animated: true)

    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {

        searchBar.setShowsCancelButton(false, animated: true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = nil
        searchBar.endEditing(true)
        collectionHome.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        searchedUser = searchArray?.filter({$0.user_code?.lowercased().prefix(searchText.count) ?? "" == searchText.lowercased() || $0.address?.lowercased().prefix(searchText.count) ?? "" == searchText.lowercased()}) ?? []
        searching = true
        self.collectionHome.reloadData()
    }
}
