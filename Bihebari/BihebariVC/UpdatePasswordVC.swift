//
//  UpdatePasswordVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/29/21.
//

import UIKit
import IBAnimatable

class UpdatePasswordVC: UIViewController {
    
    @IBOutlet weak var txtCurrentPassword: AnimatableTextField!
    @IBOutlet weak var txtNewPassword: AnimatableTextField!
    @IBOutlet weak var txtRetypeNewPassword: AnimatableTextField!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func isValid()->Bool {
        if txtCurrentPassword.text == ""{
            txtCurrentPassword.attributedPlaceholder = NSAttributedString(string: "Password required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtCurrentPassword.becomeFirstResponder()
            }
            return false
        }else{
            txtCurrentPassword.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtNewPassword.text == ""{
            txtNewPassword.attributedPlaceholder = NSAttributedString(string: "Password required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtNewPassword.becomeFirstResponder()
            }
            return false
        }else{
            txtNewPassword.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtRetypeNewPassword.text == ""{
            txtRetypeNewPassword.attributedPlaceholder = NSAttributedString(string: "Password required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtRetypeNewPassword.becomeFirstResponder()
            }
            return false
        }else{
            txtRetypeNewPassword.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        return true
    }
    
    fileprivate func loadData() {
        
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnUpdatePasswordAction(_ sender: Any) {
        if isValid(){
            if txtNewPassword.text! == txtRetypeNewPassword.text!{
                requestForChangePassword()
            }else{
                Util.showAlert(title: "Errow!", message: "Your password do not match.", view: self)
            }
        }else{
            print("FAILED...")
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension UpdatePasswordVC {
    func requestForChangePassword() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : Any] =
                [
                    "old": txtCurrentPassword.text!,
                    "password": txtNewPassword.text!,
                    "password_confirmation": txtRetypeNewPassword.text!
                ]
            
            Spinner.start()
            APIClient.shared.fetch(with: APIService.changePassword(body: params).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Password Updated Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.navigationController?.popViewController(animated: true)
                    }
                case .failure( _):
                    SwiftMessage.showBanner(title: "Error", message: "Error!", type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
}


