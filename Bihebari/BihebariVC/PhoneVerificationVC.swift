//
//  PhoneVerificationVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/17/21.
//

import UIKit
import IBAnimatable

class PhoneVerificationVC: UIViewController {
    
    var verifyPhoneModal: PhoneVerifyModal?
    
    @IBOutlet weak var txtPhoneNumber: AnimatableTextField!
    @IBOutlet weak var lblVerificationTitle: UILabel!
    @IBOutlet weak var lblOTPTitle: UILabel!
    @IBOutlet weak var lblEmailOrNumber: UILabel!
    
    var sendPhoneOTP: Bool = false
    var isRedirectToBuild : Bool?

    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnGetOTPAction(_ sender: Any) {
        verifyPhone()
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        txtPhoneNumber.isUserInteractionEnabled = false
            txtPhoneNumber.text = UserDefaultManager.shared.userDetailsModel?.phone
            lblVerificationTitle.text = "We will send you a one time password on your phone number."
            lblOTPTitle.text = "A 6 digit OTP will be sent to above number to verify your phone number"
            lblEmailOrNumber.text = "Phone Number"
            sendPhoneOTP = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    @IBAction func btnLogoutAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Bihebari", message: "Are You Sure You Want To Logout??", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    UserDefaults.standard.removeObject(forKey: IS_LOGGED_IN_USER)
                    let vc = LoginVC.instantiate(fromAppStoryboard: .Login)
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        let nav = UINavigationController(rootViewController: vc)
                        appDelegate.window?.rootViewController = nav
                    }
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self.present(alert, animated: true)
    }
    
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension PhoneVerificationVC {
    func verifyPhone() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.verifyYourself.request, objectType: .general) { [weak self] (result: Result<PhoneVerifyModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    self.verifyPhoneModal = data
                    SwiftMessage.showBanner(title: "Success", message: "OTP sent successfully.", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.verifyPhoneSuccess()
                    }
                    
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error", message: "\(error.localizedDescription)", type: .error)
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func verifyPhoneSuccess(){
        let vc = OTPVerificationVC.instantiate(fromAppStoryboard: .OTPVerification)
        vc.receivedPhone = txtPhoneNumber.text!
        vc.isPhoneOTP = true
        vc.encryptedData = verifyPhoneModal?.data?.encrypted_data ?? ""
        vc.isRedirectToBuild = isRedirectToBuild
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
