//
//  PaymentOptionVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/9/21.
//

import UIKit
import FittedSheets
import Purchases
import StoreKit

class PaymentOptionVC: UIViewController {
    
    var amount = ""
    var selectedPackageID = 0
    var productID = 0
    var selectedPackageName = ""
    var packagesAvailableForPurchase = [Purchases.Package]()
    var packageViewColors = [UIColor.init(hexString: "##44CBC6"),UIColor.init(hexString: "#273691"),UIColor.init(hexString: "#CC1C29")]

    @IBOutlet weak var stackView: UIStackView!
    
    let NUMBER_OF_COLUMN : CGFloat = 3
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 8
    let LINE_SPACE : CGFloat = 8
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationCentreCalled()
        
        switch selectedPackageID {
        case 6:
            productID = 0
        case 7:
            productID = 1
        case 8:
            productID = 2
        default:
            productID = 0
        }
        Spinner.start()
        
        // fetchng offerings from revenuecat and creating buttons
        Purchases.shared.offerings { (offerings, error) in
            if let offerings = offerings {
                // Display current offering with offerings.current
                let offer = offerings.current
                let packages = offer?.availablePackages
                guard packages != nil else {
                    return
                }
                
                let package = packages![self.productID]
                self.packagesAvailableForPurchase.append(package)
                let product = package.product
                let title = product.localizedTitle
                let price = product.price
                //create a button
                let button = UIButton(type: .system)
                button.tintColor = .white
                if self.productID == 0 {
                    button.backgroundColor = self.packageViewColors[0]
                } else if self.productID == 1 {
                    button.backgroundColor = self.packageViewColors[1]
                } else if self.productID == 2 {
                    button.backgroundColor = self.packageViewColors[2]
                }
                button.setTitle(title + " " + "$" + price.stringValue, for: .normal)
                button.layer.cornerRadius = 8
                button.tag = self.productID
                
                //add a tap handler
                button.addTarget(self, action: #selector(self.purchaseTapped(sender:)), for: .touchUpInside)
                
                //adding it to the view
                self.stackView.addArrangedSubview(button)
                Spinner.stop()
                
                //positioning and sizing
                let height = NSLayoutConstraint(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 90)
                button.addConstraint(height)
                let width = NSLayoutConstraint(item: button, attribute: .width, relatedBy: .equal, toItem: self.stackView, attribute: .width, multiplier: 1, constant: 0)
                self.stackView.addConstraint(width)
                
            }
        }
        
    }
    
    @IBAction func btnBasic(_ sender: UIButton) {
        
    }
    
    
    func verifyPayment(boughtProductId: Int) {
        
        let param = ["package_id": "\(boughtProductId)"]
        print(param)
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.applePay(body: param).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(_):
                    SwiftMessage.showBanner(title: "Success", message: "Payment successful.", type: .success)
                    self.navigationController?.popToRootViewController(animated: true)
                    Spinner.stop()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error", message: error.localizedDescription, type: .error)
                    Spinner.stop()
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    //tap handler
    @objc func purchaseTapped(sender:UIButton) {
        
        Spinner.start()
        print("TAG ======= \(sender.tag)")
        print("PRODUCT ID ====== \(productID)")
        
        let package = self.packagesAvailableForPurchase[0]
        Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, userCancelled) in
            
            
            
            switch self.productID{
            case 0 :
                print("Basic Package purchased")
                if userCancelled || (error != nil){
                    let alert = UIAlertController(title: "Bihebari", message: "Payment Cancelled", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    Spinner.stop()
//                    self.verifyPayment(boughtProductId: 6)
                }
//                else if (error != nil){
//                    let alert = UIAlertController(title: "Bihebari", message: "Payment Error", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
//                    self.present(alert, animated: true)
//                    Spinner.stop()
//                }
                else {
                    self.verifyPayment(boughtProductId: 6)
                }
                
            case 1 :
                print("Advacne Package purchased")
                if !userCancelled {
                    self.verifyPayment(boughtProductId: 7)
                }else {
                    let alert = UIAlertController(title: "Bihebari", message: "Payment Cancelled", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    Spinner.stop()
                }
            case 2 :
                print("VIP Package purchased")
                if !userCancelled {
                    self.verifyPayment(boughtProductId: 8)
                }else {
                    let alert = UIAlertController(title: "Bihebari", message: "Payment Cancelled", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                    Spinner.stop()
                }
            default:
                print("FAILED")
                Spinner.stop()
            }
            
        }
    }
    
    func notificationCentreCalled(){
        NotificationCenter.default.addObserver(self, selector: #selector(upgradePackage), name: NSNotification.Name("ProceedTapped"), object: nil)
    }
    
    @objc func upgradePackage(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let vc = UnlockVC.instantiate(fromAppStoryboard: .Unlock)
        vc.rupees = amount
        vc.packageID = productID
        let sheetController = SheetViewController(controller: vc, sizes: [.fixed(375)])
        sheetController.allowPullingPastMaxHeight = false
        sheetController.dismissOnOverlayTap = false
        sheetController.dismissOnPull = false
        self.present(sheetController, animated: true , completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}
