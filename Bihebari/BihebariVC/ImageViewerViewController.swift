//
//  ImageViewerViewController.swift
//  Bihebari
//
//  Created by Leoan Ranjit on 11/9/21.
//

import UIKit

class ImageViewerViewController: UIViewController {

    @IBOutlet weak var collectionImageViewer: UICollectionView!
    @IBOutlet weak var imgImage: UIImageView!
    var galleryImages = [String]()
    var imageURL =  ""
    var imageIndex = 0

    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionImageViewer.register(UINib(nibName: "ImageViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageViewCell")
        refresh()
    }
    override func viewWillAppear(_ animated: Bool) {
        refresh()
    }

    func refresh(){
        galleryImages = UserDefaultManager.shared.userDetailsModel?.other_image?.compactMap{$0.photo} ?? []
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ImageViewerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return galleryImages.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageViewCell", for: indexPath) as! ImageViewCell
            cell.imgImage.sd_setImage(with: URL(string: "\(Constants.BASE_URL + "/uploads/users/")" + galleryImages[indexPath.row]), completed: nil)
            return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemHeight : CGFloat = 650
        return CGSize(width: collectionViewWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSET
    }
//
//
}
