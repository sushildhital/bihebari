//
//  LoginVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import IBAnimatable

class LoginVC: UIViewController {
    
    @IBOutlet weak var txtEmailAddress: AnimatableTextField!
    @IBOutlet weak var txtPassword: AnimatableTextField!
    
    var notificationTokenUpdate : KhaltiModal?
    var isRedirectToBuild : Bool?
    
    fileprivate func configureView(){
        //commited
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtEmailAddress.text == ""{
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "Email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmailAddress.becomeFirstResponder()
            }
            return false
        }else if !Util.isValidEmail(testStr: txtEmailAddress.text!){
            txtEmailAddress.text = ""
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "Valid email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmailAddress.becomeFirstResponder()
            }
            return false
        }else{
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPassword.text == ""{
            txtPassword.attributedPlaceholder = NSAttributedString(string: "Password required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtPassword.becomeFirstResponder()
            }
            return false
        }else{
            txtPassword.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnLoginAction(_ sender: Any) {
        if Util.isConnectedToInternet(){
            if isValid(){
                requestForLogin()
            }
        }else{
            Util.showAlert(title: "Oops!", message: "No internet connection..", view: self)
        }
    }
    
    @IBAction func btnRegisterNowAction(_ sender: Any) {
        let vc = RegisterVC.instantiate(fromAppStoryboard: .Register)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        let vc = ForgotPasswordVC.instantiate(fromAppStoryboard: .ForgotPassword)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension LoginVC {
    func requestForLogin() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : String] = [
                "email": txtEmailAddress.text!,
                "password": txtPassword.text!]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.userLogin(body: params).request, objectType: .general) { [weak self] (result: Result<LoginResponseModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    UserDefaults.standard.set(true, forKey: IS_LOGGED_IN_USER)
                    
                    UserDefaultManager.shared.accessToken = data?.success?.token
                    UserDefaultManager.shared.imagePath = data?.success?.userImagePath
                    UserDefaultManager.shared.userDetailsModel  = data?.success?.user
                    UserDefaultManager.shared.isVerified = ((data?.success?.user?.otp_status ?? "") == "active" ? true : false)
                    self.isRedirectToBuild = data?.success?.redirect_to_build
                    self.notificationToken()
                    self.loginSucess()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func loginSucess(){
        if UserDefaultManager.shared.userDetailsModel?.otp_status == "inactive"{
            let vc = PhoneVerificationVC.instantiate(fromAppStoryboard: .PhoneVerification)
            self.navigationController?.pushViewController(vc, animated: true)
            vc.isRedirectToBuild = isRedirectToBuild
            UserDefaultManager.shared.redirectToBuild = isRedirectToBuild
        }else if isRedirectToBuild == true{
            let vc = BuildProfileVC.instantiate(fromAppStoryboard: .BuildProfile)
            KundaliHandleAfterLogin.shared.isFromLogin = true
            KundaliHandleAfterLogin.shared.isFromProfileEdit = false
            UserDefaultManager.shared.redirectToBuild = isRedirectToBuild
            self.navigationController?.pushViewController(vc, animated: true)
        }else if isRedirectToBuild == false && UserDefaultManager.shared.userDetailsModel?.otp_status == "inactive"{
            UserDefaultManager.shared.redirectToBuild = false
            let vc = FindSomeoneVC.instantiate(fromAppStoryboard: .FindSomeone)
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            UserDefaultManager.shared.redirectToBuild = false
            let vc = FindSomeoneVC.instantiate(fromAppStoryboard: .FindSomeone)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
    extension LoginVC {
        func notificationToken() {
            let params = ["notification_token": UserDefaultManager.shared.mobileNotificationID]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.notificationToken(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.notificationTokenUpdate = data
//                    print("notificationID \(params)")
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }
    }
    

