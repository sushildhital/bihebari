//
//  MatchInfoVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/12/21.
//

import UIKit
import FittedSheets
import SDWebImage
import IBAnimatable


protocol ReportDelegate {
    func reportDidTap(reason: String)
}

class MatchInfoVC: UIViewController {
    
    var addToShortlistModal: AddToShortlistsModal?
    var markInterestedModal: MarkInterestedModal?
    var searchProfileList : SearchProfile?
    var blockUserData : BlockUserModal?
    var isReadMoreTapped = false
    var counter = 0
    var dataReceived: UserMoreDetailsModel?
    var myInterestedData: UserMoreDetailsModel?
    var interestedToMeData: UserMoreDetailsModel?
    var selectedIndex = 0
    var receivedUserStatus = ""
    var getUserID = 0
    var reason = ""
    
    @IBOutlet weak var collectionInfo: UICollectionView!
    @IBOutlet weak var imgActive: UIImageView!
    @IBOutlet weak var lblActiveStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var lblMaritalStatus: UILabel!
    @IBOutlet weak var lblCurrentLivingAddress: UILabel!
    @IBOutlet weak var lblReligion: UILabel!
    @IBOutlet weak var lblCaste: UILabel!
    @IBOutlet weak var lblMotherTongue: UILabel!
    @IBOutlet weak var lblDiet: UILabel!
    @IBOutlet weak var lblHeightInGeneral: UILabel!
    @IBOutlet weak var lblEducationDegree: UILabel!
    @IBOutlet weak var lblEducationDegreeName: UILabel!
    @IBOutlet weak var lblProfessionInEducation: UILabel!
    @IBOutlet weak var lblSector: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet weak var viewReadMore: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblAboutUsInfo: UILabel!
    @IBOutlet weak var btnSettings: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func loadData() {
        if ViewHandler.shared.isFromMatchesVC {
            let matchedModal = dataReceived?.data?.user
            lblName.text = matchedModal?.user_code
            lblAge.text = "\(matchedModal?.profile_general?.current_age_year ?? 0)\(" Years")"
            lblHeight.text = "\(matchedModal?.profile_figure?.height_ft ?? 0)'\(matchedModal?.profile_figure?.height_in ?? 0)"
            lblAddress.text = "\(matchedModal?.get_permanent_address?.district ?? ""),\(matchedModal?.get_permanent_address?.country_name ?? "")"
            lblProfession.text = matchedModal?.profile_profession?.profession_name
            lblAboutUs.text = matchedModal?.profile_general?.about
            lblMaritalStatus.text = matchedModal?.profile_general?.marrital_status
            lblCurrentLivingAddress.text = "\(matchedModal?.get_permanent_address?.city ?? ""),\(matchedModal?.get_permanent_address?.country_name ?? "")"
            lblReligion.text = matchedModal?.profile_general?.religion?.name
            lblCaste.text = matchedModal?.profile_general?.caste?.name
            lblMotherTongue.text = matchedModal?.profile_general?.mother_tounge?.name
            lblHeightInGeneral.text = "\(matchedModal?.profile_figure?.height_ft ?? 0)'\(matchedModal?.profile_figure?.height_in ?? 0)"
            lblEducationDegree.text = matchedModal?.profile_general?.education?.name
            lblEducationDegreeName.text = matchedModal?.profile_general?.education_degree
            lblProfessionInEducation.text = matchedModal?.profile_profession?.profession_name
            lblSector.text = matchedModal?.profile_general?.sector?.name
            if matchedModal?.online == "Y"{
                imgActive.image = UIImage(named: "imgActive")
                lblActiveStatus.text = "Active Now"
            }else if matchedModal?.online == "N"{
                imgActive.image = UIImage(named: "imgNotActive")
                lblActiveStatus.text = "Recently Active"
            }
            if matchedModal?.profile_figure?.vegetarian == "yes" {
                lblDiet.text = "Veg"
            }else{
                lblDiet.text = "Non-Veg"
            }
        }else if ViewHandler.shared.isFromShortList {
            let shortlistModal = dataReceived?.data?.user
            lblName.text = shortlistModal?.user_code
            lblAge.text = "\(shortlistModal?.profile_general?.current_age_year ?? 0)\(" Years")"
            lblHeight.text = "\(shortlistModal?.profile_figure?.height_ft ?? 0)'\(shortlistModal?.profile_figure?.height_in ?? 0)"
            lblAddress.text = "\(shortlistModal?.get_permanent_address?.district ?? ""),\(shortlistModal?.get_permanent_address?.country_name ?? "")"
            lblProfession.text = shortlistModal?.profile_profession?.profession_name
            lblAboutUs.text = shortlistModal?.profile_general?.about
            lblMaritalStatus.text = shortlistModal?.profile_general?.marrital_status
            lblCurrentLivingAddress.text = "\(shortlistModal?.get_permanent_address?.city ?? ""),\(shortlistModal?.get_permanent_address?.country_name ?? "")"
            lblReligion.text = shortlistModal?.profile_general?.religion?.name
            lblCaste.text = shortlistModal?.profile_general?.caste?.name
            lblMotherTongue.text = shortlistModal?.profile_general?.mother_tounge?.name
            lblHeightInGeneral.text = "\(shortlistModal?.profile_figure?.height_ft ?? 0)'\(shortlistModal?.profile_figure?.height_in ?? 0)"
            lblEducationDegree.text = shortlistModal?.profile_general?.education?.name
            lblEducationDegreeName.text = shortlistModal?.profile_general?.education_degree
            lblProfessionInEducation.text = shortlistModal?.profile_profession?.profession_name
            lblSector.text = shortlistModal?.profile_general?.sector?.name
            if shortlistModal?.online == "Y"{
                imgActive.image = UIImage(named: "imgActive")
                lblActiveStatus.text = "Active Now"
            }else if shortlistModal?.online == "N"{
                imgActive.image = UIImage(named: "imgNotActive")
                lblActiveStatus.text = "Recently Active"
            }
            if shortlistModal?.profile_figure?.vegetarian == "yes" {
                lblDiet.text = "Veg"
            }else{
                lblDiet.text = "Non-Veg"
            }
        }else if ViewHandler.shared.isFromMyInterests{
            let myInterestmodal = myInterestedData?.data?.user
            lblName.text = myInterestmodal?.user_code
            lblAge.text = "\(myInterestmodal?.profile_general?.current_age_year ?? 0)\(" Years")"
            lblHeight.text = "\(myInterestmodal?.profile_figure?.height_ft ?? 0)'\(myInterestmodal?.profile_figure?.height_in ?? 0)"
            lblAddress.text = "\(myInterestmodal?.get_permanent_address?.district ?? ""),\(myInterestmodal?.get_permanent_address?.country_name ?? "")"
            lblProfession.text = myInterestmodal?.profile_profession?.profession_name
            lblAboutUs.text = myInterestmodal?.profile_general?.about
            lblMaritalStatus.text = myInterestmodal?.profile_general?.marrital_status
            lblCurrentLivingAddress.text = "\(myInterestmodal?.get_permanent_address?.district ?? ""),\(myInterestmodal?.get_permanent_address?.country_name ?? "")"
            lblReligion.text = myInterestmodal?.profile_general?.religion?.name
            lblCaste.text = myInterestmodal?.profile_general?.caste?.name
            lblMotherTongue.text = myInterestmodal?.profile_general?.mother_tounge?.name
            lblHeightInGeneral.text = "\(myInterestmodal?.profile_figure?.height_ft ?? 0)'\(myInterestmodal?.profile_figure?.height_in ?? 0)"
            lblEducationDegree.text = myInterestmodal?.profile_general?.education?.name
            lblEducationDegreeName.text = myInterestmodal?.profile_general?.education_degree
            lblProfessionInEducation.text = myInterestmodal?.profile_profession?.profession_name
            lblSector.text = myInterestmodal?.profile_general?.sector?.name
            if myInterestmodal?.online == "Y"{
                imgActive.image = UIImage(named: "imgActive")
                lblActiveStatus.text = "Active Now"
            }else if myInterestmodal?.online == "N"{
                imgActive.image = UIImage(named: "imgNotActive")
                lblActiveStatus.text = "Recently Active"
            }
            if myInterestmodal?.profile_figure?.vegetarian == "yes" {
                lblDiet.text = "Veg"
            }else{
                lblDiet.text = "Non-Veg"
            }
        }else if ViewHandler.shared.isFromInterestedToMe{
            let interestedToMeModal = interestedToMeData?.data?.user
            lblName.text = interestedToMeModal?.user_code
            lblAge.text = "\(interestedToMeModal?.profile_general?.current_age_year ?? 0)\(" Years")"
            lblHeight.text = "\(interestedToMeModal?.profile_figure?.height_ft ?? 0)'\(interestedToMeModal?.profile_figure?.height_in ?? 0)"
            lblAddress.text = "\(interestedToMeModal?.get_permanent_address?.district ?? ""),\(interestedToMeModal?.get_permanent_address?.country_name ?? "")"
            lblProfession.text = interestedToMeModal?.profile_profession?.profession_name
            lblAboutUs.text = interestedToMeModal?.profile_general?.about
            lblMaritalStatus.text = interestedToMeModal?.profile_general?.marrital_status
            lblCurrentLivingAddress.text = "\(interestedToMeModal?.get_permanent_address?.district ?? ""),\(interestedToMeModal?.get_permanent_address?.country_name ?? "")"
            lblReligion.text = interestedToMeModal?.profile_general?.religion?.name
            lblCaste.text = interestedToMeModal?.profile_general?.caste?.name
            lblMotherTongue.text = interestedToMeModal?.profile_general?.mother_tounge?.name
            lblHeightInGeneral.text = "\(interestedToMeModal?.profile_figure?.height_ft ?? 0)'\(interestedToMeModal?.profile_figure?.height_in ?? 0)"
            lblEducationDegree.text = interestedToMeModal?.profile_general?.education?.name
            lblEducationDegreeName.text = interestedToMeModal?.profile_general?.education_degree
            lblProfessionInEducation.text = interestedToMeModal?.profile_profession?.profession_name
            lblSector.text = interestedToMeModal?.profile_general?.sector?.name
            if interestedToMeModal?.online == "Y"{
                imgActive.image = UIImage(named: "imgActive")
                lblActiveStatus.text = "Active Now"
            }else if interestedToMeModal?.online == "N"{
                imgActive.image = UIImage(named: "imgNotActive")
                lblActiveStatus.text = "Recently Active"
            }
            if interestedToMeModal?.profile_figure?.vegetarian == "yes" {
                lblDiet.text = "Veg"
            }else{
                lblDiet.text = "Non-Veg"
            }
        }
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnMakeMyMatchAction(_ sender: Any) {
        let otpStatus = UserDefaultManager.shared.userDetailsModel?.otp_status
        let emailStatus = UserDefaultManager.shared.userDetailsModel?.email_otp_status
        if otpStatus == "active" && emailStatus == "active" {
            let alert = UIAlertController(title: "Match Request", message: "Are You Sure You Want To Send Match Request to \(dataReceived?.data?.user?.user_code ?? "")?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.markInterested()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }else{
            SwiftMessage.showBanner(title: "Permission Denied!", message: "You must be verified to send match request.", type: .error)
        }
    }
    
    @IBAction func btnReadMoreAction(_ sender: Any) {
        isReadMoreTapped = !isReadMoreTapped
        if isReadMoreTapped{
            UIView.animate(withDuration: 0.3) {
                self.lblAboutUs.numberOfLines = 0
                Util.dropShadow(view: self.viewReadMore, color: .clear, opacity: 1, offSet: CGSize(width: 0, height: -10), radius: 10, scale: true)
                self.btnReadMore.setTitle("Read Less", for: .normal)
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.lblAboutUs.numberOfLines = 4
                self.btnReadMore.setTitle("Read More", for: .normal)
                Util.dropShadow(view: self.viewReadMore, color: .white, opacity: 1, offSet: CGSize(width: 0, height: -10), radius: 10, scale: true)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnSettingsAction(_ sender: Any) {
        moveToSettingVC()
    }
    
    @IBAction func btnGalleryAction(_ sender: Any) {
        if ViewHandler.shared.isFromMatchesVC{
            moveToGalleryVC(data: self.dataReceived)
        }else if ViewHandler.shared.isFromShortList{
            moveToGalleryVC(data: self.dataReceived)
        }else if ViewHandler.shared.isFromMyInterests{
            moveToGalleryVC(data: self.myInterestedData)
        }else if ViewHandler.shared.isFromInterestedToMe{
            moveToGalleryVC(data: self.interestedToMeData)
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func moveToSettingVC(){
        if ViewHandler.shared.isFromMatchesVC{
            let vc = SettingsVC.instantiate(fromAppStoryboard: .Settings)
            let sheetController = SheetViewController(controller: vc, sizes: [.fixed(200)])
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: true , completion: nil)
        }else if ViewHandler.shared.isFromShortList{
            let vc = SettingsVC.instantiate(fromAppStoryboard: .Settings)
            let sheetController = SheetViewController(controller: vc, sizes: [.fixed(280)])
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: true , completion: nil)
        }else if ViewHandler.shared.isFromMyInterests{
            let vc = SettingsVC.instantiate(fromAppStoryboard: .Settings)
            let sheetController = SheetViewController(controller: vc, sizes: [.fixed(280)])
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: true , completion: nil)
        }else if ViewHandler.shared.isFromInterestedToMe{
            let vc = SettingsVC.instantiate(fromAppStoryboard: .Settings)
            let sheetController = SheetViewController(controller: vc, sizes: [.fixed(280)])
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: true , completion: nil)
        }
    }
    
    func moveToGalleryVC(data: UserMoreDetailsModel?){
        if ViewHandler.shared.isFromMatchesVC{
            let vc = GalleryVC.instantiate(fromAppStoryboard: .Gallery)
            vc.profileList = data
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ViewHandler.shared.isFromShortList{
            let vc = GalleryVC.instantiate(fromAppStoryboard: .Gallery)
            vc.profileList = data
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ViewHandler.shared.isFromMyInterests{
            let vc = GalleryVC.instantiate(fromAppStoryboard: .Gallery)
            vc.profileList = data
            self.navigationController?.pushViewController(vc, animated: true)
        }else if ViewHandler.shared.isFromInterestedToMe{
            let vc = GalleryVC.instantiate(fromAppStoryboard: .Gallery)
            vc.profileList = data
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        pageControl.numberOfPages = dataReceived?.data?.user?.other_image?.count ?? 0
        pageControl.currentPage = 0
        notificationCentreCalled()
        collectionInfo.register(UINib(nibName: "MatchInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MatchInfoCollectionViewCell")
    }
    
    func notificationCentreCalled(){
        NotificationCenter.default.addObserver(self, selector: #selector(addToShortlist), name: NSNotification.Name("AddToShortlistTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideStory), name: NSNotification.Name("HideStoryTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(shareProfile), name: NSNotification.Name("ShareProfileTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reportProfile), name: NSNotification.Name("ReportTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(blockProfile), name: NSNotification.Name("BlockTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(otherBtnTappped), name: NSNotification.Name(rawValue: "OthersBtnTapped"), object: nil)
    }
    
    @objc func addToShortlist(){
        let alert = UIAlertController(title: "Add to Shortlist?", message: "Do you want to add this user in your shortlist?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.addUserToShortlist()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc func hideStory(){
        let alert = UIAlertController(title: "Hide Your Story?", message: "Do you want to hide your story?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.hideYourStory()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc func shareProfile(){
        let alert = UIAlertController(title: "Share This Profile?", message: "Do you want to share this profile?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            let webUrl = "\(Constants.BASE_URL + "profile/detail/" + (self.dataReceived?.data?.user?.user_code ?? "") + "/active")"
            let name = self.dataReceived?.data?.user?.user_code
            let myWebsite = NSURL(string : webUrl)
            let shareAll = [name ?? "", myWebsite as Any] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc func reportProfile(){
        let vc = ReportProfileVC.instantiate(fromAppStoryboard: .ReportProfile)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @objc func otherBtnTappped() {
        let vc = ReportOtherVC.instantiate(fromAppStoryboard: .ReportOther)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.reportingData = self.interestedToMeData
        vc.reportingDataID = self.getUserID
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @objc func blockProfile(){
        if receivedUserStatus == "accept" {
            blockAlert()
        }else{
            SwiftMessage.showBanner(title: "Error", message: "You Cannot Block This User", type: .error)
        }
    }
    
    func blockAlert(){
        let alert = UIAlertController(title: "Block This Profile?", message: "Do you want to block this profile?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.blockUser(code: "\(self.interestedToMeData?.data?.user?.user_code ?? "")", id: self.interestedToMeData?.data?.user?.id ?? 0)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isReadMoreTapped = false
        lblAboutUs.numberOfLines = 4
        navigationController?.navigationBar.isHidden = true
        Util.dropShadow(view: viewReadMore, color: .white, opacity: 1, offSet: CGSize(width: 0, height: -10), radius: 10, scale: true)
        scrollView.setContentOffset(.zero, animated: true)
        navigationController?.navigationBar.isHidden = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(collectionInfo.contentOffset.x) / Int(collectionInfo.frame.width)
        counter = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
    
}

extension MatchInfoVC : ReportDelegate{
    func reportDidTap(reason: String) {
        if ViewHandler.shared.isFromInterestedToMe{
            self.reason = reason
            if receivedUserStatus == "accept"{
                reportAlert()
            }else{
                SwiftMessage.showBanner(title: "Error!!", message: "You Cannot Report This User.", type: .error)
            }
            
        }else{
            SwiftMessage.showBanner(title: "Error!!", message: "You Cannot Report This User.", type: .error)
        }
    }
    
    func reportAlert(){
        let alert = UIAlertController(title: "Report Profile?", message: "Do you want to report this profile?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.reqReportProfile()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}

extension MatchInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ViewHandler.shared.isFromMatchesVC{
            return dataReceived?.data?.user?.other_image?.count ?? 0
        }else if ViewHandler.shared.isFromShortList {
            return dataReceived?.data?.user?.other_image?.count ?? 0
        }else if ViewHandler.shared.isFromMyInterests {
            return myInterestedData?.data?.user?.other_image?.count ?? 0
        }else if ViewHandler.shared.isFromInterestedToMe {
            return interestedToMeData?.data?.user?.other_image?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchInfoCollectionViewCell", for: indexPath) as! MatchInfoCollectionViewCell
        if ViewHandler.shared.isFromMatchesVC{
            let url = Constants.BASE_URL + "/\(UserDefaultManager.shared.imagePath ?? "")" + (dataReceived?.data?.user?.other_image?[indexPath.row].photo ?? "")
            cell.imgInfo.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }else if ViewHandler.shared.isFromShortList {
            let url = Constants.BASE_URL + "/\(UserDefaultManager.shared.imagePath ?? "")" + (dataReceived?.data?.user?.other_image?[indexPath.row].photo ?? "")
            cell.imgInfo.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }else if ViewHandler.shared.isFromMyInterests {
            let url = Constants.BASE_URL + "/\(UserDefaultManager.shared.imagePath ?? "")" + (myInterestedData?.data?.user?.other_image?[indexPath.row].photo ?? "")
            cell.imgInfo.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }else if ViewHandler.shared.isFromInterestedToMe {
            let url = Constants.BASE_URL + "/\(UserDefaultManager.shared.imagePath ?? "")" + (interestedToMeData?.data?.user?.other_image?[indexPath.row].photo ?? "")
            cell.imgInfo.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionInfo.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}

extension MatchInfoVC {
    func addUserToShortlist() {
        if Reachability.isConnectedToNetwork(){
            let params = ["profile_code": dataReceived?.data?.user?.user_code ?? ""] as [String : Any]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            print("FROM MATCHINFO:\(params)")
            Spinner.start()
            APIClient.shared.fetch(with: APIService.markShortlist(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<AddToShortlistsModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    if data?.success == "error"{
                        SwiftMessage.showBanner(title: "Error", message: "User Already Added to Shortlist", type: .error)
                    }else{
                        SwiftMessage.showBanner(title: "Success", message: "Added to shortlist successfully.", type: .success)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.addToShortlistSuccess()
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func addToShortlistSuccess(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension MatchInfoVC {
    func markInterested() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            NetworkHandler.shared.markInterestedPost(profileId: dataReceived?.data?.user?.user_code ?? "") { status in
                Spinner.stop()
                if status == "error"{
                    SwiftMessage.showBanner(title: "Error", message: "Match Request Already Sent", type: .error)
                }else{
                    SwiftMessage.showBanner(title: "Success", message: "Match Request Send Successfully.", type: .success)
                }
            } failure: { Error in
                Spinner.stop()
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension MatchInfoVC{
    func blockUser(code: String, id: Int) {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.blockUser(header: header, profileID: getUserID, profileCode: code).request, objectType: .general) { [weak self] (result: Result<BlockUserModal?, APIError>) in
                print("getUserID: \(self?.getUserID ?? 0)")
                Spinner.stop()
                switch result {
                case .success(let data):
                    if data?.success == "error"{
                        SwiftMessage.showBanner(title: "Error", message: "User Already Blocked.", type: .error)
                    }else{
                        SwiftMessage.showBanner(title: "Success", message: "User Blocked Successfully.", type: .success)
                    }
                    self?.blockUserData = data
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension MatchInfoVC {
    func hideYourStory() {
        if Reachability.isConnectedToNetwork(){
            let params = ["user_code": dataReceived?.data?.user?.user_code ?? ""] as [String : Any]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.hideStory(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    if data?.success == "error"{
                        SwiftMessage.showBanner(title: "Error", message: "Story Already Hidden For This User.", type: .error)
                    }else{
                        SwiftMessage.showBanner(title: "Success", message: "Story Is Now Hidden For This User", type: .success)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.hideStorySuccess()
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func hideStorySuccess(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension MatchInfoVC {
    func reqReportProfile() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : String] = ["user_code": self.interestedToMeData?.data?.user?.user_code ?? "", "id": "\(getUserID)", "reason": self.reason]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.reportProfile(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<ReportProfileModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    if data?.success == "error"{
                        SwiftMessage.showBanner(title: "Error", message: "Profile Already Reported.", type: .error)
                    }else{
                        SwiftMessage.showBanner(title: "Success", message: "Profile Reported Successfully", type: .success)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}
