//
//  ViewProfileVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/14/21.
//

import UIKit
import IBAnimatable

class ViewProfileVC: UIViewController {
    
    var viewProfileArray: LoginUserModal?
    
    @IBOutlet weak var imgProfile: AnimatableImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMaritalStatus: UILabel!
    @IBOutlet weak var lblCurrentlyLiving: UILabel!
    @IBOutlet weak var lblReligion: UILabel!
    @IBOutlet weak var lblCaste: UILabel!
    @IBOutlet weak var lblMotherTongue: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblDiet: UILabel!
    @IBOutlet weak var lblEducationDegree: UILabel!
    @IBOutlet weak var lblEducationDegreeName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblSector: UILabel!
    @IBOutlet weak var lblAnnualIncome: UILabel!
    @IBOutlet weak var lblTimeOfBirth: UILabel!
    @IBOutlet weak var lblBirthPlace: UILabel!
    @IBOutlet weak var lblGotraName: UILabel!
    @IBOutlet weak var lblRashiName: UILabel!
    @IBOutlet weak var lblManglik: UILabel!
    @IBOutlet weak var lblAboutMe: UILabel!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: Button Actions
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEditPhotoAction(_ sender: Any) {
        let vc = UploadPhotoVC.instantiate(fromAppStoryboard: .UploadPhoto)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnUpdateGeneralAction(_ sender: Any) {
        let vc = BuildProfileVC.instantiate(fromAppStoryboard: .BuildProfile)
        BuildProfileHandler.shared.isFromEditProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnUpdateEducationAction(_ sender: Any) {
        let vc = EducationVC.instantiate(fromAppStoryboard: .Education)
        BuildProfileHandler.shared.isFromEditProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnUpdateKundaliAction(_ sender: Any) {
        let vc = KundaliVC.instantiate(fromAppStoryboard: .Kundali)
        KundaliHandleAfterLogin.shared.isFromProfileEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        updateUI()
        viewProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadViewProfileData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func updateUI(){
        lblFullName.text = UserDefaultManager.shared.userDetailsModel?.name
        lblEmail.text = UserDefaultManager.shared.userDetailsModel?.email
        lblContactNumber.text = UserDefaultManager.shared.userDetailsModel?.phone
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")"
        imgProfile.sd_setImage(with: URL(string: url + (UserDefaultManager.shared.userDetailsModel?.profile_image?.photo ?? "")), completed: nil)
        lblMaritalStatus.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.marrital_status
        lblCurrentlyLiving.text = UserDefaultManager.shared.userDetailsModel?.get_current_address?.district
        lblReligion.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.relegion_name
        lblCaste.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.caste_name
        lblMotherTongue.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.mother_tounge_name
        lblHeight.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_figure?.height_ft ?? 0)'\(UserDefaultManager.shared.userDetailsModel?.profile_figure?.height_in ?? 0)"
        if UserDefaultManager.shared.userDetailsModel?.profile_figure?.vegetarian == "yes" {
            lblDiet.text = "Veg"
        }else{
            lblDiet.text = "Non-Veg"
        }
        lblEducationDegree.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.education_degree
        lblEducationDegreeName.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.education_name
        lblProfession.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.profession_name
        lblSector.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.sectors_name
        lblAnnualIncome.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_profession?.monthly_income ?? 0)"
        lblTimeOfBirth.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.time_of_birth
        lblBirthPlace.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.birth_place
        lblGotraName.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.gotra_name
        lblRashiName.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.rashi_name
        lblManglik.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.is_mangalic
        lblAboutMe.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.about
    }
}

extension ViewProfileVC {
    func viewProfile() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) { [weak self] (result: Result<LoginUserModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    
                    self?.viewProfileArray = data
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension ViewProfileVC {
    func reloadViewProfileData() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<ProfileSuccessModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.userDetailsModel  = data?.data?.user
                    self.updateUI()
                case .failure(let error):
                    Spinner.stop()
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            Spinner.stop()
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}
