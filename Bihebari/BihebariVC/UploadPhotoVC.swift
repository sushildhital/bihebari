//
//  UploadPhotoVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/13/21.
//

import UIKit
import IBAnimatable

class UploadPhotoVC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    let picker = UIImagePickerController()
    
    @IBOutlet weak var imgUploadPhoto: AnimatableImageView!
    @IBOutlet weak var btnUploadPhoto: AnimatableButton!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUploadPhotoAction(_ sender: Any) {
        uploadImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    func uploadImage(){
        let actionSheet = UIAlertController(title: "Upload Photo", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Capture Photo", style: .default) { _ in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(camera)
        
        let gallery = UIAlertAction(title: "Select From Gallery", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(gallery)
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            imgUploadPhoto.image = img
            CheckImage(image: img)
            
        }
        
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            let choosenImage1 = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let data = choosenImage1.pngData()! as NSData
            data.write(toFile: localPath!, atomically: true)
        }
    }
    
    func CheckImage(image: UIImage){
        let alert = UIAlertController(title: "Upload", message: "Upload Profile Photo?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.requestPhotoUpdate(image: image)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
}

extension UploadPhotoVC {
    func requestPhotoUpdate(image: UIImage){
        if Reachability.isConnectedToNetwork(){
            guard let data = image.jpegData(compressionQuality: 0.8) else {return}
            let params : [MultipartMediaType] = [ MultipartMediaType(data: data, mimeType: "image/jpeg", keyName: "photo")]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.addUploadPhoto(files: params).request, objectType: .fromData) { (result: Result<AddStoryResponseModel?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.userDetailsModel?.photo  = data?.profileImage?.photo
                    SwiftMessage.showBanner(title: "Success", message: "Uploaded Successfully.", type: .success)
                    self.photoUploadSuccess()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func photoUploadSuccess(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension UploadPhotoVC {
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        alertController.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont(name: "Arial", size: 16)!,NSAttributedString.Key.foregroundColor : UIColor(hexString: "#F1F1F1")]), forKey: "attributedTitle")
        alertController.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont(name: "Arial", size: 12)!,NSAttributedString.Key.foregroundColor : UIColor(hexString: "#F1F1F1")]), forKey: "attributedMessage")
        alertController.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor(hexString: "#333333")
        alertController.view.tintColor = UIColor(hexString: "#FF8A00")
        self.present(alertController, animated: true, completion: nil)
    }
}
