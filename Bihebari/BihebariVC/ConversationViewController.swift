//
//  ConversationViewController.swift
//  Bihebari
//
//  Created by Newarpunk on 6/4/21.
//

import UIKit
import IBAnimatable

struct MessageList {
    var indicator: Int
    var msg: String
}

class ConversationViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextField: UITextView!
    @IBOutlet weak var satisfiedButton: AnimatableButton!
    @IBOutlet weak var notSatisfiedButton: AnimatableButton!
    @IBOutlet weak var textviewHeight: NSLayoutConstraint!
    @IBOutlet weak var greetingsView: UIView!
    @IBOutlet weak var greetingsLabel: UILabel!
    
    var message_status: Int = 1
    var showSuggestionView = true
    var indexSelect : Int?
    var preferences = UserDefaults.standard
    var fetchedThread = [MessageList]()
    var sendMessageList =
        [
            "Hi",
            "How are you?",
            "Where are you from?",
            "What are your interests?"
        ]
    var receiveMessageList =
        [
            "Hello",
            "I am fine thank you.",
            "I am form Butwal.",
            "Reading books."
        ]
    
    //    var fetchedThread = [
    //        MessageList(indicator: 0, msg: "Hi"),
    //        MessageList(indicator: 1, msg: "Hello"),
    //        MessageList(indicator: 0, msg: "How are you?"),
    //        MessageList(indicator: 1, msg: "I am fine thank you."),
    //        MessageList(indicator: 0, msg: "Where are you from?"),
    //        MessageList(indicator: 1, msg: "I am form Butwal."),
    //        MessageList(indicator: 0, msg: "What are your interests?"),
    //        MessageList(indicator: 1, msg: "Reading books.")
    //    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageTextField.delegate = self
        tableView.register(UINib(nibName: "SenderTableViewCell", bundle: nil), forCellReuseIdentifier: "SenderTableViewCell")
        tableView.register(UINib(nibName: "ReceiveTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceiveTableViewCell")
        tableView.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi));
        self.title = "Message Box"
        greetingsLabel.text = "Hello! \(UserDefaultManager.shared.userDetailsModel?.name ?? "")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        requestForMessage()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        adjustHeight()
    }
    
    @IBAction func notSatisfiedButtonTaped(_ sender: Any) {
        indexSelect = 1
    }
    
    func requestForMessage() {
        // Receive msg here
    }
    
    func adjustHeight(){
        messageTextField.sizeToFit()
        messageTextField.layoutIfNeeded()
        let height = messageTextField.sizeThatFits(CGSize(width: messageTextField.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        messageTextField.contentSize.height = height
        textviewHeight.constant = height
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if (messageTextField.text!.isEmpty || (messageTextField.text!.trimmingCharacters(in: .whitespaces) == "")){
            SwiftMessage.showBanner(title: "Warning!!!", message: "Please Type Your Message", type: .warning)
        } else {
            greetingsView.isHidden = true
            sendMessage()
        }
    }
    
    func sendMessage() {
        // Send Msg Here
        if messageTextField.text == "Hi" {
            fetchedThread.append(MessageList(indicator: 0, msg: messageTextField.text))
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.fetchedThread.append(MessageList(indicator: 1, msg: "Hello."))
                self.tableView.reloadData()
            }
            tableView.reloadData()
            messageTextField.text = ""
        } else if messageTextField.text == "How are you?" {
            fetchedThread.append(MessageList(indicator: 0, msg: messageTextField.text))
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.fetchedThread.append(MessageList(indicator: 1, msg: "I am fine thank you."))
                self.tableView.reloadData()
            }
            tableView.reloadData()
            messageTextField.text = ""
        }  else if messageTextField.text == "Address?" {
            fetchedThread.append(MessageList(indicator: 0, msg: messageTextField.text))
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.fetchedThread.append(MessageList(indicator: 1, msg: "I am from Butwal."))
                self.tableView.reloadData()
            }
            tableView.reloadData()
            messageTextField.text = ""
        }  else if messageTextField.text == "Hobbies?" {
            fetchedThread.append(MessageList(indicator: 0, msg: messageTextField.text))
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.fetchedThread.append(MessageList(indicator: 1, msg: "I love reading, dancing and singing etc."))
                self.tableView.reloadData()
            }
            tableView.reloadData()
            messageTextField.text = ""
        } else if messageTextField.text == "I love you" {
            fetchedThread.append(MessageList(indicator: 0, msg: messageTextField.text))
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.fetchedThread.append(MessageList(indicator: 1, msg: "I love you too. 😘"))
                self.tableView.reloadData()
            }
            tableView.reloadData()
            messageTextField.text = ""
        }
    }
}

extension ConversationViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedThread.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = fetchedThread[indexPath.item]
        if data.indicator == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiveTableViewCell", for: indexPath) as! ReceiveTableViewCell
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
            cell.messageLabel.text = data.msg
            let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .short)
            cell.dateLabel.text = timestamp
            cell.messageBackground.backgroundColor = .red
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTableViewCell", for: indexPath) as! SenderTableViewCell
            cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
            cell.messageLabel.text = data.msg
            let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .short)
            cell.dateLabel.text = timestamp
            cell.FirstNameLabel.text = "AS"
            return cell
        }
    }
    
}
