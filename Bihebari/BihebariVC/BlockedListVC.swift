//
//  BlockedListVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/13/21.
//

import UIKit

class BlockedListVC: UIViewController {
    
    @IBOutlet weak var tblBlockedList: UITableView!
    @IBOutlet weak var hiddenView: UIView!

    var blockedListData : GetBlockedListModal?
    var profileListData : ProfileList?
    var unblockUserData : UnblockUserModal?
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        blockedList()
        print("blockedList: \(blockedListData?.data?.count ?? 0)")
        tblBlockedList.register(UINib(nibName: "CellBlockedList", bundle: nil), forCellReuseIdentifier: "CellBlockedList")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension BlockedListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockedListData?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBlockedList", for: indexPath) as! CellBlockedList
        cell.lblName.text = blockedListData?.data?[indexPath.row].user_code
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (blockedListData?.data?[indexPath.row].image ?? "")
        
        if blockedListData?.data?[indexPath.row].image == nil{
            cell.imgProfile.image = UIImage(named: "imgNoImage")
        }else{
            cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [
            makeDeleteContextualAction(forRowAt: indexPath)
        ])
    }
    
    //MARK: - Contextual Actions
    private func makeDeleteContextualAction(forRowAt indexPath: IndexPath) -> UIContextualAction {
        return UIContextualAction(style: .destructive, title: "Unblock") { (action, swipeButtonView, completion) in
            let alert = UIAlertController(title: "Unblock", message: "Are You Sure You Want To Unblock This User?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                self.UnblockUser(code: self.blockedListData?.data?[indexPath.row].user_code ?? "", id: self.blockedListData?.data?[indexPath.row].id ?? 0)
                self.tblBlockedList.reloadData()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                //
            }))
            self.present(alert, animated: true, completion: nil)
            completion(true)
        }
    }
    
}

extension BlockedListVC {
    func blockedList() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.blockedList(header: header).request, objectType: .general) { [weak self] (result: Result<GetBlockedListModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.blockedListData = data
                    
                    if self?.blockedListData?.data?.count == 0{
                        self?.hiddenView.isHidden = false
                    }else{                        self?.hiddenView.isHidden = true
                    }
                    
                    self?.tblBlockedList.reloadData()
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
}

extension BlockedListVC{
    func UnblockUser(code: String, id: Int) {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.unblockUser(header: header, profileID: id, profileCode: code).request, objectType: .general) { [weak self] (result: Result<UnblockUserModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.unblockUserData = data
                    self?.tblBlockedList.reloadData()
                    self?.unblockSuccess()
                    SwiftMessage.showBanner(title: "Success", message: "User Blocked Successfully.", type: .success)
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func unblockSuccess(){
        self.navigationController?.popViewController(animated: true)
    }
}
