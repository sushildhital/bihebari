//
//  NotificationVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import SocketIO


class NotificationVC: UIViewController {
    
    @IBOutlet weak var collectionNotification: UICollectionView!
    @IBOutlet weak var lblNotificationLabel: UILabel!
    @IBOutlet weak var noRequestView: UIView!
    
    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 0
    
    var interestedData: [Interested_to_me]?
    var currentIndexPath = 0
    let userID = "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionNotification.register(UINib(nibName: "NotificationMatchedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NotificationMatchedCollectionViewCell")
        collectionNotification.reloadData()
        collectionNotification.isPagingEnabled = true
        
        guard let allINterestedData = UserDefaultManager.shared.userDetailsModel?.interested_to_me else {return}
        for i in 0..<allINterestedData.count{
            if allINterestedData[i].status == "approach"{
                interestedData?.append(allINterestedData[i])
            }
        }
        
        //        MARK:- FOR FILTERING APPROACH KEYWORD
        self.interestedData = UserDefaultManager.shared.userDetailsModel?.interested_to_me?.filter{ $0.status == "approach" }
        lblNotificationLabel.text = "\(interestedData?.count ?? 0) peope are waiting for you. Respond Now."
        
        NotificationCenter.default.addObserver(self, selector: #selector(rejectBtnAction), name: NSNotification.Name("RejectBtnTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(acceptBtnAction), name: NSNotification.Name("AcceptBtnTapped"), object: nil)
        
        if interestedData?.count != 0 {
            noRequestView.isHidden = true
        } else {
            noRequestView.isHidden = false
        }
        print("Notifications exec")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    @objc func rejectBtnAction() {
        self.interestedData?.remove(at: self.currentIndexPath)
        self.collectionNotification.reloadData()
    }
    
    @objc func acceptBtnAction() {
        self.interestedData?.remove(at: self.currentIndexPath)
        self.collectionNotification.reloadData()
    }
    
    func moveToDetailVC(data: UserMoreDetailsModel?) {
        let vc = NotificationMatchedInfoVC.instantiate(fromAppStoryboard: .NotificationMatchedInfo)
        vc.dataReceived = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension NotificationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interestedData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationMatchedCollectionViewCell", for: indexPath) as! NotificationMatchedCollectionViewCell
        guard let model = interestedData?[indexPath.item] else { return cell }
        cell.updateUI(modelData: model, indexPath: indexPath)
        cell.accepTapped = { [weak self] in
            self?.interestedData?.remove(at: indexPath.item)
            self?.collectionNotification.reloadData()
            self?.callAcceptAPI(code: "\(model.user_code ?? "")", id: model.id ?? 0)
        }
        
        cell.rejectTapped = { [weak self] in
            self?.interestedData?.remove(at: indexPath.item)
            self?.collectionNotification.reloadData()
            self?.callRejectAPI(code: "\(model.user_code ?? "")", id: model.id ?? 0)
        }
        
        cell.btnViewProfileTapped = { [weak self] view in
//            print("indexPath: \(indexPath.item)")
            self?.currentIndexPath = indexPath.item
            self?.userDetailsApi(profile: self?.interestedData?[indexPath.item].user_code ?? "")
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemHeight : CGFloat = 650
        return CGSize(width: collectionViewWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSET
    }
    
}

extension NotificationVC {
    func callAcceptAPI(code: String, id: Int) {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            APIClient.shared.fetch(with: APIService.accept(header: header, profileID: id, profileCode: code).request, objectType: .general) { [weak self] (result: Result<BlockUserModal?, APIError>) in
                Spinner.stop()
                guard let _ = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success !!", message: "", type: .success)
                    self?.collectionNotification.reloadData()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func callRejectAPI(code: String, id: Int) {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            APIClient.shared.fetch(with: APIService.reject(header: header, profileID: id, profileCode: code).request, objectType: .general) { [weak self] (result: Result<BlockUserModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success !!", message: "", type: .success)
                    self.collectionNotification.reloadData()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func userDetailsApi(profile: String) {
        if Reachability.isConnectedToNetwork(){
            APIClient.shared.fetch(with: APIService.userInfoProfile(profileID: profile).request, objectType: .general) { [weak self] (result: Result<UserMoreDetailsModel?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    self.moveToDetailVC(data: data)
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}


