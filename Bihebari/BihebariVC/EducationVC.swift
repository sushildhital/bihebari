//
//  EducationVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/13/21.
//

import UIKit
import IBAnimatable

class EducationVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    
    var educationProfileArray : EducationModal?
    var educationDegreeID: Int?
    var professionID: Int?
    
    @IBOutlet weak var txtEducationDegree: AnimatableTextField!
    @IBOutlet weak var txtEducationDegreeName: AnimatableTextField!
    @IBOutlet weak var txtProfession: AnimatableTextField!
    @IBOutlet weak var txtProfessionType: AnimatableTextField!
    @IBOutlet weak var txtCompanyType: AnimatableTextField!
    @IBOutlet weak var txtCurrency: AnimatableTextField!
    @IBOutlet weak var txtFrom: AnimatableTextField!
    @IBOutlet weak var txtTo: AnimatableTextField!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtEducationDegree.text == ""{
            txtEducationDegree.attributedPlaceholder = NSAttributedString(string: "Education degree required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtEducationDegree.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtEducationDegreeName.text == ""{
            txtEducationDegreeName.attributedPlaceholder = NSAttributedString(string: "Education degree name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtEducationDegreeName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtProfession.text == ""{
            txtProfession.attributedPlaceholder = NSAttributedString(string: "Profession required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtProfession.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtProfessionType.text == ""{
            txtProfessionType.attributedPlaceholder = NSAttributedString(string: "Profession type required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtProfessionType.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCompanyType.text == ""{
            txtCompanyType.attributedPlaceholder = NSAttributedString(string: "Company type required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCompanyType.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCurrency.text == ""{
            txtCurrency.attributedPlaceholder = NSAttributedString(string: "Currency required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCurrency.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnContinueAction(_ sender: Any) {
        if isValid(){
            requestEducationUpdate()
        }else {
            let alert = UIAlertController(title: "Fields Required.", message: "Some required fields are empty.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        educationProfile()
        setupPicker()
        navigationController?.navigationBar.isHidden = true
        updateTextFieldValue()
        txtCurrency.text = "NPR"
    }
    
    func updateTextFieldValue(){
        txtEducationDegree.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.education_name
        self.educationDegreeID = UserDefaultManager.shared.userDetailsModel?.profile_general?.education_id
        txtEducationDegreeName.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.education_degree
        txtProfession.text = UserDefaultManager.shared.userDetailsModel?.profile_profession?.profession_name
        self.professionID = UserDefaultManager.shared.userDetailsModel?.profile_profession?.profession_id
        txtProfessionType.text = UserDefaultManager.shared.userDetailsModel?.profile_profession?.profession_type
        txtCompanyType.text = UserDefaultManager.shared.userDetailsModel?.profile_profession?.company_type
        if UserDefaultManager.shared.userDetailsModel?.profile_profession?.currency == ""{
            txtCurrency.text = "NPR"
        }else{
            txtCurrency.text = UserDefaultManager.shared.userDetailsModel?.profile_profession?.currency
        }
        txtFrom.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_profession?.monthly_income ?? 0)"
        txtTo.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_profession?.monthly_income ?? 0)"
    }
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtEducationDegree.delegate = self
        txtProfession.delegate = self
        txtProfessionType.delegate = self
        txtCompanyType.delegate = self
        txtCurrency.delegate = self
        self.pickerView = picker
        return picker
    }()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEducationDegree {
            txtEducationDegree.text = "\(educationProfileArray?.data?.education_degree?[0].name ?? "")"
            educationDegreeID = educationProfileArray?.data?.education_degree?[0].id
        } else if  textField == txtProfession {
            txtProfession.text = "\(educationProfileArray?.data?.profession?[0].name ?? "")"
            professionID = educationProfileArray?.data?.profession?[0].id
        } else if textField == txtProfessionType {
            txtProfessionType.text = "\(educationProfileArray?.data?.profession_type?[0] ?? "")"
        } else if textField == txtCompanyType {
            txtCompanyType.text = "\(educationProfileArray?.data?.company_type?[0] ?? "")"
        }else if textField == txtCurrency {
            txtCurrency.text = "\(educationProfileArray?.data?.currency?[0].code ?? "")"
        }
        return true
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtEducationDegree.isFirstResponder{
            return educationProfileArray?.data?.education_degree?.count ?? 0
        }else if txtProfession.isFirstResponder{
            return educationProfileArray?.data?.profession?.count ?? 0
        }else if txtProfessionType.isFirstResponder{
            return educationProfileArray?.data?.profession_type?.count ?? 0
        }else if txtCompanyType.isFirstResponder{
            return educationProfileArray?.data?.company_type?.count ?? 0
        }else if txtCurrency.isFirstResponder{
            return educationProfileArray?.data?.currency?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtEducationDegree.isFirstResponder{
            return educationProfileArray?.data?.education_degree?[row].name ?? ""
        }else if txtProfession.isFirstResponder{
            return educationProfileArray?.data?.profession?[row].name
        }else if txtProfessionType.isFirstResponder{
            return educationProfileArray?.data?.profession_type?[row]
        }else if txtCompanyType.isFirstResponder{
            return educationProfileArray?.data?.company_type?[row] ?? ""
        }else if txtCurrency.isFirstResponder{
            return educationProfileArray?.data?.currency?[row].code ?? ""
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtEducationDegree.isFirstResponder{
            let itemselected = educationProfileArray?.data?.education_degree?[row].name ?? ""
            self.educationDegreeID = educationProfileArray?.data?.education_degree?[row].id
            txtEducationDegree.text = itemselected
        }else if txtProfession.isFirstResponder{
            let itemselected = educationProfileArray?.data?.profession?[row].name
            self.professionID = educationProfileArray?.data?.profession?[row].id
            txtProfession.text = itemselected
        }else if txtProfessionType.isFirstResponder{
            let itemselected = educationProfileArray?.data?.profession_type?[row]
            txtProfessionType.text = itemselected
        }else if txtCompanyType.isFirstResponder{
            let itemselected = educationProfileArray?.data?.company_type?[row] ?? ""
            txtCompanyType.text = itemselected
        }else if txtCurrency.isFirstResponder{
            let itemselected = educationProfileArray?.data?.currency?[row].code ?? ""
            txtCurrency.text = itemselected
        }
    }
    
    private func setupPicker(){
        txtEducationDegree.inputView = pickerView
        txtProfession.inputView = pickerView
        txtProfessionType.inputView = pickerView
        txtCompanyType.inputView = pickerView
        txtCurrency.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension EducationVC {
    func educationProfile() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.education.request, objectType: .general) { [weak self] (result: Result<EducationModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.educationProfileArray = data
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension EducationVC {
    func requestEducationUpdate() {
        if Reachability.isConnectedToNetwork(){
            let params = [
                "education_degree": "\(educationDegreeID ?? 0)",
                "education_degree_name": txtEducationDegreeName.text!,
                "profession_id": "\(professionID ?? 0)",
                "company_type": txtCompanyType.text!,
                "profession_type": txtProfessionType.text!,
                "currency": txtCurrency.text!,
                "amount_from": txtFrom.text!,
                "amount_to": txtTo.text!,
            ] as [String : Any]
            
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            
            Spinner.start()
            APIClient.shared.fetch(with: APIService.updateEducation(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<UpdateGeneralModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Education Updated Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        if BuildProfileHandler.shared.isFromEditProfile {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.educationUpdateSuccess()
                        }
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func educationUpdateSuccess(){
        let vc = KundaliVC.instantiate(fromAppStoryboard: .Kundali)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
