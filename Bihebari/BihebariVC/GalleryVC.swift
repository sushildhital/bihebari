//
//  GalleryVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/12/21.
//

import UIKit
import Viewer
import IBAnimatable
import ImageViewer_swift

class GalleryVC: UIViewController {
    
    var profileList : UserMoreDetailsModel?
    var responseArray : HomeModal?
    
    @IBOutlet weak var collectionStory: UICollectionView!
    @IBOutlet weak var collectionGallery: UICollectionView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblStoryTitle: UILabel!
    @IBOutlet weak var lblNoStoryAvailable: UILabel!
    @IBOutlet weak var lblNoGalleryAvailable: UILabel!
    @IBOutlet weak var imgProfile: AnimatableImageView!
    
    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 0
    let BOTTOM_INSET : CGFloat = 0
    let LEFT_INSET : CGFloat = 0
    let RIGHT_INSET : CGFloat = 0
    let SECTION_INSET : CGFloat = 0
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 10
    
    let NUMBER_OF_COLUMNN : CGFloat = 3
    let TOP_INSETT : CGFloat = 2
    let BOTTOM_INSETT : CGFloat = 2
    let LEFT_INSETT : CGFloat = 2
    let RIGHT_INSETT : CGFloat = 2
    let SECTION_INSETT : CGFloat = 2
    let INTER_ITEM_INSETT : CGFloat = 4
    let LINE_SPACEE : CGFloat = 4
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        collectionStory.register(UINib(nibName: "StoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "StoriesCollectionViewCell")
        collectionGallery.register(UINib(nibName: "GalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCollectionViewCell")
        lblUserName.text = profileList?.data?.user?.user_code
        
        if profileList?.data?.user?.story_image?.count ?? 0 == 0{
            collectionStory.isHidden = true
            lblNoStoryAvailable.isHidden = false
        }else{
            collectionStory.isHidden = false
            lblNoStoryAvailable.isHidden = true
        }
        
        if profileList?.data?.user?.other_image?.count ?? 0 == 0{
            collectionGallery.isHidden = true
            lblNoGalleryAvailable.isHidden = false
        }else{
            collectionGallery.isHidden = false
            lblNoGalleryAvailable.isHidden = true
        }
        
        
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (profileList?.data?.user?.profile_image?.photo ?? "")
        
        if profileList?.data?.user?.profile_image?.photo == nil{
            imgProfile.image = UIImage(named: "imgNoImage")
        }else{
            imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension GalleryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionStory{
            return profileList?.data?.user?.story_image?.count ?? 0
        }else{
            return profileList?.data?.user?.other_image?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionStory{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoriesCollectionViewCell", for: indexPath) as! StoriesCollectionViewCell
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (profileList?.data?.user?.story_image?[indexPath.row].photo ?? "")
            
            if profileList?.data?.user?.story_image?[indexPath.row].photo == nil{
                cell.imgStory.image = UIImage(named: "imgNoImage")
                cell.imgStory.setupImageViewer()
            }else{
                cell.imgStory.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
                cell.imgStory.setupImageViewer()

            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (profileList?.data?.user?.other_image?[indexPath.row].photo ?? "")
            
            if profileList?.data?.user?.other_image?[indexPath.row].photo == nil{
                cell.imgGallery.image = UIImage(named: "imgNoImage")
                cell.imgGallery.setupImageViewer()

            }else{
                cell.imgGallery.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
                cell.imgGallery.setupImageViewer()

            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionStory{
            let itemHeight : CGFloat = 50
            let itemWidth : CGFloat = 50
            return CGSize(width: itemWidth, height: itemHeight)
        }else{
            let collectionViewWidth = collectionView.bounds.width
            let itemWidth = (collectionViewWidth - LEFT_INSETT - RIGHT_INSETT - (INTER_ITEM_INSETT * (NUMBER_OF_COLUMNN - 1))) / NUMBER_OF_COLUMNN
            let itemHeight : CGFloat = 104
            return CGSize(width: itemWidth, height: itemHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionStory{
            return UIEdgeInsets(top: TOP_INSET, left: LEFT_INSET, bottom: BOTTOM_INSET, right: RIGHT_INSET)
        }else{
            return UIEdgeInsets(top: TOP_INSETT, left: LEFT_INSETT, bottom: BOTTOM_INSETT, right: RIGHT_INSETT)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionStory{
            return LINE_SPACE
        }else{
            return LINE_SPACEE
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionStory{
            return INTER_ITEM_INSET
        }else{
            return INTER_ITEM_INSETT
        }
    }
    
}
