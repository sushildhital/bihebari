//
//  UpgradePackageVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/31/21.
//

import UIKit

class UpgradePackageVC: UIViewController {
    
    @IBOutlet weak var collectionUpgradePackage: UICollectionView!

    var packageModal: UpgradePackageModal?
    var packageImages = [UIImage(named: "imgUpgradeHeart")!, UIImage(named: "imgUpgradeStar")!, UIImage(named: "imgUpgradeVIP")!, UIImage(named: "imgUpgradeHeart")!]
    var mainviewColors = [UIColor.init(hexString: "#FFFFFF"),UIColor.init(hexString: "#FFFFFF"),UIColor.init(hexString: "#FFFFFF"),UIColor.init(hexString: "#FFFFFF")]
    var packageViewColors = [UIColor.init(hexString: "##44CBC6"),UIColor.init(hexString: "#273691"),UIColor.init(hexString: "#CC1C29")]
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        packageUpgrade()
        collectionUpgradePackage.register(UINib(nibName: "UpgradePackageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UpgradePackageCollectionViewCell")
        customizeCollectionViewCell()
    }
    
    func customizeCollectionViewCell(){
        let flowLayout = UPCarouselFlowLayout()
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.size.width - 60.0, height: collectionUpgradePackage.frame.size.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.sideItemScale = 0.8
        flowLayout.sideItemAlpha = 1.0
        flowLayout.spacingMode = .fixed(spacing: 16.0)
        collectionUpgradePackage.collectionViewLayout = flowLayout
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.collectionUpgradePackage.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }

    fileprivate var currentPage: Int = 0 {
        didSet {
            print("page at centre = \(currentPage)")
        }
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.collectionUpgradePackage.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension UpgradePackageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return packageModal?.data?.packages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpgradePackageCollectionViewCell", for: indexPath) as! UpgradePackageCollectionViewCell
        cell.loadColor(color: packageViewColors)
        cell.btnPayNowTapped = { [weak self] view in
            self!.moveToPaymentVC(data: "\(self?.packageModal?.data?.packages?[indexPath.row].amount ?? 0)", pkgID: self?.packageModal?.data?.packages?[indexPath.row].id ?? 0, pkgName: self?.packageModal?.data?.packages?[indexPath.row].name ?? "")
        }

        let model = packageModal?.data?.packages?[indexPath.row]
        cell.lblPackageName.text = model?.name
        cell.lblRupees.text = "Rs. \(model?.amount ?? 0)"
        cell.lblMonths.text = "(\(model?.number_of_months ?? 0) Months)"
        cell.imgPackage.image = packageImages[indexPath.row]
        cell.imgCheckBox.tintColor = packageViewColors[indexPath.row]
        cell.mainView.backgroundColor = mainviewColors[indexPath.row]
        cell.packageView.backgroundColor = packageViewColors[indexPath.row]
        cell.btnPayNow.backgroundColor = packageViewColors[indexPath.row]
        cell.btnPayNow.shadowColor = packageViewColors[indexPath.row]
        cell.mainView.borderColor = packageViewColors[indexPath.row]
        cell.selectedIndex = indexPath.item
        cell.cellUpgradePackage.reloadData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func moveToPaymentVC(data: String, pkgID: Int, pkgName: String){
        
        let vc = UIStoryboard(name: "PaymentOption2", bundle: nil).instantiateViewController(withIdentifier: "PaymentOption2") as! PaymentOption2
//        let vc = PaymentOptionVC.instantiate(fromAppStoryboard: .PaymentOption)
//        vc.amount = data
        vc.selectedPackageID = pkgID
//        vc.selectedPackageName = pkgName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension UpgradePackageVC {
    func packageUpgrade() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.packageUpgrade.request, objectType: .general) { [weak self] (result: Result<UpgradePackageModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.packageModal = data
                    self?.collectionUpgradePackage.reloadData()
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Spinner.stop()
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}
