//
//  NotificationMatchedInfoVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/16/21.
//

import UIKit
import FittedSheets

struct ImageName {
    var imgName: String?
}

class NotificationMatchedInfoVC: UIViewController {
    
    @IBOutlet weak var collectionInfo: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet weak var viewReadMore: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblAboutUsInfo: UILabel!
    @IBOutlet weak var btnSettings: UIButton!
    // MARK:- INFO LABELS
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblMaritalStatus: UILabel!
    @IBOutlet weak var lblCurrentlyLivingIn: UILabel!
    @IBOutlet weak var lbReligion: UILabel!
    @IBOutlet weak var lblCaste: UILabel!
    @IBOutlet weak var lblMotherTongue: UILabel!
    @IBOutlet weak var lblDiet: UILabel!
    @IBOutlet weak var lblHeightGeneral: UILabel!
    @IBOutlet weak var lblEducationDegree: UILabel!
    @IBOutlet weak var lblEdecationDegreeName: UILabel!
    @IBOutlet weak var lblProfessionEducation: UILabel!
    @IBOutlet weak var lblSector: UILabel!
    @IBOutlet weak var imgActiveStatus: UIImageView!
    
    var isReadMoreTapped = false
    var counter = 0
    var dataReceived: UserMoreDetailsModel?
    var imageCollection: [ImageName]?
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReadMoreAction(_ sender: Any) {
        isReadMoreTapped = !isReadMoreTapped
        if isReadMoreTapped{
            UIView.animate(withDuration: 0.3) {
                self.lblAboutUs.numberOfLines = 0
                Util.dropShadow(view: self.viewReadMore, color: .clear, opacity: 1, offSet: CGSize(width: 0, height: -10), radius: 10, scale: true)
                self.btnReadMore.setTitle("Read Less", for: .normal)
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.3) {
                self.lblAboutUs.numberOfLines = 4
                self.btnReadMore.setTitle("Read More", for: .normal)
                Util.dropShadow(view: self.viewReadMore, color: .white, opacity: 1, offSet: CGSize(width: 0, height: -10), radius: 10, scale: true)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnSettingsAction(_ sender: Any) {
        moveToSettingVC()
    }
    
    @IBAction func btnGalleryAction(_ sender: Any) {
        moveToGalleryVC()
    }
    
    func moveToSettingVC(){
        let vc = SettingsVC.instantiate(fromAppStoryboard: .Settings)
        let sheetController = SheetViewController(controller: vc, sizes: [.fixed(280)])
        sheetController.allowPullingPastMaxHeight = false
        self.present(sheetController, animated: true , completion: nil)
    }
    
    func moveToGalleryVC(){
        let vc = GalleryVC.instantiate(fromAppStoryboard: .Gallery)
        vc.profileList = self.dataReceived
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        collectionInfo.register(UINib(nibName: "NotificationMatchedInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NotificationMatchedInfoCollectionViewCell")
        informationLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isReadMoreTapped = false
        lblAboutUs.numberOfLines = 4
        Util.dropShadow(view: viewReadMore, color: .white, opacity: 1, offSet: CGSize(width: 0, height: -10), radius: 10, scale: true)
        scrollView.setContentOffset(.zero, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func informationLabel() {
        if let data = dataReceived?.data?.user {
            lblName.text = data.name
            lblAge.text = "\(data.profile_general?.current_age_year ?? 0) Years"
            lblAddress.text = data.get_current_address?.city
            lblHeight.text = "\(data.profile_figure?.height_ft ?? 0)'\(data.profile_figure?.height_in ?? 0)"
            lblAboutUs.text = data.profile_general?.about
            lblProfession.text = data.profile_general?.profession_name
            lblMaritalStatus.text = data.profile_general?.marrital_status
            lblCurrentlyLivingIn.text = data.get_current_address?.country_name
            lbReligion.text = data.profile_general?.relegion_name
            lblCaste.text = data.profile_general?.caste_name
            lblMotherTongue.text = data.profile_general?.mother_tounge_name
            lblHeightGeneral.text = "\(data.profile_figure?.height_ft ?? 0)'\(data.profile_figure?.height_in ?? 0)"
            if data.profile_figure?.vegetarian == "no" {
                lblDiet.text = "Non-Veg"
            }else{
                lblDiet.text = "Veg"
            }
            lblEducationDegree.text =  data.profile_general?.education_name
            lblEdecationDegreeName.text = data.profile_general?.education_name
            lblProfessionEducation.text = data.profile_general?.profession_name
            lblSector.text = data.profile_general?.sectors_name
            
            if data.online == "Y"{
                imgActiveStatus.image = UIImage(named: "imgActive")
            }else if data.online == "N"{
                imgActiveStatus.image = UIImage(named: "imgNotActive")
            }
        }
    }
    
//    func loadImages() {
//        if let imageData = dataReceived?.data?.user?.profile_image?.photo {
//            imageCollection?.append(ImageName(imgName: imageData))
//            print("First image appended here")
//        }
//
//        if let otherImage = dataReceived?.data?.user?.other_image {
//            for photo in otherImage {
//                imageCollection?.append(ImageName(imgName: photo.photo))
//            }
//            print("Second image appended here")
//        }
//        print("Image Value here: \(imageCollection ?? [])")
//    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(collectionInfo.contentOffset.x) / Int(collectionInfo.frame.width)
        counter = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
    // MARK:- IBActions
    @IBAction func rejectButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name("RejectBtnTapped"), object: nil)
    }
    
    @IBAction func acceptButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name("AcceptBtnTapped"), object: nil)
    }
    
}

extension NotificationMatchedInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataReceived?.data?.user?.other_image?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationMatchedInfoCollectionViewCell", for: indexPath) as! NotificationMatchedInfoCollectionViewCell
        let url = Constants.BASE_URL + "/\(UserDefaultManager.shared.imagePath ?? "")" + (dataReceived?.data?.user?.other_image?[indexPath.row].photo ?? "")
        cell.imgInfo.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionInfo.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}

