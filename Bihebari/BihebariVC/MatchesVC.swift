//
//  MatchesVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/9/21.
//

import UIKit
import FittedSheets

class MatchesVC: UIViewController {
    
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lblNoDataMessage: UILabel!
    
    @IBOutlet weak var collectionMatches: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var shortlistData: GetShortlistModal?
    var addToShortlistModal: AddToShortlistsModal?
    var profileList : SearchProfile?
    var searchedArray = [SearchProfile]()
    var userCode = ""
    var indexPath: IndexPath?
    
    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 0
    
    // MARK: -
    // MARK: Private Utility Methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.default
    }
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        if ViewHandler.shared.isFromMatchesVC {
            self.navigationController?.popViewController(animated: true)
        }else if ViewHandler.shared.isFromShortList {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnOKAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let indexPath = indexPath {
            collectionMatches.isPagingEnabled = false
            collectionMatches.scrollToItem(at: indexPath, at: .left, animated: false)
            collectionMatches.isPagingEnabled = true
        }
        collectionMatches.shouldIgnoreContentInsetAdjustment = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        handleMultipleViews()
        collectionMatches.isPagingEnabled = true
        collectionMatches.register(UINib(nibName: "MatchesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MatchesCollectionViewCell")
        collectionMatches.reloadData()
    }
    
    func handleMultipleViews(){
        if ViewHandler.shared.isFromMatchesVC {
            lblTitle.text = "Here are few matches as per your preferences"
        }else if ViewHandler.shared.isFromShortList {
            lblTitle.text = "My Shortlist"
            lblTitle.textAlignment = .center
            getShortlist()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension MatchesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ViewHandler.shared.isFromMatchesVC {
            return searchedArray.count
        } else if ViewHandler.shared.isFromShortList {
            return shortlistData?.data?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchesCollectionViewCell", for: indexPath) as! MatchesCollectionViewCell
        
        if ViewHandler.shared.isFromMatchesVC {
            cell.btnViewProfileTapped = { [weak self] view in
                self?.profileDetailsAPI(profile: self?.searchedArray[indexPath.row].user_code ?? "")
            }
            userCode = searchedArray[indexPath.row].user_code ?? ""
            cell.btnMarkInterestedTapped = { [weak self] view in
                let alert = UIAlertController(title: "Add to Shortlist?", message: "Do you want to add this user in your shortlist?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                    self?.addToShortlist()
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self?.present(alert, animated: true)
            }
            
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (searchedArray[indexPath.row].profile_image ?? "")
            
            if searchedArray[indexPath.row].profile_image == nil{
                cell.imgProfile.image = UIImage(named: "imgNoImage")
            }else{
                cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
            }
            
            cell.lblName.text = searchedArray[indexPath.row].user_code
            cell.lblAddress.text = searchedArray[indexPath.row].address
            cell.lblHeight.text = "\(searchedArray[indexPath.row].height_ft ?? 0)'\(searchedArray[indexPath.row].height_inch ?? 0)"
            cell.lblAge.text = "\(searchedArray[indexPath.row].age ?? 0) Years"
            cell.lblProfession.text = searchedArray[indexPath.row].profession
            cell.lblActiveStatus.text = "Active 1 Hour Ago"
            cell.btnRemoveFromShortlist.isHidden = true
            
            if searchedArray[indexPath.row].online_status == "Y"{
                cell.imgActive.image = UIImage(named: "imgActive")
            }else if searchedArray[indexPath.row].online_status == "N"{
                cell.imgActive.image = UIImage(named: "imgNotActive")
            }
            
            cell.cellAction = {action in
                DispatchQueue.main.async {
                    collectionView.isPagingEnabled = false
                    let row = action == .next ? indexPath.item + 1 : indexPath.item - 1
                    collectionView.scrollToItem(at: IndexPath(row: row, section: 0), at: .left, animated: false)
                    collectionView.isPagingEnabled = true
                }
            }
        } else if ViewHandler.shared.isFromShortList {
            
            cell.btnRemoveFromShortlistTapped = { [weak self] view in
                self?.removeShortlist(id: self?.shortlistData?.data?[indexPath.item].id ?? 0)
            }
            
            cell.btnViewProfileTapped = { [weak self] view in
                self?.profileDetailsAPI(profile: self?.shortlistData?.data?[indexPath.row].user_code ?? "")
            }
            
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (shortlistData?.data?[indexPath.row].image ?? "")
            
            if shortlistData?.data?[indexPath.row].image == nil{
                cell.imgProfile.image = UIImage(named: "imgNoImage")
            }else{
                cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
            }
                        
            cell.lblName.text = shortlistData?.data?[indexPath.row].user_code
            cell.lblAge.text = "\(shortlistData?.data?[indexPath.row].age ?? 0)\(" yrs")"
            cell.lblAddress.text = shortlistData?.data?[indexPath.row].address
            cell.lblHeight.text = "\(shortlistData?.data?[indexPath.row].height_ft ?? 0)'\(shortlistData?.data?[indexPath.row].height_inch ?? 0)"
            cell.lblProfession.text = shortlistData?.data?[indexPath.row].profession
            cell.imgShortlist.isHidden = true
            cell.btnShortlist.isHidden = true
            cell.imgActive.isHidden = true
            cell.btnRemoveFromShortlist.isHidden = false
            cell.cellAction = {action in
                DispatchQueue.main.async {
                    collectionView.isPagingEnabled = false
                    let row = action == .next ? indexPath.item + 1 : indexPath.item - 1
                    collectionView.scrollToItem(at: IndexPath(row: row, section: 0), at: .left, animated: false)
                    collectionView.isPagingEnabled = true
                }
            }
            cell.imgActive.isHidden = true
            cell.lblActiveStatus.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? MatchesCollectionViewCell else {return}
        let totalNumberOfItems = collectionView.numberOfItems(inSection: 0)
        cell.btnPrevious.isHidden = indexPath.item == 0
        cell.btnNext.isHidden =  indexPath.item == totalNumberOfItems - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemHeight : CGFloat = 650
        return CGSize(width: collectionViewWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSET
    }
    
    func moveToUserDetails(data: SearchProfile?){
        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
        vc.searchProfileList = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    func moveToShorlistData(data: ProfileList?){
    //        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
    //        vc.profileList = data
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    
}

extension MatchesVC{
    func getShortlist() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.getShortlist(header: header).request, objectType: .general) { [weak self] (result: Result<GetShortlistModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.shortlistData = data
                        if data?.data?.count == 0{
                            self?.hiddenView.isHidden = false
                            self?.lblNoData.text = "No Shortlisted Users"
                            self?.lblNoDataMessage.text = "There Aren't Any Shortlised Users."
                        }else{
                            self?.hiddenView.isHidden = true
                        }
                    self?.collectionMatches.reloadData()
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension MatchesVC {
    func addToShortlist() {
        if Reachability.isConnectedToNetwork(){
            let params = ["profile_code": userCode] as [String : Any]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            print("FROM MATCHES:\(params)")
            Spinner.start()
            APIClient.shared.fetch(with: APIService.addToShortlist(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<AddToShortlistsModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    if data?.success == "error"{
                        SwiftMessage.showBanner(title: "Error", message: "User Already Added to Shortlist", type: .error)
                    }else{
                        SwiftMessage.showBanner(title: "Success", message: "Added to shortlist successfully.", type: .success)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension MatchesVC{
    func removeShortlist(id: Int) {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.removeShortlist(profileId: id).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success( _):
                    self?.removeShortlistSuccess()
                    self?.getShortlist()
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func removeShortlistSuccess(){
        self.getShortlist()
    }
}

extension MatchesVC{
    func profileDetailsAPI(profile: String) {
        if Reachability.isConnectedToNetwork(){
            APIClient.shared.fetch(with: APIService.userInfoProfile(profileID: profile).request, objectType: .general) { [weak self] (result: Result<UserMoreDetailsModel?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    self.moveToDetailVC(data: data)
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func moveToDetailVC(data: UserMoreDetailsModel?) {
        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
        vc.dataReceived = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
