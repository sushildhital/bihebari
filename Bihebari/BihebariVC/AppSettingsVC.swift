//
//  AppSettingsVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/30/21.
//

import UIKit

class AppSettingsVC: UIViewController {
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdateAccountDetailAction(_ sender: Any) {
        let vc = UpdateAccountDetailVC.instantiate(fromAppStoryboard: .UpdateAccountDetail)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
        if let url = NSURL(string: "https://bihebari.com/privacy-policy"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func btnTermsAndConditionAction(_ sender: Any) {
        if let url = NSURL(string: "https://bihebari.com/terms-conditions"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func btnBlockedListAction(_ sender: Any) {
        let vc = BlockedListVC.instantiate(fromAppStoryboard: .BlockedList)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
}
