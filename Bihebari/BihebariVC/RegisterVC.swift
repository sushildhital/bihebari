//
//  RegisterVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import IBAnimatable

class RegisterVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    
    var createProfileForArray = ["Self", "Son", "Daughter", "Brother", "Sister", "Others"]
    var countryArray : CountryModal?
    var selectedCountryId = 153
    var itemSelected = ""
    var gender = String()
    
    @IBOutlet weak var txtFullName: AnimatableTextField!
    @IBOutlet weak var txtEmailAddress: AnimatableTextField!
    @IBOutlet weak var txtPassword: AnimatableTextField!
    @IBOutlet weak var txtConfirmPassword: AnimatableTextField!
    @IBOutlet weak var txtPhoneNumber: AnimatableTextField!
    @IBOutlet weak var txtCreateProfileFor: AnimatableTextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtCountry: AnimatableTextField!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var imgTerms: UIImageView!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    var isTermsSelected: Bool = false{
        didSet{
            if isTermsSelected{
                imgTerms.image = UIImage(named: "imgCheckBox")
            }else{
                imgTerms.image = UIImage(named: "imgBox")
            }
        }
    }
        
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.default
    }
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtFullName.text == ""{
            txtFullName.attributedPlaceholder = NSAttributedString(string: "Full name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtFullName.becomeFirstResponder()
            }
            return false
        }else{
            txtFullName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtEmailAddress.text == ""{
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "Email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmailAddress.becomeFirstResponder()
            }
            return false
        }else if !Util.isValidEmail(testStr: txtEmailAddress.text!){
            txtEmailAddress.text = ""
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "Valid email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmailAddress.becomeFirstResponder()
            }
            return false
        }else{
            txtEmailAddress.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPassword.text == ""{
            txtPassword.attributedPlaceholder = NSAttributedString(string: "Password required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtPassword.becomeFirstResponder()
            }
            return false
        }else{
            txtPassword.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtConfirmPassword.text == ""{
            txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Password required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtConfirmPassword.becomeFirstResponder()
            }
            return false
        }else{
            txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPhoneNumber.text == ""{
            txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone number required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtPhoneNumber.becomeFirstResponder()
            }
            return false
        }
//        else if !Util.isValidPhone(testStr: txtPhoneNumber.text!){
//            txtPhoneNumber.text = ""
//            txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Valid phone number required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
//                self.txtPhoneNumber.becomeFirstResponder()
//            }
//            return false
//        }
        else{
            txtPhoneNumber.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCountry.text == ""{
            txtCountry.attributedPlaceholder = NSAttributedString(string: "Country required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtCountry.becomeFirstResponder()
            }
            return false
        }else{
            txtCountry.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnTermsAction(_ sender: Any) {
        isTermsSelected = !isTermsSelected
    }
    
    @IBAction func btnAcceptTermsAndCondition(_ sender: Any) {
        if let url = NSURL(string: "https://bihebari.com/terms-conditions"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func btnMaleAction(_ sender: Any) {
        btnMale.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnFemale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnOthers.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "male"
//        print(gender)
    }
    
    @IBAction func btnFemaleAction(_ sender: Any) {
        btnFemale.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnMale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnOthers.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "female"
//        print(gender)
    }
    
    @IBAction func btnOthersAction(_ sender: Any) {
        btnOthers.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnMale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnFemale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "others"
//        print(gender)
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        if isValid(){
            if isTermsSelected{
                if txtPassword.text! == txtConfirmPassword.text!{
                    requestForRegister()
                }else{
                    Util.showAlert(title: "Errow!", message: "Your password do not match.", view: self)
                }
            }else{
                SwiftMessage.showBanner(title: "Error", message: "You Haven't Accepted To Our Terms And Condition.", type: .error)
            }
        }else{
            print("FAILED...")
        }
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPicker()
        Country()
        txtCountry.text = "Nepal"
        navigationController?.navigationBar.isHidden = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCreateProfileFor.delegate = self
        txtCountry.delegate = self
        self.pickerView = picker
        return picker
    }()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtCreateProfileFor.isFirstResponder{
            return createProfileForArray.count
        }else if txtCountry.isFirstResponder{
            return countryArray?.data?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtCreateProfileFor.isFirstResponder{
            return createProfileForArray[row]
        }else if txtCountry.isFirstResponder{
            return countryArray?.data?[row].country_name ?? ""
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtCreateProfileFor.isFirstResponder{
            let itemselected = createProfileForArray[row]
            txtCreateProfileFor.text = itemselected
        }else if txtCountry.isFirstResponder{
            let itemselected = countryArray?.data?[row].country_name ?? ""
            txtCountry.text = itemselected
            selectedCountryId = countryArray?.data?[row].id ?? -1
            lblCountryCode.text = "+" + (countryArray?.data?[row].country_phone_code ?? "--")
        }
    }
    
    private func setupPicker(){
        txtCreateProfileFor.inputView = pickerView
        txtCountry.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = .default
        btnMale.isSelected = true
        btnMale.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnFemale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnOthers.setImage(UIImage(named: "btnUnselected"), for: .normal)
        txtCreateProfileFor.text = "Self"
        gender = "male"
//        print(gender)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        txtFullName.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension RegisterVC {
    func requestForRegister() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : Any] = [
                "name": txtFullName.text!,
                "email": txtEmailAddress.text!,
                "password": txtPassword.text!,
                "password_confirmation": txtConfirmPassword.text!,
                "phone": txtPhoneNumber.text!,
                "profile_for": txtCreateProfileFor.text!,
                "country": selectedCountryId,
                "gender": gender
            ]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.userRegister(body: params).request, objectType: .general) { [weak self] (result: Result<LoginResponseModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Registration Successfull.", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.RegisterSuccess()
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                    SwiftMessage.showBanner(title: "Error", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func RegisterSuccess(){
        let vc = LoginVC.instantiate(fromAppStoryboard: .Login)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension RegisterVC {
    func Country() {
        Spinner.start()
        APIClient.shared.fetch(with: APIService.country.request, objectType: .general) { [weak self] (result: Result<CountryModal?, APIError>) in
            Spinner.stop()
            switch result {
            case .success(let data):
                self?.countryArray = data
            case .failure(let error):
                SwiftMessage.showBanner(title: "Error", message: error.localizedDescription, type: .error)
            }
        }
    }
}
