//
//  KundaliVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/13/21.
//

import UIKit
import IBAnimatable

class KundaliVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    
    var KundaliArray : KundaliModal?
    var AMorPMArray = ["AM", "PM"]
    var manglik = "dont"
    var rashiID = 0
    var gotraID = 0
    var isHourValid = false
    var isMinuteValid = false
    
    var hours = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    var minutes = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "64", "55", "56", "57", "58", "59", "60"]
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnDontKnow: UIButton!
    @IBOutlet weak var txtHour: AnimatableTextField!
    @IBOutlet weak var txtMinute: AnimatableTextField!
    @IBOutlet weak var txtAMOrPM: AnimatableTextField!
    @IBOutlet weak var txtBirthPlace: AnimatableTextField!
    @IBOutlet weak var txtGotraName: AnimatableTextField!
    @IBOutlet weak var txtRashi: AnimatableTextField!
    @IBOutlet weak var txtAboutMe: AnimatableTextView!
    @IBOutlet weak var txtCriticalDetails: AnimatableTextView!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtHour.text == ""{
            txtHour.attributedPlaceholder = NSAttributedString(string: "Hour required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtHour.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtMinute.text == ""{
            txtMinute.attributedPlaceholder = NSAttributedString(string: "Minute required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMinute.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtAMOrPM.text == ""{
            txtAMOrPM.attributedPlaceholder = NSAttributedString(string: "Required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtAMOrPM.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtBirthPlace.text == ""{
            txtBirthPlace.attributedPlaceholder = NSAttributedString(string: "Birth Place required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtBirthPlace.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtGotraName.text == ""{
            txtGotraName.attributedPlaceholder = NSAttributedString(string: "Gotra name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtGotraName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtRashi.text == ""{
            txtRashi.attributedPlaceholder = NSAttributedString(string: "Rashi required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtRashi.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtHour.text == ""{
            txtHour.attributedPlaceholder = NSAttributedString(string: "Hour required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtHour.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtMinute.text == ""{
            txtMinute.attributedPlaceholder = NSAttributedString(string: "Minute required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMinute.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnYesAction(_ sender: Any) {
        btnYes.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnNo.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnDontKnow.setImage(UIImage(named: "btnUnselected"), for: .normal)
        manglik = "yes"
    }
    
    @IBAction func btnNoAction(_ sender: Any) {
        btnNo.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnYes.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnDontKnow.setImage(UIImage(named: "btnUnselected"), for: .normal)
        manglik = "No"
    }
    
    @IBAction func btnDontKnowAction(_ sender: Any) {
        btnDontKnow.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnNo.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnYes.setImage(UIImage(named: "btnUnselected"), for: .normal)
        manglik = "dont" 
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        if isValid(){
            requestKundaliUpdate()
        }else {
            let alert = UIAlertController(title: "Fields Required.", message: "Some required fields are empty.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kundaliProfile()
        setupPicker()
        navigationController?.navigationBar.isHidden = true
        updateTextFieldValue()
    }
    
    func updateTextFieldValue(){
        
        if UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.is_mangalic == "yes" {
            btnYesAction(self)
        }else if UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.is_mangalic == "No"{
            btnNoAction(self)
        }else{
            btnDontKnowAction(self)
        }
        
        txtBirthPlace.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.birth_place
        txtGotraName.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.gotra_name
        self.gotraID = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.gotra_id ?? 0
        txtRashi.text = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.rashi_name
        self.rashiID = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.rashi_id ?? 0
        manglik = "\(UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.is_mangalic ?? "dont")"
        txtAboutMe.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.about
        txtCriticalDetails.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.extra
        let timeOfBirth = UserDefaultManager.shared.userDetailsModel?.profile_kundalini_detail?.time_of_birth?.components(separatedBy: ":")
        let hourTime = timeOfBirth?[0]
        let minuteTime = timeOfBirth?[1]
        txtMinute.text = minuteTime
        
        guard let intHour = Int(hourTime ?? "") else { return }
        if intHour > 12 {
            print("Hour is in 24 hours format")
            txtHour.text = "\(intHour - 12)"
        } else {
            txtHour.text = hourTime
        }
        
        if hourTime ?? "" < "\(12)"{
            txtAMOrPM.text = "AM"
        }else{
            txtAMOrPM.text = "PM"
        }
    }
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtAMOrPM.delegate = self
        txtGotraName.delegate = self
        txtRashi.delegate = self
        txtHour.delegate = self
        txtMinute.delegate = self
        self.pickerView = picker
        return picker
    }()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtHour {
            txtHour.text = "\(hours[0])"
        } else if textField == txtMinute {
            txtMinute.text = "\(minutes[0])"
        } else if textField == txtAMOrPM {
            txtAMOrPM.text = "\(AMorPMArray[0])"
        } else if textField == txtGotraName {
            txtGotraName.text = "\(KundaliArray?.data?.gotra?[0].name ?? "")"
            gotraID = KundaliArray?.data?.gotra?[0].id ?? 0
        } else if textField == txtRashi {
            txtRashi.text = "\(KundaliArray?.data?.rashi?[0].name ?? "")"
            rashiID = KundaliArray?.data?.rashi?[0].id ?? 0
        }
        return true
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtHour.isFirstResponder{
            return hours.count
        }else if txtMinute.isFirstResponder{
            return minutes.count
        }else if txtAMOrPM.isFirstResponder{
            return AMorPMArray.count
        }else if txtGotraName.isFirstResponder{
            return KundaliArray?.data?.gotra?.count ?? 0
        }else if txtRashi.isFirstResponder{
            return KundaliArray?.data?.rashi?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtHour.isFirstResponder{
            return hours[row]
        }else if txtMinute.isFirstResponder{
            return minutes[row]
        }else if txtAMOrPM.isFirstResponder{
            return AMorPMArray[row]
        }else if txtGotraName.isFirstResponder{
            return KundaliArray?.data?.gotra?[row].name ?? ""
        }else if txtRashi.isFirstResponder{
            return KundaliArray?.data?.rashi?[row].name ?? ""
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtHour.isFirstResponder{
            let itemSelected = hours[row]
            txtHour.text = itemSelected
        }else if txtMinute.isFirstResponder{
            let itemSelected = minutes[row]
            txtMinute.text = itemSelected
        }else if txtAMOrPM.isFirstResponder{
            let itemselected = AMorPMArray[row]
            txtAMOrPM.text = itemselected
        }else if txtGotraName.isFirstResponder{
            let itemselected = KundaliArray?.data?.gotra?[row].name ?? ""
            self.gotraID = KundaliArray?.data?.gotra?[row].id ?? 0
            txtGotraName.text = itemselected
        }else if txtRashi.isFirstResponder{
            let itemselected = KundaliArray?.data?.rashi?[row].name ?? ""
            self.rashiID = KundaliArray?.data?.rashi?[row].id ?? 0
            txtRashi.text = itemselected
        }
    }
    
    private func setupPicker(){
        txtAMOrPM.inputView = pickerView
        txtGotraName.inputView = pickerView
        txtRashi.inputView = pickerView
        txtHour.inputView = pickerView
        txtMinute.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension KundaliVC {
    func kundaliProfile() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.Kundali.request, objectType: .general) { [weak self] (result: Result<KundaliModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.KundaliArray = data
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension KundaliVC {
    func requestKundaliUpdate() {
        if Reachability.isConnectedToNetwork(){
            let hour = Int(txtHour.text ?? "") ?? 0
            var timeOfBirth = ""
            if txtAMOrPM.text == "PM"{
                timeOfBirth = "\(hour + 12):" + "\(txtMinute.text!):00"
            }else{
                timeOfBirth = "\(hour):" + "\(txtMinute.text!):00"
            }
            
            let params : [String : String] = [
                "time_of_birth": "\(timeOfBirth)",
                "rashi_id": "\(rashiID)",
                "gotra_id": "\(gotraID)",
                "is_mangalic": "\(manglik)",
                "about_me": txtAboutMe.text!,
                "other_critical_detalis": txtCriticalDetails.text!,
                "birth_place": txtBirthPlace.text!,
            ]
            
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
                        
            Spinner.start()
            APIClient.shared.fetch(with: APIService.updateKundali(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<UpdateGeneralModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Kundali Updated Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        UserDefaultManager.shared.redirectToBuild = false
                        if KundaliHandleAfterLogin.shared.isFromLogin {
                            if KundaliHandleAfterLogin.shared.isFromProfileEdit {
                                self.navigationController?.popViewController(animated: true)
                            } else {
                                let vc = FindSomeoneVC.instantiate(fromAppStoryboard: .FindSomeone)
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        } else {
                            self.kundaliUpdateSuccess()
                        }
                    
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func kundaliUpdateSuccess(){
        let vc = FindSomeoneVC.instantiate(fromAppStoryboard: .FindSomeone)
        self.navigationController?.pushViewController(vc, animated: true)    }
}


