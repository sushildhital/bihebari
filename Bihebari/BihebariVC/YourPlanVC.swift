//
//  YourPlanVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/12/21.
//

import UIKit

class YourPlanVC: UIViewController {
    
    @IBOutlet weak var tblYourPlan: UITableView!
    @IBOutlet weak var lblPlan: UILabel!
    @IBOutlet weak var lblPurchaseDate: UILabel!
    
    var packageModal: [Packages]?
    var selectedIndex = 0
        
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnYourPlanAction(_ sender: Any) {
        let vc = UpgradePackageVC.instantiate(fromAppStoryboard: .UpgradePackage)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.currentPlan()
        print("package modal:\(packageModal)")
        print("Name::::\(packageModal?[0].name)")
        print("Number Of Months:-\(packageModal?[selectedIndex].number_of_months ?? 0)")
        print("Package Type:-\(UserDefaultManager.shared.userDetailsModel?.package?.package_id ?? 0)")
        print("Package Expiry Date:-\(UserDefaultManager.shared.userDetailsModel?.package?.expiry_date ?? "")")
        
        if UserDefaultManager.shared.userDetailsModel?.package?.type == "full"{
            lblPlan.text = "Your Plan for \(packageModal?[selectedIndex].number_of_months ?? 0) months is active now."
            lblPurchaseDate.text = "(From \(UserDefaultManager.shared.userDetailsModel?.package?.transition_date ?? "")" + " to \(UserDefaultManager.shared.userDetailsModel?.package?.expiry_date ?? ""))"
        }else if UserDefaultManager.shared.userDetailsModel?.package?.type == "trial"{
            lblPlan.text = "Your Trial Version is active."
            lblPurchaseDate.text = "Your Trial Version will expire on \(UserDefaultManager.shared.userDetailsModel?.package?.expiry_date ?? "")."
        }
        
        tblYourPlan.register(UINib(nibName: "CellYourPlan", bundle: nil), forCellReuseIdentifier: "CellYourPlan")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension YourPlanVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if UserDefaultManager.shared.userDetailsModel?.package?.type == "trial"{
            print("null")
        }else{
            
            return packageModal?[0].features?.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellYourPlan", for: indexPath) as! CellYourPlan
        cell.lblPlanName.text = packageModal?[0].features?[indexPath.row].value

        if let colorCode = packageModal?[0].features?[indexPath.row].color_code{
            if colorCode == "cyan"{
                cell.viewImage.backgroundColor = UIColor.init(hexString: "##44CBC6")
            }else if colorCode == "purple"{
                cell.viewImage.backgroundColor = UIColor.init(hexString: "#273691")
            }else{
                cell.viewImage.backgroundColor = UIColor.init(hexString: "#CC1C29")
            }
        }
        
        return cell
    }
    
}

extension YourPlanVC {
    func currentPlan() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.packageUpgrade.request, objectType: .general) { [weak self] (result: Result<UpgradePackageModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    let filterredArray = data?.data?.packages?.filter{ $0.id == UserDefaultManager.shared.userDetailsModel?.package?.package_id }
                        self?.packageModal = filterredArray
                        self?.tblYourPlan.reloadData()
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}


