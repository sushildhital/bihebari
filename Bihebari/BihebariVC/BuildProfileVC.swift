//
//  BuildProfileVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/13/21.
//

import UIKit
import FittedSheets
import IBAnimatable

class BuildProfileVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    
    var buildProfileArray : CurrentlyLivingModal?
    var dietArray = ["Veg", "Non-Veg"]
    var selectedIndexDistrict = 0
    var selectedIndexMunicipality = 0
    var selectedIndexWardNo = 0
    var gender = String()
    var tempCountryID: Int?
    var permanentCountryID: Int?
    var motherTonID: Int?
    var religionID: Int?
    var casteID: Int?
    var dobID: String?
    var skinColorID: Int?
    var hairColorID: Int?
    var eyeColorID: Int?
    var perCountryId: Int?
    
    @IBOutlet weak var txtFirstName: AnimatableTextField!
    @IBOutlet weak var txtMiddleName: AnimatableTextField!
    @IBOutlet weak var txtLastName: AnimatableTextField!
    @IBOutlet weak var txtDateOfBirth: AnimatableTextField!
    @IBOutlet weak var txtMaritalStatus: AnimatableTextField!
    @IBOutlet weak var txtPermanentProvince: AnimatableTextField!
    @IBOutlet weak var txtPermanentDistrict: AnimatableTextField!
    @IBOutlet weak var txtPermanentMunicipality: AnimatableTextField!
    @IBOutlet weak var txtPermanentWardNo: AnimatableTextField!
    @IBOutlet weak var txtPermanentCity: AnimatableTextField!
    @IBOutlet weak var txtCurrentProvince: AnimatableTextField!
    @IBOutlet weak var txtCurrentDistrict: AnimatableTextField!
    @IBOutlet weak var txtCurrentMunicipality: AnimatableTextField!
    @IBOutlet weak var txtCurrentWardNo: AnimatableTextField!
    @IBOutlet weak var txtTemporaryCity: AnimatableTextField!
    @IBOutlet weak var txtReligion: AnimatableTextField!
    @IBOutlet weak var txtCaste: AnimatableTextField!
    @IBOutlet weak var txtMotherTongue: AnimatableTextField!
    @IBOutlet weak var txtDiet: AnimatableTextField!
    @IBOutlet weak var txtHeightInFeet: AnimatableTextField!
    @IBOutlet weak var txtHeightInInch: AnimatableTextField!
    @IBOutlet weak var txtWeight: AnimatableTextField!
    @IBOutlet weak var txtEyeColor: AnimatableTextField!
    @IBOutlet weak var txtHairColor: AnimatableTextField!
    @IBOutlet weak var txtSkinColor: AnimatableTextField!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var tfPermanentCountry: AnimatableTextField!
    @IBOutlet weak var tfTempCountry: AnimatableTextField!
    @IBOutlet weak var sameAddressBtn: UIButton!
    @IBOutlet weak var checkBoxImg: UIImageView!
    
    var sameAddressClicked: Bool = false {
        didSet {
            if sameAddressClicked {
                checkBoxImg.image = UIImage(named: "checkbox")
                self.sameAddressCopied()
            } else {
                checkBoxImg.image = UIImage(named: "unChecked")
                self.notSameAddress()
            }
        }
    }
    
    // MARK: -
    // MARK: Private Utility Methods
    
    // MARK:- Checkbox function
    func sameAddressCopied() {
        tfTempCountry.text = tfPermanentCountry.text
        txtCurrentProvince.text = txtPermanentProvince.text
        txtCurrentDistrict.text = txtPermanentDistrict.text
        txtCurrentMunicipality.text = txtCurrentMunicipality.text
        txtCurrentWardNo.text = txtPermanentWardNo.text
        txtTemporaryCity.text = txtPermanentCity.text
        txtCurrentMunicipality.text = txtPermanentMunicipality.text
        
        self.tempCountryID = self.perCountryId
    }
    
    func notSameAddress() {
        tfTempCountry.text = ""
        txtCurrentProvince.text = ""
        txtCurrentDistrict.text = ""
        txtCurrentMunicipality.text = ""
        txtCurrentWardNo.text = ""
        txtTemporaryCity.text = ""
        
        self.tempCountryID = 0
    }
    
    func defaultCountryID() {
        if self.perCountryId == 153 {
            permanentPicker()
        } else {
            print("Do nothing, NO Permanent Picker")
            txtPermanentProvince.inputView = nil
            txtPermanentDistrict.inputView = nil
            txtPermanentMunicipality.inputView = nil
            txtPermanentWardNo.inputView = nil
        }
    }
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtFirstName.text == ""{
            txtFirstName.attributedPlaceholder = NSAttributedString(string: "First name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtFirstName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtLastName.text == ""{
            txtLastName.attributedPlaceholder = NSAttributedString(string: "Last name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtLastName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtDateOfBirth.text == ""{
            txtDateOfBirth.attributedPlaceholder = NSAttributedString(string: "Date of birth required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtDateOfBirth.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtMaritalStatus.text == ""{
            txtMaritalStatus.attributedPlaceholder = NSAttributedString(string: "Marital status required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMaritalStatus.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if tfPermanentCountry.text == ""{
            tfPermanentCountry.attributedPlaceholder = NSAttributedString(string: "Country required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            tfPermanentCountry.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPermanentProvince.text == ""{
            txtPermanentProvince.attributedPlaceholder = NSAttributedString(string: "Province required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtPermanentProvince.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPermanentDistrict.text == ""{
            txtPermanentDistrict.attributedPlaceholder = NSAttributedString(string: "District required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtPermanentDistrict.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPermanentMunicipality.text == ""{
            txtPermanentMunicipality.attributedPlaceholder = NSAttributedString(string: "Municipality required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtPermanentMunicipality.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPermanentWardNo.text == ""{
            txtPermanentWardNo.attributedPlaceholder = NSAttributedString(string: "Ward number required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtPermanentWardNo.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPermanentCity.text == ""{
            txtPermanentCity.attributedPlaceholder = NSAttributedString(string: "City required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtPermanentCity.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if tfTempCountry.text == ""{
            tfTempCountry.attributedPlaceholder = NSAttributedString(string: "Country required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            tfTempCountry.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCurrentProvince.text == ""{
            txtCurrentProvince.attributedPlaceholder = NSAttributedString(string: "Province required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCurrentProvince.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCurrentDistrict.text == ""{
            txtCurrentDistrict.attributedPlaceholder = NSAttributedString(string: "District required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCurrentDistrict.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCurrentMunicipality.text == ""{
            txtCurrentMunicipality.attributedPlaceholder = NSAttributedString(string: "Municipality required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCurrentMunicipality.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCurrentWardNo.text == ""{
            txtCurrentWardNo.attributedPlaceholder = NSAttributedString(string: "Ward number required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCurrentWardNo.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtTemporaryCity.text == ""{
            txtTemporaryCity.attributedPlaceholder = NSAttributedString(string: "City required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtTemporaryCity.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtReligion.text == ""{
            txtReligion.attributedPlaceholder = NSAttributedString(string: "Religion required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtReligion.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCaste.text == ""{
            txtCaste.attributedPlaceholder = NSAttributedString(string: "Caste required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCaste.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtMotherTongue.text == ""{
            txtMotherTongue.attributedPlaceholder = NSAttributedString(string: "Mother Tongue required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMotherTongue.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtDiet.text == ""{
            txtDiet.attributedPlaceholder = NSAttributedString(string: "Diet required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtDiet.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtHeightInFeet.text == ""{
            txtHeightInFeet.attributedPlaceholder = NSAttributedString(string: "Height required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtHeightInFeet.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtHeightInInch.text == ""{
            txtHeightInInch.attributedPlaceholder = NSAttributedString(string: "Height required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtHeightInInch.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtWeight.text == ""{
            txtWeight.attributedPlaceholder = NSAttributedString(string: "Weight required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtWeight.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtEyeColor.text == ""{
            txtEyeColor.attributedPlaceholder = NSAttributedString(string: "Eye color required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtEyeColor.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtHairColor.text == ""{
            txtHairColor.attributedPlaceholder = NSAttributedString(string: "Hair color required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtHairColor.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtSkinColor.text == ""{
            txtSkinColor.attributedPlaceholder = NSAttributedString(string: "Skin color required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtSkinColor.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnContinueAction(_ sender: Any) {
        if isValid(){
            requestForGeneralUpdate()
        }else {
            let alert = UIAlertController(title: "Fields Required.", message: "Some required fields are empty.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnMaleAction(_ sender: Any) {
        btnMale.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnFemale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnOther.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "male"
    }
    
    @IBAction func btnFemaleAction(_ sender: Any) {
        btnFemale.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnMale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnOther.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "female"
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        btnOther.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnMale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnFemale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "others"
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildProfile()
        setupPicker()
        countryPicker()
        navigationController?.navigationBar.isHidden = true
        self.txtDateOfBirth.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        updateTextfieldData()
        sameAddressBtn.addTarget(self, action: #selector(sameAddBtnTapped), for: .touchUpInside)
    }
    
    @objc func sameAddBtnTapped() {
        sameAddressClicked = !sameAddressClicked
    }
    
    @objc func tapDone() {
        if let datePicker = self.txtDateOfBirth.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            //            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "YYYY-MM-dd"
            self.txtDateOfBirth.text = dateformatter.string(from: datePicker.date)
            self.dobID = self.txtDateOfBirth.text
        }
        self.txtDateOfBirth.resignFirstResponder()
    }
    
    func updateTextfieldData(){
        tfPermanentCountry.text = UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.country_name
        self.perCountryId = UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.country_id
        defaultCountryID()
        tfTempCountry.text = UserDefaultManager.shared.userDetailsModel?.get_current_address?.country_name
        self.tempCountryID = UserDefaultManager.shared.userDetailsModel?.get_current_address?.country_id ?? 0
        self.motherTonID = UserDefaultManager.shared.userDetailsModel?.profile_general?.mother_tongue_id
        self.religionID = UserDefaultManager.shared.userDetailsModel?.profile_general?.religion_id
        self.casteID = UserDefaultManager.shared.userDetailsModel?.profile_general?.caste_id
        self.dobID = UserDefaultManager.shared.userDetailsModel?.profile_general?.dob
        self.skinColorID = UserDefaultManager.shared.userDetailsModel?.profile_figure?.skin_color_id
        self.hairColorID = UserDefaultManager.shared.userDetailsModel?.profile_figure?.hair_color_id
        self.eyeColorID = UserDefaultManager.shared.userDetailsModel?.profile_figure?.eye_color_id
        txtFirstName.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.first_name
        txtMiddleName.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.middle_name
        txtLastName.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.last_name
        txtDateOfBirth.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.dob
        txtMaritalStatus.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.marrital_status
        gender = UserDefaultManager.shared.userDetailsModel?.profile_general?.gender ?? ""
        txtPermanentProvince.text = UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.state
        txtPermanentDistrict.text = UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.district
        txtPermanentMunicipality.text = UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.muncipality_vdc
        txtPermanentWardNo.text = "\(UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.ward_number ?? 0)"
        txtPermanentCity.text = UserDefaultManager.shared.userDetailsModel?.get_permanent_address?.city
        txtCurrentProvince.text = UserDefaultManager.shared.userDetailsModel?.get_current_address?.state
        txtCurrentDistrict.text = UserDefaultManager.shared.userDetailsModel?.get_current_address?.district
        txtCurrentMunicipality.text = UserDefaultManager.shared.userDetailsModel?.get_current_address?.muncipality_vdc
        txtCurrentWardNo.text = "\(UserDefaultManager.shared.userDetailsModel?.get_current_address?.ward_number ?? 0)"
        txtTemporaryCity.text = UserDefaultManager.shared.userDetailsModel?.get_current_address?.city
        txtReligion.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.relegion_name
        txtCaste.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.caste_name
        txtMotherTongue.text = UserDefaultManager.shared.userDetailsModel?.profile_general?.mother_tounge_name
        if UserDefaultManager.shared.userDetailsModel?.profile_figure?.vegetarian == "yes"{
            txtDiet.text = "Veg"
        }else if UserDefaultManager.shared.userDetailsModel?.profile_figure?.vegetarian == "no"{
            txtDiet.text = "Non-Veg"
        }
        txtHeightInFeet.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_figure?.height_ft ?? 0)"
        txtHeightInInch.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_figure?.height_in ?? 0)"
        txtWeight.text = "\(UserDefaultManager.shared.userDetailsModel?.profile_figure?.weight_kg ?? 0)"
        txtEyeColor.text = UserDefaultManager.shared.userDetailsModel?.profile_figure?.eye_color_name
        txtHairColor.text = UserDefaultManager.shared.userDetailsModel?.profile_figure?.hair_color_name
        txtSkinColor.text = UserDefaultManager.shared.userDetailsModel?.profile_figure?.skin_color_name
    }
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtMaritalStatus.delegate = self
        txtPermanentProvince.delegate = self
        txtPermanentDistrict.delegate = self
        txtPermanentMunicipality.delegate = self
        txtPermanentWardNo.delegate = self
        txtCurrentProvince.delegate = self
        txtCurrentDistrict.delegate = self
        txtCurrentMunicipality.delegate = self
        txtCurrentWardNo.delegate = self
        txtReligion.delegate = self
        txtCaste.delegate = self
        txtMotherTongue.delegate = self
        txtDiet.delegate = self
        txtEyeColor.delegate = self
        txtHairColor.delegate = self
        txtSkinColor.delegate = self
        tfPermanentCountry.delegate = self
        tfTempCountry.delegate = self
        self.pickerView = picker
        return picker
    }()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == tfPermanentCountry {
//            let itemselected = buildProfileArray?.data?.countries?[0].country_name ?? ""
//            tfPermanentCountry.text = "\(itemselected)"
//        } else if textField == tfTempCountry {
//            let itemselected = buildProfileArray?.data?.countries?[0].country_name ?? ""
//            tfTempCountry.text = "\(itemselected)"
//        }
        self.pickerView.reloadAllComponents()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfPermanentCountry{
            tfPermanentCountry.text = "\(buildProfileArray?.data?.countries?[0].country_name ?? "")"
            perCountryId = buildProfileArray?.data?.countries?[0].id ?? 0
            txtPermanentProvince.text = ""
            txtPermanentDistrict.text = ""
            txtPermanentMunicipality.text = ""
            self.defaultCountryID()
        }else if textField == txtPermanentProvince {
            if tfPermanentCountry.text == "Nepal" {
                if tfPermanentCountry.text != "" {
                    txtPermanentProvince.text = "\(buildProfileArray?.data?.provincesData?[0].name ?? "")"
                }else {
                    tfPermanentCountry.text = "Select a country first!"
                    return false
                }
            }
        }else if textField == txtPermanentDistrict {
            if tfPermanentCountry.text == "Nepal" {
                if txtPermanentProvince.text != "" {
                    txtPermanentDistrict.text = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[0].name ?? "")"
                } else {
//                    txtPermanentProvince.text = "Select a province first!"
                    txtPermanentProvince.placeholder = "Select a province first!"
                    return false
                }
            }
        }else if textField == txtPermanentMunicipality {
            if tfPermanentCountry.text == "Nepal" {
                if txtPermanentDistrict.text != "" {
                    txtPermanentMunicipality.text = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[0].name ?? "")"
                } else {
                    
                    txtPermanentDistrict.placeholder = "Select a district first!"
                    return false
                    
                }
            }
        }else if textField == txtPermanentWardNo {
            if tfPermanentCountry.text == "Nepal" {
                if txtPermanentMunicipality.text != "" {
                    txtPermanentWardNo.text = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?[0] ?? 0)"
                } else {
                    txtPermanentMunicipality.placeholder = "Select a municipality first!"
                    return false
                }
            }
        }
        
        else if textField == tfTempCountry{
            tfTempCountry.text = "\(buildProfileArray?.data?.countries?[0].country_name ?? "")"
            tempCountryID = buildProfileArray?.data?.countries?[0].id ?? 0
            txtCurrentProvince.text = ""
            txtCurrentDistrict.text = ""
            txtCurrentMunicipality.text = ""
            self.tempPicker()
        }else if textField == txtCurrentProvince {
            if tfTempCountry.text == "Nepal" {
                if tfTempCountry.text != "" {
                    txtCurrentProvince.text = "\(buildProfileArray?.data?.provincesData?[0].name ?? "")"
                }else {
                    tfTempCountry.placeholder = "Select a country first!"
                    return false
                }
            }
        }else if textField == txtCurrentDistrict {
            if tfTempCountry.text == "Nepal" {
                if txtCurrentProvince.text != "" {
                txtCurrentDistrict.text = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[0].name ?? "")"
                } else {
                txtCurrentProvince.placeholder = "Select a province first!"
                    return false
                }
            }
        }else if textField == txtCurrentMunicipality {
            if tfTempCountry.text == "Nepal" {
                if txtCurrentDistrict.text != "" {
                    txtCurrentMunicipality.text = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[0].name ?? "")"
                } else {
                    txtCurrentDistrict.placeholder = "Select a district first!"
                    return false
                }
            }
        }else if textField == txtCurrentWardNo {
            if tfTempCountry.text == "Nepal" {
                if txtCurrentMunicipality.text != "" {
                    txtCurrentWardNo.text = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?[0] ?? 0)"
                } else {
                    txtCurrentMunicipality.placeholder = "Select a municipality first!"
                    return false
                }
            }
        }
        
        else if textField == txtReligion {
            txtReligion.text = "\(buildProfileArray?.data?.religions?[0].name ?? "")"
            religionID = buildProfileArray?.data?.religions?[0].id ?? 0
        }else if textField == txtCaste {
            txtCaste.text = "\(buildProfileArray?.data?.casts?[0].name ?? "")"
            casteID = buildProfileArray?.data?.casts?[0].id ?? 0
        }else if textField == txtDiet {
            txtDiet.text = "\(dietArray[0])"
        }else if textField == txtEyeColor {
            txtEyeColor.text = "\(buildProfileArray?.data?.eye_color?[0].name ?? "")"
            eyeColorID = buildProfileArray?.data?.eye_color?[0].id ?? 0
        }else if textField == txtHairColor {
            txtHairColor.text = "\(buildProfileArray?.data?.hair_color?[0].name ?? "")"
            hairColorID = buildProfileArray?.data?.hair_color?[0].id ?? 0
        }else if textField == txtSkinColor {
            txtSkinColor.text = "\(buildProfileArray?.data?.skin?[0].name ?? "")"
            skinColorID = buildProfileArray?.data?.skin?[0].id ?? 0
        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtMaritalStatus.isFirstResponder{
            return buildProfileArray?.data?.marital_status?.count ?? 0
        }else if txtPermanentProvince.isFirstResponder{
            return buildProfileArray?.data?.provincesData?.count ?? 0
        }else if txtPermanentDistrict.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?.count ?? 0
        }else if txtPermanentMunicipality.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?.count ?? 0
        }else if txtPermanentWardNo.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?.count ?? 0
        }else if txtCurrentProvince.isFirstResponder{
            return buildProfileArray?.data?.provincesData?.count ?? 0
        }else if txtCurrentDistrict.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?.count ?? 0
        }else if txtCurrentMunicipality.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?.count ?? 0
        }else if txtCurrentWardNo.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?.count ?? 0
        }else if txtReligion.isFirstResponder{
            return buildProfileArray?.data?.religions?.count ?? 0
        }else if txtCaste.isFirstResponder{
            return buildProfileArray?.data?.casts?.count ?? 0
        }else if txtMotherTongue.isFirstResponder{
            return buildProfileArray?.data?.mother_tongues?.count ?? 0
        }else if txtDiet.isFirstResponder{
            return dietArray.count
        }else if txtEyeColor.isFirstResponder{
            return buildProfileArray?.data?.eye_color?.count ?? 0
        }else if txtHairColor.isFirstResponder{
            return buildProfileArray?.data?.hair_color?.count ?? 0
        }else if txtSkinColor.isFirstResponder{
            return buildProfileArray?.data?.skin?.count ?? 0
        } else if tfPermanentCountry.isFirstResponder {
            return buildProfileArray?.data?.countries?.count ?? 0
        } else if tfTempCountry.isFirstResponder {
            return buildProfileArray?.data?.countries?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtMaritalStatus.isFirstResponder{
            return buildProfileArray?.data?.marital_status?[row] ?? ""
        }else if txtPermanentProvince.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[row].name ?? ""
        }else if txtPermanentDistrict.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[row].name ?? ""
        }else if txtPermanentMunicipality.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[row].name ?? ""
        }else if txtPermanentWardNo.isFirstResponder{
            return "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?[row] ?? 0)"
        }else if txtCurrentProvince.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[row].name ?? ""
        }else if txtCurrentDistrict.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[row].name ?? ""
        }else if txtCurrentMunicipality.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[row].name ?? ""
        }else if txtCurrentWardNo.isFirstResponder{
            return "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?[row] ?? 0)"
        }else if txtReligion.isFirstResponder{
            return buildProfileArray?.data?.religions?[row].name ?? ""
        }else if txtCaste.isFirstResponder{
            return buildProfileArray?.data?.casts?[row].name ?? ""
        }else if txtMotherTongue.isFirstResponder{
            return buildProfileArray?.data?.mother_tongues?[row].name ?? ""
        }else if txtDiet.isFirstResponder{
            return dietArray[row]
        }else if txtEyeColor.isFirstResponder{
            return buildProfileArray?.data?.eye_color?[row].name ?? ""
        }else if txtHairColor.isFirstResponder{
            return buildProfileArray?.data?.hair_color?[row].name ?? ""
        }else if txtSkinColor.isFirstResponder{
            return buildProfileArray?.data?.skin?[row].name ?? ""
        } else if tfPermanentCountry.isFirstResponder {
            return buildProfileArray?.data?.countries?[row].country_name ?? ""
        } else if tfTempCountry.isFirstResponder {
            return buildProfileArray?.data?.countries?[row].country_name ?? ""
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtMaritalStatus.isFirstResponder{
            let itemselected = buildProfileArray?.data?.marital_status?[row] ?? ""
            txtMaritalStatus.text = itemselected
        }else if txtPermanentProvince.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[row].name ?? ""
            txtPermanentProvince.text = itemselected
            selectedIndexDistrict = row
        }else if txtPermanentDistrict.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[row].name ?? ""
            txtPermanentDistrict.text = itemselected
            selectedIndexMunicipality = row
        }else if txtPermanentMunicipality.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[row].name ?? ""
            txtPermanentMunicipality.text = itemselected
            selectedIndexWardNo = row
        }else if txtPermanentWardNo.isFirstResponder{
            let itemselected = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?[row] ?? 0)"
            txtPermanentWardNo.text = itemselected
        }else if txtCurrentProvince.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[row].name ?? ""
            txtCurrentProvince.text = itemselected
            selectedIndexDistrict = row
        }else if txtCurrentDistrict.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[row].name ?? ""
            txtCurrentDistrict.text = itemselected
            selectedIndexMunicipality = row
        }else if txtCurrentMunicipality.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[row].name ?? ""
            txtCurrentMunicipality.text = itemselected
            selectedIndexWardNo = row
        }else if txtCurrentWardNo.isFirstResponder{
            let itemselected = "\(buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[selectedIndexWardNo].wards?[row] ?? 0)"
            txtCurrentWardNo.text = itemselected
        }else if txtReligion.isFirstResponder{
            let itemselected = buildProfileArray?.data?.religions?[row].name ?? ""
            self.religionID = buildProfileArray?.data?.religions?[row].id
            txtReligion.text = itemselected
        }else if txtCaste.isFirstResponder{
            let itemselected = buildProfileArray?.data?.casts?[row].name ?? ""
            self.casteID = buildProfileArray?.data?.casts?[row].id ?? 0
            txtCaste.text = itemselected
        }else if txtMotherTongue.isFirstResponder{
            let itemselected = buildProfileArray?.data?.mother_tongues?[row].name ?? ""
            self.motherTonID = buildProfileArray?.data?.mother_tongues?[row].id ?? 0
            txtMotherTongue.text = itemselected
        }else if txtDiet.isFirstResponder{
            let itemselected = dietArray[row]
            txtDiet.text = itemselected
        }else if txtEyeColor.isFirstResponder{
            let itemselected = buildProfileArray?.data?.eye_color?[row].name ?? ""
            self.eyeColorID = buildProfileArray?.data?.eye_color?[row].id ?? 0
            txtEyeColor.text = itemselected
        }else if txtHairColor.isFirstResponder{
            let itemselected = buildProfileArray?.data?.hair_color?[row].name ?? ""
            self.hairColorID = buildProfileArray?.data?.hair_color?[row].id ?? 0
            txtHairColor.text = itemselected
        }else if txtSkinColor.isFirstResponder{
            let itemselected = buildProfileArray?.data?.skin?[row].name ?? ""
            self.skinColorID = buildProfileArray?.data?.skin?[row].id ?? 0
            txtSkinColor.text = itemselected
        } else if tfPermanentCountry.isFirstResponder {
            let countryName = buildProfileArray?.data?.countries?[row].country_name ?? ""
            tfPermanentCountry.text = countryName
            self.perCountryId = buildProfileArray?.data?.countries?[row].id ?? 0
            self.defaultCountryID()
        } else if tfTempCountry.isFirstResponder {
            let countryName = buildProfileArray?.data?.countries?[row].country_name ?? ""
            let countryId = buildProfileArray?.data?.countries?[row].id ?? 0
            self.tempCountryID = countryId
            tfTempCountry.text = countryName
            if countryId == 153 {
                tempPicker()
            }  else {
                print("Do nothing, NO Temporary Picker")
                txtCurrentProvince.inputView = nil
                txtCurrentDistrict.inputView = nil
                txtCurrentMunicipality.inputView = nil
                txtCurrentWardNo.inputView = nil
            }
        }
    }
    
    private func setupPicker(){
        txtMaritalStatus.inputView = pickerView
        txtReligion.inputView = pickerView
        txtCaste.inputView = pickerView
        txtMotherTongue.inputView = pickerView
        txtDiet.inputView = pickerView
        txtEyeColor.inputView = pickerView
        txtHairColor.inputView = pickerView
        txtSkinColor.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    private func permanentPicker() {
        txtPermanentProvince.inputView = pickerView
        txtPermanentDistrict.inputView = pickerView
        txtPermanentMunicipality.inputView = pickerView
        txtPermanentWardNo.inputView = pickerView
    }
    
    private func tempPicker() {
        txtCurrentProvince.inputView = pickerView
        txtCurrentDistrict.inputView = pickerView
        txtCurrentMunicipality.inputView = pickerView
        txtCurrentWardNo.inputView = pickerView
    }
    
    private func countryPicker() {
        tfPermanentCountry.inputView = pickerView
        tfTempCountry.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnMale.isSelected = true
        btnMale.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnFemale.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnOther.setImage(UIImage(named: "btnUnselected"), for: .normal)
        gender = "male"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension BuildProfileVC {
    func buildProfile() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.currentlyLivingIn.request, objectType: .general) { [weak self] (result: Result<CurrentlyLivingModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.buildProfileArray = data
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension BuildProfileVC {
    func requestForGeneralUpdate() {
        if Reachability.isConnectedToNetwork(){
            
            var veg = ""
            if txtDiet.text == "Veg"{
                veg = "yes"
            }else{
                veg = "no"
            }
            
            let params = [
                "marrital_status": txtMaritalStatus.text!,
                "temp_country_id": "\(tempCountryID ?? 0)",
                "temp_state": txtCurrentProvince.text!,
                "temp_district": txtCurrentDistrict.text!,
                "temp_muncipality_vdc": txtCurrentMunicipality.text!,
                "temp_city": txtTemporaryCity.text!,
                "temp_ward_number": txtCurrentWardNo.text!,
                "mother_tongue_id": "\(motherTonID ?? 0)",
                "vegetarian": veg,
                "height_ft": txtHeightInFeet.text!,
                "height_in": txtHeightInInch.text!,
                "gender": gender,
                "religion_id": "\(religionID ?? 0)",
                "caste_id": "\(casteID ?? 0)",
                "first_name": txtFirstName.text!,
                "middle_name": txtMiddleName.text!,
                "last_name": txtLastName.text!,
                //            "date_of_birth_bs": "2048-04-22",
                "date_of_birth_ad": "\(self.dobID ?? "")",
                "permanent_country_id": "\(self.perCountryId ?? 0)",
                "permanent_state": txtPermanentProvince.text!,
                "permanent_district": txtPermanentDistrict.text!,
                "permanent_muncipality_vdc": txtPermanentMunicipality.text!,
                "permanent_city": txtPermanentCity.text!,
                "permanent_ward_number": txtPermanentWardNo.text!,
                "weight_kg": txtWeight.text!,
                "skin_color_id": "\(skinColorID ?? 0)",
                "hair_color_id": "\(hairColorID ?? 0)",
                "eye_color_id": "\(eyeColorID ?? 0)"
            ] as [String : Any]
            
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            
            Spinner.start()
            APIClient.shared.fetch(with: APIService.updateGeneral(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<UpdateGeneralModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "General Details Updated Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        if BuildProfileHandler.shared.isFromEditProfile {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.generalInformationUpdateSuccess()
                        }
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func generalInformationUpdateSuccess(){
        let vc = EducationVC.instantiate(fromAppStoryboard: .Education)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
