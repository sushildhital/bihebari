//
//  FindSomeoneVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import IBAnimatable
import RangeSeekSlider

class FindSomeoneVC: UIViewController  {
    
    @IBOutlet weak var slider: RangeSeekSlider!
    @IBOutlet weak var lblLowerValue: UILabel!
    @IBOutlet weak var lblHigherValue: UILabel!
    @IBOutlet weak var btnBride: AnimatableButton!
    @IBOutlet weak var btnGroom: AnimatableButton!
    @IBOutlet weak var viewCast: TagListView!
    @IBOutlet weak var viewMaritalStatus: TagListView!
    
    var responseArray = [Casts]()
    var maritalArray = [Marital_status]()

    var gender = String()
    var casteList = [String]()
    var maritalList = [String]()
    
    var selectedCasteId = -1
    var selectedMaritalID = ""
    
    // MARK: -
    // MARK: Private Utility Methods
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBrideAction(_ sender: UIButton) {
        gender = "female"
        btnBrideCustomization()
    }
    
    @IBAction func btnGroomAction(_ sender: UIButton) {
        gender = "male"
        btnGroomCustomization()
    }
    
    @IBAction func btnFindMatchAction(_ sender: Any) {
        if selectedCasteId == -1{
            Util.showAlert(title: "Info!", message: "Please Choose Caste To Proceed.", view: self)
        }else{
            findSomeoneSuccess(gender:gender, minAge:lblLowerValue.text!, maxAge:lblHigherValue.text!, caste: "\(selectedCasteId)", maritalStatus: "\(selectedMaritalID)")
        }
    }
    
    func btnBrideCustomization(){
        btnBride.backgroundColor = UIColor(red: 0.79, green: 0.12, blue: 0.15, alpha: 1)
        btnBride.setTitleColor(UIColor.white, for: .normal)
        btnBride.borderColor = .none
        
        btnGroom.backgroundColor = UIColor.white
        btnGroom.borderWidth = 1
        btnGroom.borderColor = UIColor(named: "Gray-4")
        btnGroom.setTitleColor(UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1), for: .normal)
    }
    
    func btnGroomCustomization(){
        btnGroom.backgroundColor = UIColor(red: 0.79, green: 0.12, blue: 0.15, alpha: 1)
        btnGroom.setTitleColor(UIColor.white, for: .normal)
        btnGroom.borderColor = .none
        
        btnBride.backgroundColor = UIColor.white
        btnBride.borderWidth = 1
        btnBride.borderColor = UIColor(named: "Gray-4")
        btnBride.setTitleColor(UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 1), for: .normal)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gender = "female"
        btnBrideCustomization()
        findSomeone()
        slider.delegate = self
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: .default)
        navigationBar?.shadowImage = UIImage()
        navigationBar?.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension FindSomeoneVC {
    func findSomeone() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.findSomeone.request, objectType: .general) { [weak self] (result: Result<FindSomeoneModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.responseArray = data?.data?.casts ?? []
                    self?.maritalArray = data?.data?.marital_status ?? []
                    self?.casteList.removeAll()
                    self?.maritalList.removeAll()
                    
                    if let castData = self?.responseArray{
                        for data in castData{
                            self?.casteList.append(data.name ?? "")
                        }
                    }
                    
                    if let maritalData = self?.maritalArray{
                        for data in maritalData{
                            self?.maritalList.append(data.name ?? "")
                        }
                    }

                    self?.viewCast.textFont = UIFont(name: "Lato-Regular", size: 14)!
                    self?.viewCast.addTags(self?.casteList ?? [])
                    self?.viewCast.delegate = self
                    
                    self?.viewMaritalStatus.textFont = UIFont(name: "Lato-Regular", size: 14)!
                    self?.viewMaritalStatus.addTags(self?.maritalList ?? [])
                    self?.viewMaritalStatus.delegate = self
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func findSomeoneSuccess(gender:String,minAge:String,maxAge:String,caste:String, maritalStatus: String){
        print("helloCasteId: \(caste)")
        print("maritalID: \(maritalStatus)")
        UserDefaultManager.shared.selectedCaste = caste
        UserDefaultManager.shared.selectedGender = gender
        UserDefaultManager.shared.selectedMinAge = minAge
        UserDefaultManager.shared.selectedMaxAge = maxAge
        UserDefaultManager.shared.selectedMaritalStatus = maritalStatus
        UserDefaults.standard.set(true, forKey: IS_CAST_SELECTED)
        UserDefaults.standard.set(true, forKey: IS_MARITAL_SELECTED)
        let resultViewController = Helper.getDashboardWithNavBar()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = resultViewController
        appDelegate.window!.makeKeyAndVisible()
    }
}

extension FindSomeoneVC: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        for tag in sender.tagViews{
            if tag.titleLabel?.text == title{
                tag.isSelected = true
                for castData in responseArray{
                    if let cast = castData.name{
                        if cast == title{
                            selectedCasteId = castData.id ?? -1
                        }
                    }
                }
                
                for maritalData in maritalArray{
                    if let marital = maritalData.name{
                        if marital == title{
                            selectedMaritalID = maritalData.value ?? ""
                        }
                    }
                }
            }else{
                tag.isSelected = false
            }
        }
    }
}

extension FindSomeoneVC: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === self.slider {
            lblLowerValue.text = "\(String(format:"%.0f", minValue))"
            lblHigherValue.text = "\(String(format:"%.0f", maxValue))"
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
        UIDevice.vibrate()
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
