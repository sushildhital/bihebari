//
//  PaymentOption2.swift
//  Bihebari
//
//  Created by Leoan Ranjit on 11/17/21.
//

import UIKit
import StoreKit
import IBAnimatable

class PaymentOption2: UIViewController {
    
    @IBOutlet weak var btnPurchase: AnimatableButton!
    
    
    private var products = [SKProduct]()
    private var productBeingPurchased : SKProduct?
    
    var selectedPackageID = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        IAPManager.shared.fetchProducts()
        switch selectedPackageID {
        case 6:
            btnPurchase.setTitle("Basic Package $14.99", for: .normal)
            btnPurchase.backgroundColor = UIColor.init(hexString: "##44CBC6")
        case 7:
            btnPurchase.setTitle("Advance Package $24.99", for: .normal)
            btnPurchase.backgroundColor = UIColor.init(hexString: "##273691")

        case 8:
            btnPurchase.setTitle("VIP Package $44.99", for: .normal)
            btnPurchase.backgroundColor = UIColor.init(hexString: "##CC1C29")

        default:
            break
        }
        // Do any additional setup after loading the view.
    }
  
    @IBAction func btnPurchaseAction(_ sender: Any) {
        Spinner.start()
        switch selectedPackageID {
        case 6:
            IAPManager.shared.purchase(product: .threeMonths) { [weak self] productID in
                DispatchQueue.main.async {
                    self?.verifyPayment(boughtProductId: 6)
                }
                
            }
        case 7:
            IAPManager.shared.purchase(product: .sixMonths) { [weak self] productID in
                DispatchQueue.main.async {
                    self?.verifyPayment(boughtProductId: 7)
                }
                
            }
        case 8:
            IAPManager.shared.purchase(product: .oneYear) { [weak self] productID in
                DispatchQueue.main.async {
                    self?.verifyPayment(boughtProductId: 8)
                }
                
            }
        default:
            break
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func verifyPayment(boughtProductId: Int) {
        
        let param = ["package_id": "\(boughtProductId)"]
        print(param)
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.applePay(body: param).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(_):
                    SwiftMessage.showBanner(title: "Success", message: "Payment successful.", type: .success)
                    self.navigationController?.popToRootViewController(animated: true)
                    Spinner.stop()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error", message: error.localizedDescription, type: .error)
                    Spinner.stop()
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }

}
