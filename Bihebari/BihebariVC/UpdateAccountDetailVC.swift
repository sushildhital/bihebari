//
//  UpdateAccountDetailVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/26/21.
//

import UIKit
import IBAnimatable
import Photos

class UpdateAccountDetailVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
    @IBOutlet weak var txtFullName: AnimatableTextField!
    @IBOutlet weak var txtEmail: AnimatableTextField!
    @IBOutlet weak var txtPhone: AnimatableTextField!
    @IBOutlet weak var txtCountry: AnimatableTextField!
    @IBOutlet weak var txtProvince: AnimatableTextField!
    @IBOutlet weak var txtDistrict: AnimatableTextField!
    @IBOutlet weak var txtMunicipality: AnimatableTextField!
    @IBOutlet weak var txtCurrentAddress: AnimatableTextField!
    @IBOutlet weak var txtZipCode: AnimatableTextField!
    @IBOutlet weak var imgUploadPhoto: UIImageView!
    @IBOutlet weak var btnUploadPhoto: UIButton!
    
    let picker = UIImagePickerController()
    var perCountryId: Int?
    var buildProfileArray : CurrentlyLivingModal?
    var selectedIndexDistrict = 0
    var selectedIndexMunicipality = 0
    var imagePicker = UIImagePickerController()
    let avCaptureSession = AVCaptureSession()
    
    fileprivate func isValid()->Bool {
        if txtFullName.text == ""{
            txtFullName.attributedPlaceholder = NSAttributedString(string: "Full name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtFullName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtEmail.text == ""{
            txtEmail.attributedPlaceholder = NSAttributedString(string: "Email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmail.becomeFirstResponder()
            }
            return false
        }else if !Util.isValidEmail(testStr: txtEmail.text!){
            txtEmail.text = ""
            txtEmail.attributedPlaceholder = NSAttributedString(string: "Valid email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmail.becomeFirstResponder()
            }
            return false
        }else{
            txtEmail.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtPhone.text == ""{
            txtPhone.attributedPlaceholder = NSAttributedString(string: "Phone number required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtPhone.becomeFirstResponder()
            }
            return false
        }else if !Util.isValidPhone(testStr: txtPhone.text!){
            txtPhone.text = ""
            txtPhone.attributedPlaceholder = NSAttributedString(string: "Valid phone number required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtPhone.becomeFirstResponder()
            }
            return false
        }else{
            txtPhone.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCountry.text == ""{
            txtCountry.attributedPlaceholder = NSAttributedString(string: "Country required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCountry.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtProvince.text == ""{
            txtProvince.attributedPlaceholder = NSAttributedString(string: "Province required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtProvince.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtDistrict.text == ""{
            txtDistrict.attributedPlaceholder = NSAttributedString(string: "District required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtDistrict.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtMunicipality.text == ""{
            txtMunicipality.attributedPlaceholder = NSAttributedString(string: "Municipality required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMunicipality.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtCurrentAddress.text == ""{
            txtCurrentAddress.attributedPlaceholder = NSAttributedString(string: "Address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtCurrentAddress.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        return body
    }
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdateAction(_ sender: Any) {
        if isValid(){
            let alert = UIAlertController(title: "Update", message: "Are you sure you want to update?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.requestForUpdateDetail()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func btnUploadPhotoAction(_ sender: Any) {
//        let actionSheet = UIAlertController(title: "Upload Photo", message: "", preferredStyle: .actionSheet)
//        let camera = UIAlertAction(title: "Capture Photo", style: .default) { _ in
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
//                self.avCaptureSession.automaticallyConfiguresCaptureDeviceForWideColor = false
//                self.avCaptureSession.startRunning()
//                let imagePicker = UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = UIImagePickerController.SourceType.camera
//                imagePicker.allowsEditing = false
//                self.present(imagePicker, animated: true, completion: nil)
//            }
//        }
//        actionSheet.addAction(camera)
//        let gallery = UIAlertAction(title: "Select From Gallery", style: .default) { (action) in
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
//                let imagePicker = UIImagePickerController()
//                imagePicker.delegate = self
//                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//                imagePicker.allowsEditing = true
//                self.present(imagePicker, animated: true, completion: nil)
//            }
//        }
//        actionSheet.addAction(gallery)
//        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (cancel) in
//        }
//        actionSheet.addAction(cancel)
//        self.present(actionSheet, animated: true, completion: nil)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Capture Photo", style: .default, handler: { _ in
               self.openCamera()
           }))

           alert.addAction(UIAlertAction(title: "Select From Gallery", style: .default, handler: { _ in
               self.openGallary()
           }))

           alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        buildProfile()
        setupPicker()
        countryPicker()
        loadDataInTextField()
        picker.delegate = self
        disablePhoneNumberCase()
    }
    
    func disablePhoneNumberCase(){
        if UserDefaultManager.shared.userDetailsModel?.otp_status == "active" && UserDefaultManager.shared.userDetailsModel?.email_otp_status == "active"{
            txtPhone.isUserInteractionEnabled = false
        }else{
            txtPhone.isUserInteractionEnabled = true
        }
    }
    
    func loadDataInTextField(){
        txtFullName.text = UserDefaultManager.shared.userDetailsModel?.name
        txtEmail.text = UserDefaultManager.shared.userDetailsModel?.email
        txtPhone.text = UserDefaultManager.shared.userDetailsModel?.phone
        txtCountry.text = UserDefaultManager.shared.userDetailsModel?.get_account_address?.country_name
        txtProvince.text = UserDefaultManager.shared.userDetailsModel?.get_account_address?.state
        txtDistrict.text = UserDefaultManager.shared.userDetailsModel?.get_account_address?.district
        txtMunicipality.text = UserDefaultManager.shared.userDetailsModel?.get_account_address?.muncipality_vdc
        txtCurrentAddress.text = UserDefaultManager.shared.userDetailsModel?.get_account_address?.city
        txtZipCode.text = UserDefaultManager.shared.userDetailsModel?.get_account_address?.zip_code
        
        if UserDefaultManager.shared.userDetailsModel?.photo == nil{
            imgUploadPhoto.image = UIImage(named: "imgUploadPhoto")
        }else{
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")"
            imgUploadPhoto.sd_setImage(with: URL(string: url + (UserDefaultManager.shared.userDetailsModel?.photo ?? "")), completed: nil)
        }
    }
    
    private func setupPicker(){
        txtCountry.inputView = pickerView
        txtProvince.inputView = pickerView
        txtDistrict.inputView = pickerView
        txtMunicipality.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCountry.delegate = self
        txtProvince.delegate = self
        txtDistrict.delegate = self
        txtMunicipality.delegate = self
        self.pickerView = picker
        return picker
    }()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtCountry.isFirstResponder{
            return buildProfileArray?.data?.countries?.count ?? 0
        }else if txtProvince.isFirstResponder{
            return buildProfileArray?.data?.provincesData?.count ?? 0
        }else if txtDistrict.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?.count ?? 0
        }else if txtMunicipality.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtCountry.isFirstResponder{
            return buildProfileArray?.data?.countries?[row].country_name ?? ""
        }else if txtProvince.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[row].name ?? ""
        }else if txtDistrict.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[row].name ?? ""
        }else if txtMunicipality.isFirstResponder{
            return buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[row].name ?? ""
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtCountry.isFirstResponder{
            let countryName = buildProfileArray?.data?.countries?[row].country_name ?? ""
            let countryId = buildProfileArray?.data?.countries?[row].id ?? 0
            txtCountry.text = countryName
            self.defaultCountryID()
            
            if countryId == 153 {
                tempPicker()
            }  else {
                print("Do nothing, NO Temporary Picker")
                txtProvince.inputView = nil
                txtDistrict.inputView = nil
                txtMunicipality.inputView = nil
            }
            
        }else if txtProvince.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[row].name ?? ""
            txtProvince.text = itemselected
            selectedIndexDistrict = row
        }else if txtDistrict.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[row].name ?? ""
            txtDistrict.text = itemselected
            selectedIndexMunicipality = row
        }else if txtMunicipality.isFirstResponder{
            let itemselected = buildProfileArray?.data?.provincesData?[selectedIndexDistrict].districts?[selectedIndexMunicipality].municipalities?[row].name ?? ""
            txtMunicipality.text = itemselected
        }
    }
    
    private func tempPicker() {
        txtProvince.inputView = pickerView
        txtDistrict.inputView = pickerView
        txtMunicipality.inputView = pickerView
    }
    
    private func permanentPicker() {
        txtProvince.inputView = pickerView
        txtDistrict.inputView = pickerView
        txtMunicipality.inputView = pickerView
    }
    
    func defaultCountryID() {
        if self.perCountryId == 153 {
            permanentPicker()
        } else {
            print("Do nothing, NO Permanent Picker")
            txtProvince.inputView = nil
            txtDistrict.inputView = nil
            txtMunicipality.inputView = nil
        }
    }
    
    private func countryPicker() {
        txtCountry.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
}

extension UpdateAccountDetailVC {
    func buildProfile() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.currentlyLivingIn.request, objectType: .general) { [weak self] (result: Result<CurrentlyLivingModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    
                    self?.buildProfileArray = data
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension UpdateAccountDetailVC {
    func requestForUpdateDetail() {
        if Reachability.isConnectedToNetwork(){
            let myUrl = NSURL(string: "\(Constants.BASE_URL)" + "\(Constants.updateDetail)")
            let request = NSMutableURLRequest(url: myUrl! as URL)
            request.httpMethod = "POST"
            
            let params = [
                "name": txtFullName.text!,
                "phone": txtPhone.text!,
                "country": "153",
                "state": txtProvince.text!,
                "district": txtDistrict.text!,
                "city": txtCurrentAddress.text!,
                "zip_code": txtZipCode.text!,
                "muncipality_vdc": txtMunicipality.text!
            ]
            Spinner.start()
            let boundary = generateBoundaryString()
            request.setValue("Bearer \(UserDefaultManager.shared.accessToken ?? "")", forHTTPHeaderField: "Authorization")
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let imageData = imgUploadPhoto.image!.jpegData(compressionQuality: 0.7)
            if(imageData==nil)  { return; }
            request.httpBody = createBodyWithParameters(parameters: params, filePathKey: "photo", imageDataKey: imageData! as NSData, boundary: boundary) as Data
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                if error != nil {
                    SwiftMessage.showBanner(title: "Error", message: "\(String(describing: error?.localizedDescription))", type: .error)
                    Spinner.stop()
                    return
                }
                do {
                    DispatchQueue.main.async() {
                        SwiftMessage.showBanner(title: "Success", message: "Profile Detail Updated Successfully.", type: .success)
                        self.navigationController?.popViewController(animated: true)
                        self.reloadData()
                        Spinner.stop()
                    }
                }catch{
                    SwiftMessage.showBanner(title: "Error", message: "\(error.localizedDescription)", type: .error)
                }
            }
            task.resume()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.avCaptureSession.stopRunning()
        
        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            self.imgUploadPhoto.image = img
        }
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            let choosenImage1 = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            
            let data = choosenImage1.pngData()! as NSData
            data.write(toFile: localPath!, atomically: true)
        }
        dismiss(animated: true, completion: nil)
    }
    
}

extension UpdateAccountDetailVC {
    func reloadData() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<ProfileSuccessModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.userDetailsModel  = data?.data?.user
                    
                case .failure(let error):
                    Spinner.stop()
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            Spinner.stop()
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}
