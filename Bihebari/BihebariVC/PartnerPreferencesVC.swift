//
//  PartnerPreferencesVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/14/21.
//

import UIKit
import IBAnimatable
import RangeSeekSlider

class PartnerPreferencesVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
    
    var dietArray = ["veg", "non-veg"]
    var generalDetailsArray : CurrentlyLivingModal?
    var educationDetailsArray : EducationModal?
    var partnerPreferencesArray : PartnerPreferencesModal?
    var textfieldpreloaddata: LoginResponseModal?
    var timer = Timer()
    var isReadMoreTapped = false
    var religionID: Int?
    var motherTongueID: Int?
    var educationID: Int?
    var professionID: Int?
    var height: Int?
    
    var isExpanded: Bool = false
    
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var viewMore: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewExpandable: UIView!
    @IBOutlet weak var btnShort: UIButton!
    @IBOutlet weak var btnTall: UIButton!
    @IBOutlet weak var btnMedium: UIButton!
    @IBOutlet weak var btnSave: AnimatableButton!
    @IBOutlet weak var txtMaritalStatus: AnimatableTextField!
    @IBOutlet weak var txtReligion: AnimatableTextField!
    @IBOutlet weak var txtMotherTongue: AnimatableTextField!
    @IBOutlet weak var txtDiet: AnimatableTextField!
    @IBOutlet weak var txtCountry: AnimatableTextField!
    @IBOutlet weak var txtEducation: AnimatableTextField!
    @IBOutlet weak var txtEducationDegreeName: AnimatableTextField!
    @IBOutlet weak var txtProfession: AnimatableTextField!
    @IBOutlet weak var txtAnnualIncome: AnimatableTextField!
    @IBOutlet weak var slider: RangeSeekSlider!
    @IBOutlet weak var lblLowerValue: UILabel!
    @IBOutlet weak var lblHigherValue: UILabel!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtMaritalStatus.text == ""{
            txtMaritalStatus.attributedPlaceholder = NSAttributedString(string: "Marital status required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMaritalStatus.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtReligion.text == ""{
            txtReligion.attributedPlaceholder = NSAttributedString(string: "Religion required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtReligion.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtMotherTongue.text == ""{
            txtMotherTongue.attributedPlaceholder = NSAttributedString(string: "Mother tongue required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtMotherTongue.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if txtDiet.text == ""{
            txtDiet.attributedPlaceholder = NSAttributedString(string: "Diet required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            return false
        }else{
            txtDiet.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        if isExpanded{
            if txtCountry.text == ""{
                txtCountry.attributedPlaceholder = NSAttributedString(string: "Currently living in required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
                return false
            }else{
                txtCountry.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            }
            
            if txtEducation.text == ""{
                txtEducation.attributedPlaceholder = NSAttributedString(string: "Education required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
                return false
            }else{
                txtEducation.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            }
            
            if txtEducationDegreeName.text == ""{
                txtEducationDegreeName.attributedPlaceholder = NSAttributedString(string: "Education degree name required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
                return false
            }else{
                txtEducationDegreeName.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            }
            
            if txtProfession.text == ""{
                txtProfession.attributedPlaceholder = NSAttributedString(string: "Profession required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
                return false
            }else{
                txtProfession.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            }
            
            if txtAnnualIncome.text == ""{
                txtAnnualIncome.attributedPlaceholder = NSAttributedString(string: "Annual income required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
                return false
            }else{
                txtAnnualIncome.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            }
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShortAction(_ sender: Any) {
        btnShort.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnMedium.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnTall.setImage(UIImage(named: "btnUnselected"), for: .normal)
        height = partnerPreferencesArray?.data?.height_options?[0].value
    }
    
    @IBAction func btnMediumAction(_ sender: Any) {
        btnMedium.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnShort.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnTall.setImage(UIImage(named: "btnUnselected"), for: .normal)
        height = partnerPreferencesArray?.data?.height_options?[1].value
    }
    
    @IBAction func btnTallAction(_ sender: Any) {
        btnTall.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnShort.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnMedium.setImage(UIImage(named: "btnUnselected"), for: .normal)
        height = partnerPreferencesArray?.data?.height_options?[2].value
    }
    
    @IBAction func btnMoreAction(_ sender: Any) {
        isReadMoreTapped = !isReadMoreTapped
        if isReadMoreTapped{
            isExpanded = true
            
            btnMore.setTitle("Less", for: .normal)
            UIView.animate(withDuration: 1){
                self.viewExpandable.isHidden = false
            }
        }else{
            isExpanded = false
            
            btnMore.setTitle("More", for: .normal)
            UIView.animate(withDuration: 1){
                self.viewExpandable.isHidden = true
            }
        }
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        if isValid(){
            requestPartnerPreferencesUpdate()
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalDetails()
        educationDetails()
        getPartnerPreferences()
        setupPicker()
        slider.delegate = self
        height = 1
    }
    
    func preloadDataInTextField(data: PartnerPreferencesModal?){
        
        guard let religionCount = data?.data?.religions?.count else { return }
        for i in 0..<religionCount {
            let value = data?.data?.religions?[i].id
            if value == UserDefaultManager.shared.userDetailsModel?.partner_details?.religion_id{
                txtReligion.text = data?.data?.religions?[i].name
            }
        }
        
        guard let motherTongueCount = data?.data?.mother_tongue?.count else { return }
        for i in 0..<motherTongueCount {
            let value = data?.data?.mother_tongue?[i].id
            if value == UserDefaultManager.shared.userDetailsModel?.partner_details?.mother_tongue_id{
                txtMotherTongue.text = data?.data?.mother_tongue?[i].name
            }
        }
        
        guard let educationCount = data?.data?.education_degree?.count else { return }
        for i in 0..<educationCount {
            let value = data?.data?.education_degree?[i].id
            if value == UserDefaultManager.shared.userDetailsModel?.partner_details?.education_id{
                txtEducation.text = data?.data?.education_degree?[i].name
            }
        }
        
        guard let professionCount = data?.data?.profession?.count else { return }
        for i in 0..<professionCount {
            let value = data?.data?.profession?[i].id
            if value == UserDefaultManager.shared.userDetailsModel?.partner_details?.profession_id{
                txtProfession.text = data?.data?.profession?[i].name
            }
        }
        
        if UserDefaultManager.shared.userDetailsModel?.partner_details?.height == 1{
            btnShortAction(self)
        }else if UserDefaultManager.shared.userDetailsModel?.partner_details?.height == 2{
            btnMediumAction(self)
        }else{
            btnTallAction(self)
        }
        
        txtMaritalStatus.text = UserDefaultManager.shared.userDetailsModel?.partner_details?.maritial_status
        self.religionID = UserDefaultManager.shared.userDetailsModel?.partner_details?.religion_id
        self.motherTongueID = UserDefaultManager.shared.userDetailsModel?.partner_details?.mother_tongue_id
        txtDiet.text = UserDefaultManager.shared.userDetailsModel?.partner_details?.vegetarian
        txtCountry.text = UserDefaultManager.shared.userDetailsModel?.partner_details?.address
        self.educationID = UserDefaultManager.shared.userDetailsModel?.partner_details?.education_id
        txtEducationDegreeName.text = UserDefaultManager.shared.userDetailsModel?.partner_details?.education_degree_name
        self.professionID = UserDefaultManager.shared.userDetailsModel?.partner_details?.profession_id
        if UserDefaultManager.shared.userDetailsModel?.partner_details?.income == nil{
            txtAnnualIncome.text = ""
        }else{
            txtAnnualIncome.text = "\(UserDefaultManager.shared.userDetailsModel?.partner_details?.income ?? 0)"
        }
    }
    
    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtMaritalStatus.delegate = self
        txtReligion.delegate = self
        txtMotherTongue.delegate = self
        txtDiet.delegate = self
        txtEducation.delegate = self
        txtProfession.delegate = self
        self.pickerView = picker
        return picker
    }()
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerView.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if txtMaritalStatus.isFirstResponder{
            return generalDetailsArray?.data?.marital_status?.count ?? 0
        }else if txtReligion.isFirstResponder{
            return generalDetailsArray?.data?.religions?.count ?? 0
        }else if txtMotherTongue.isFirstResponder{
            return generalDetailsArray?.data?.mother_tongues?.count ?? 0
        }else if txtDiet.isFirstResponder{
            return dietArray.count
        }else if txtEducation.isFirstResponder{
            return educationDetailsArray?.data?.education_degree?.count ?? 0
        }else if txtProfession.isFirstResponder{
            return educationDetailsArray?.data?.profession?.count ?? 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if txtMaritalStatus.isFirstResponder{
            return generalDetailsArray?.data?.marital_status?[row] ?? ""
        }else if txtReligion.isFirstResponder{
            return generalDetailsArray?.data?.religions?[row].name ?? ""
        }else if txtMotherTongue.isFirstResponder{
            return generalDetailsArray?.data?.mother_tongues?[row].name ?? ""
        }else if txtDiet.isFirstResponder{
            return dietArray[row]
        }else if txtEducation.isFirstResponder{
            return educationDetailsArray?.data?.education_degree?[row].name ?? ""
        }else if txtProfession.isFirstResponder{
            return educationDetailsArray?.data?.profession?[row].name ?? ""
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if txtMaritalStatus.isFirstResponder{
            let itemselected = generalDetailsArray?.data?.marital_status?[row] ?? ""
            txtMaritalStatus.text = itemselected
        }else if txtReligion.isFirstResponder{
            let itemselected = generalDetailsArray?.data?.religions?[row].name ?? ""
            religionID = generalDetailsArray?.data?.religions?[row].id
            txtReligion.text = itemselected
        }else if txtMotherTongue.isFirstResponder{
            let itemselected = generalDetailsArray?.data?.mother_tongues?[row].name ?? ""
            motherTongueID = generalDetailsArray?.data?.mother_tongues?[row].id
            txtMotherTongue.text = itemselected
        }else if txtDiet.isFirstResponder{
            let itemselected = dietArray[row]
            txtDiet.text = itemselected
        }else if txtEducation.isFirstResponder{
            let itemselected = educationDetailsArray?.data?.education_degree?[row].name ?? ""
            educationID = educationDetailsArray?.data?.education_degree?[row].id
            txtEducation.text = itemselected
        }else if txtProfession.isFirstResponder{
            let itemselected = educationDetailsArray?.data?.profession?[row].name ?? ""
            professionID = educationDetailsArray?.data?.profession?[row].id
            txtProfession.text = itemselected
        }
    }
    
    private func setupPicker(){
        txtMaritalStatus.inputView = pickerView
        txtReligion.inputView = pickerView
        txtMotherTongue.inputView = pickerView
        txtDiet.inputView = pickerView
        txtEducation.inputView = pickerView
        txtProfession.inputView = pickerView
        self.pickerView.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewExpandable.isHidden = true
        btnShort.setImage(UIImage(named: "btnSelected"), for: .normal)
        btnMedium.setImage(UIImage(named: "btnUnselected"), for: .normal)
        btnTall.setImage(UIImage(named: "btnUnselected"), for: .normal)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension PartnerPreferencesVC {
    func generalDetails() {
        Spinner.start()
        APIClient.shared.fetch(with: APIService.currentlyLivingIn.request, objectType: .general) { [weak self] (result: Result<CurrentlyLivingModal?, APIError>) in
            Spinner.stop()
            switch result {
            case .success(let data):
                
                self?.generalDetailsArray = data
                
            case .failure(let error):
                print("Failed: \(error.localizedDescription)")
            }
        }
    }
}

extension PartnerPreferencesVC {
    func educationDetails() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.education.request, objectType: .general) { [weak self] (result: Result<EducationModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    
                    self?.educationDetailsArray = data
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension PartnerPreferencesVC {
    func getPartnerPreferences() {
        if Reachability.isConnectedToNetwork(){
            Spinner.start()
            APIClient.shared.fetch(with: APIService.getPartnerPreferences.request, objectType: .general) { [weak self] (result: Result<PartnerPreferencesModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    self?.partnerPreferencesArray = data
                    self?.preloadDataInTextField(data: data)
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension PartnerPreferencesVC {
    func requestPartnerPreferencesUpdate() {
        if Reachability.isConnectedToNetwork(){
            var params = [
                "maritial_status": txtMaritalStatus.text!,
                "religion_id": "\(religionID ?? 0)",
                "mother_tongue_id": "\(motherTongueID ?? 0)",
                "vegetarian": txtDiet.text!,
                "age_from": lblLowerValue.text!,
                "age_to": lblHigherValue.text!,
                "height": height ?? 0
            ] as [String : Any]
            
            if isExpanded{
                params["address"] = txtCountry.text!
                params["education_id"] = "\(educationID ?? 0)"
                params["education_degree_name"] = txtEducationDegreeName.text!
                params["profession_id"] = "\(professionID ?? 0)"
                params["income"] = txtAnnualIncome.text!
            }
            
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            
            Spinner.start()
            APIClient.shared.fetch(with: APIService.updatePartnerPreferences(body: params, header: header).request, objectType: .general) { [weak self] (result: Result<UpdatePartnerPreferencesModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Partner Preferences Details Updated Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.generalInformationUpdateSuccess()
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func generalInformationUpdateSuccess(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension PartnerPreferencesVC: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === self.slider {
            lblLowerValue.text = "\(String(format:"%.0f", minValue))"
            lblHigherValue.text = "\(String(format:"%.0f", maxValue))"
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
        UIDevice.vibrate()
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
