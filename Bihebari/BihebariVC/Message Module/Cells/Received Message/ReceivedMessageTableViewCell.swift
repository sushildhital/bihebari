//
//  ReceivedMessageTableViewCell.swift
//    Bihebari
//
//  Created by Prakash Shahi on 12/06/2021.
//

import UIKit
import IBAnimatable

class ReceivedMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgProfile: AnimatableImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bgView.layer.cornerRadius = 8
        bgView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        bgView.clipsToBounds = true
    }
}




