//
//  ChatListTableViewCell.swift
//   Bihebari
//
//  Created by Prakash Shahi on 12/06/2021.
//

import UIKit
import IBAnimatable
import SDWebImage

class ChatListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: AnimatableImageView!
    @IBOutlet weak var lblUserCode: UILabel!
    @IBOutlet weak var lblRecentMsg: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var btnMessageCount: AnimatableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
//    func updateUI(model: ChatListModel?) {
//        
//        lblUserCode.text = model?.user_code
//        lblDate.text = "\(model?.date?.prefix(10) ?? "")"
//        if model?.msgCount == 0 {
//            btnMessageCount.isHidden = true
//            lblRecentMsg.font = UIFont(name: "Lato-Regular", size: 14)
//            lblRecentMsg.textColor = UIColor(named: "Gray-3")
//
//        }else {
//            btnMessageCount.isHidden = false
//            btnMessageCount.setTitle("\(model?.msgCount ?? 0)", for: .normal)
//            lblRecentMsg.font = UIFont(name: "Lato-Bold", size: 14)
//            lblRecentMsg.textColor = UIColor(named: "Gray-1")
//        }
//        if model?.message == "" || model?.message == nil {
//            lblRecentMsg.text = "You have new match say hi.."
//        }else{
//            
//            lblRecentMsg.text = model?.message?.removingPercentEncoding
//        }
//        
//        if model?.photo == nil{
//            imgProfile.image = UIImage(named: "imgUploadPhoto")
//            imgProfile.tintColor = UIColor.red
//        }else{
//            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (model?.photo ?? "")
//            imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
//        }
//       
//        if model?.online == "Y" {
//            imgStatus.image = UIImage(named: "imgActive")
//        }else{
//            imgStatus.image = UIImage(named: "imgNotActive")
//        }
//        
//    }
    
}
