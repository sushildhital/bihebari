
//  ChatListViewController.swift
//  Bihebari

//  Created by Prakash Shahi on 12/06/2021.
//

import UIKit
import SocketIO

class ChatListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let refreshControl = UIRefreshControl()

    private var chatListModel: [ChatListModel]? {
        didSet {
            tableView.reloadData()
        }
    }
//    var chatListModel = [ChatListModel]()
    var isSocketConnected = false

    
    let userID = "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)"
    lazy var manager = SocketManager(socketURL:URL(string: Constants.BASE_URL + "?id=\(userID)")!, config: [.log(false), .forceNew(false)])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavBar()
        tableView.register(UINib(nibName: "ChatListTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatListTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView()
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
        if !isSocketConnected{
            setupSocket()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        manager.defaultSocket.disconnect()
    }

    func customizeNavBar(){
        self.navigationItem.title = "Message"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Lato-Bold", size: 16)!]
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 0.3
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    @objc func popBack(){
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func setupSocket() {
            activityIndicator.isHidden = false
            let socket = manager.defaultSocket
            socket.on(clientEvent: .connect) {data, ack in
                print("socket connected")
                socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
            }
            socket.on(clientEvent: .disconnect) {data, ack in
                print("socket disconnected")
//                socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
            }
            socket.on("chatListRes") { data, ack in
                print("*********************** DATA RECEIVED ***************************")
                self.isSocketConnected = true
                socket.emit("chatList", "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)")
                self.decodeResponse(data: data)
                self.activityIndicator.isHidden = true
            }
            socket.connect()
    }
    
    func decodeResponse(data: [Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data)
            let decoder = JSONDecoder()
            let chatList = try decoder.decode([ChatResponseModel].self, from: jsonData)
            
            if let model = chatList[0].chatList {
                self.chatListModel = model
                let msgCount = chatListModel?.reduce(0) {$0 + $1.msgCount!}
                let vcs = tabBarController?.viewControllers
                if msgCount == 0 {
                    vcs?[2].tabBarItem.badgeValue = nil
                }else{
                    vcs?[2].tabBarItem.badgeColor = UIColor(named: "Bihebari-Red")
                    vcs?[2].tabBarItem.badgeValue = "\(msgCount ?? 0)"
                }
            }
        }catch{
            print(error)
        }
    }
    
    
}

extension ChatListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatListModel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListTableViewCell", for: indexPath) as! ChatListTableViewCell
//        cell.updateUI(model: model?[indexPath.row])
        let model = self.chatListModel?[indexPath.row]
        cell.lblUserCode.text = model?.user_code
        cell.lblDate.text = "\(model?.date?.prefix(10) ?? "")"
        if model?.msgCount == 0 {
            cell.btnMessageCount.isHidden = true
            cell.lblRecentMsg.font = UIFont(name: "Lato-Regular", size: 14)
            cell.lblRecentMsg.textColor = UIColor(named: "Gray-3")

        }else {
            cell.btnMessageCount.isHidden = false
            cell.btnMessageCount.setTitle("\(model?.msgCount ?? 0)", for: .normal)
            cell.lblRecentMsg.font = UIFont(name: "Lato-Bold", size: 14)
            cell.lblRecentMsg.textColor = UIColor(named: "Gray-1")
        }
        if model?.message == "" || model?.message == nil {
            cell.lblRecentMsg.text = "You have new match say hi.."
        }else{
            
            cell.lblRecentMsg.text = model?.message?.removingPercentEncoding
        }
        
        if model?.photo == nil{
            cell.imgProfile.image = UIImage(named: "imgUploadPhoto")
            cell.imgProfile.tintColor = UIColor.red
        }else{
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (model?.photo ?? "")
            cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }
       
        if model?.online == "Y" {
            cell.imgStatus.image = UIImage(named: "imgActive")
        }else{
            cell.imgStatus.image = UIImage(named: "imgNotActive")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = self.chatListModel?[indexPath.row]
        let userId = model?.id
        let viewController = UIStoryboard(name: "Message", bundle: nil).instantiateViewController(withIdentifier: "MessageTableViewController") as! MessageTableViewController
        viewController.sendToUser = userId
        viewController.title = model?.user_code
        viewController.userCode = model?.user_code ?? ""
        viewController.userImage = model?.photo ?? ""
        navigationController?.pushViewController(viewController, animated: true)
    }
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { suggestedActions in
            
        let model = self.chatListModel?[indexPath.row]
        let toUserId = model?.id ?? 0
        let delete = UIAction(title: "Delete",  image: UIImage(systemName: "trash"),attributes: .destructive) { action in
            let alert = UIAlertController(title: "Delete Message", message: "Do you want to delete this message?", preferredStyle: .actionSheet)
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
                APIHandler.shared.deleteAllMsg(firstUserID: "\(self.userID)", secondUserID: "\(toUserId)") { success in
                    SwiftMessage.showBanner(title: "Success", message: "Message deleted!", type: .success)
                } failure: { failure in
                    SwiftMessage.showBanner(title: "Error", message: "Could not delete message.", type: .error)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
        }
        return UIMenu(title: "", children: [delete])
        }
    }
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let model = self.chatListModel?[indexPath.row]
//        let toUserId = model?.id ?? 0
//        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
//                let alert = UIAlertController(title: "Delete Message", message: "Do you want to delete this message?", preferredStyle: .actionSheet)
//                let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
//                    APIHandler.shared.deleteAllMsg(firstUserID: "\(self.userID)", secondUserID: "\(toUserId)") { success in
//                        SwiftMessage.showBanner(title: "Success", message: "Message deleted!", type: .success)
//                    } failure: { failure in
//                        SwiftMessage.showBanner(title: "Error", message: "Could not delete message.", type: .error)
//                    }
//
//                }
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//
//                alert.addAction(deleteAction)
//                alert.addAction(cancelAction)
//                self.present(alert, animated: true, completion: nil)
//
//            }
//
//            deleteAction.backgroundColor = .systemRed
//            return [deleteAction]
//        }
}
