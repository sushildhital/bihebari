
//  MessageTableViewController.swift
//  Bihebari
//
//  Created by Prakash Shahi on 12/06/2021.
//

import UIKit
import SocketIO
import IBAnimatable

class MessageTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: AnimatableImageView!
    
    private var model: [MessageModel]? {
        didSet {
            tableView.reloadData()
            if (model?.count ?? 0) > 0 {
                tableView.scrollToRow(at: IndexPath(row: model!.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    struct msgRead {
        let room_id : String
        let to_user_id : String
    }
    
    let userID = "\(UserDefaultManager.shared.userDetailsModel?.id ?? 0)"
    let intUserID = UserDefaultManager.shared.userDetailsModel?.id ?? 0
    lazy var manager = SocketManager(socketURL: URL(string: Constants.BASE_URL + "?id=\(userID)")!, config: [.log(false), .compress])
    var socket: SocketIOClient!
    var sendToUser: Int!
    var userCode = ""
    var userImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        if (receiverUserID.toInt() > userData.id!!) {
        //                        socket.emit("activeChatRoom", receiverUserID + "_" + userData.id)
        //                    } else {
        //                        socket.emit("activeChatRoom", userData.id.toString() + "_" + receiverUserID)
        //                    }
        
        navigationController?.navigationBar.isHidden = true
        view.backgroundColor = .white
        tableView.register(UINib(nibName: "ReceivedMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "ReceivedMessageTableViewCell")
        tableView.register(UINib(nibName: "SentMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "SentMessageTableViewCell")
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        lblName.text = userCode
        imgProfile.tintColor = UIColor.red
        
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (userImage)
        imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: UIImage(named: "imgUploadPhoto"), options: .delayPlaceholder, completed: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupSocket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    func setupSocket() {
        socket = manager.defaultSocket
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            self.socket.emit("getMessages", ["fromUserId" : self.userID, "toUserId" : self.sendToUser])
        }
        
        socket.on("getMessagesResponse") { data, ack in
            print("*********************** MESSAGE RECEIVED ***************************")
            var params = [String: String] ()
            
            if (self.sendToUser ?? 0 > self.intUserID) {
                params = [
                    "room_id" : "\(self.sendToUser ?? 0)_\(self.userID)",
                    "to_user_id" : "\(self.userID)"
                ]
                self.socket.emit("activeChatRoom", params)
                print("activeChatRoom", params)
                
            } else {
                params = [
                    "room_id" : "\(self.userID)_\(self.sendToUser ?? 0)",
                    "to_user_id" : "\(self.userID)"
                ]
                self.socket.emit("activeChatRoom", params)
                print("activeChatRoom", params)
            }
            self.decodeResponse(data: data)
        }
        
        socket.on("addMessageResponse") { data, ack in
            print("*********************** MESSAGE ADDED ***************************")
            self.singleDecodeResponse(data: data)
            
        }
        socket.connect()
        
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            bottomConstraint.constant = isKeyboardShowing ? keyboardHeight : 6
            self.view.layoutIfNeeded()
            if isKeyboardShowing {
                if (self.model?.count ?? 0) > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: self.model!.count - 1, section: 0), at: .bottom, animated: true)
                }
            }
        }
    }
    
    func sendMessage(message: String) {
        let params : [String : Any] = [
            "type" : "text",
            "fileFormat" : "",
            "filePath" : "",
            "fromUserId" : userID,
            "toUserId" : sendToUser!,
            "toSocketId" : "",
            "message" : message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "" ,
            "time" : dateFormatter(format: "h:mm a", date: Date()),
            "date" : dateFormatter(format: "yyyy-MM-dd", date: Date())
        ]
        
        socket.emit("addMessage", params)
        model?.append(
            MessageModel(
                date: dateFormatter(format: "yyyy-MM-dd", date: Date()),
                fileFormat: "",
                filePath: "",
                fromUserId: Int(userID),
                id: 0,
                message: message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "",
                time: dateFormatter(format: "h:mm a", date: Date()),
                toUserId: sendToUser,
                type: "text", fcmKey: "")
        )
    }
    
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func dateFormatter(format: String, date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        let text = messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if !text.isEmpty {
            sendMessage(message: text)
            messageTextView.text = ""
        }
    }
    
    func decodeResponse(data: [Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data)
            
            let decoder = JSONDecoder()
            let messageList = try decoder.decode([MessageResponseModel].self, from: jsonData)
            if let model = messageList[0].result {
                self.model = model
            }
            
        } catch {
            print(error)
        }
    }
    
    func singleDecodeResponse(data: [Any]) {
        for item in data{
            guard let dict = item as? [String: Any],
                  let message = dict["message"] as? String,
                  let fromUserIdMsg = dict["fromUserId"] as? Int,
                  let toUserIdMsg = dict["toUserId"] as? Int
            else {
                return
            }
            if self.intUserID == fromUserIdMsg ||  self.intUserID == toUserIdMsg{
                if self.sendToUser == fromUserIdMsg || self.sendToUser == toUserIdMsg{
                    model?.append(
                        MessageModel(
                            date: dateFormatter(format: "yyyy-MM-dd", date: Date()),
                            fileFormat: "",
                            filePath: "",
                            fromUserId: fromUserIdMsg,
                            id: 0,
                            message: message,
                            time: dateFormatter(format: "h:mm a", date: Date()),
                            toUserId: toUserIdMsg,
                            type: "text", fcmKey: "")
                    )
                }
            }
//            model?.append(
//                MessageModel(
//                    date: dateFormatter(format: "yyyy-MM-dd", date: Date()),
//                    fileFormat: "",
//                    filePath: "",
//                    fromUserId: fromUserIdMsg,
//                    id: 0,
//                    message: message,
//                    time: dateFormatter(format: "h:mm a", date: Date()),
//                    toUserId: toUserIdMsg,
//                    type: "text", fcmKey: "")
//            )
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.model?[indexPath.row],
              let fromUserID = model.fromUserId,
              let toUserIDD = model.toUserId
        else {return UITableViewCell()}
        
        if fromUserID ==  sendToUser! && toUserIDD == Int(userID) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceivedMessageTableViewCell", for: indexPath) as! ReceivedMessageTableViewCell
            cell.messageLabel.text = model.message?.removingPercentEncoding
            let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (userImage)
            cell.imgProfile.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), placeholderImage: UIImage(named: "imgUploadPhoto"), options: .delayPlaceholder, completed: nil)
            cell.imgProfile.tintColor = UIColor.red
            cell.dateLabel.text = model.time
            return cell
        }else if fromUserID == Int(userID) && toUserIDD == sendToUser! {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SentMessageTableViewCell", for: indexPath) as! SentMessageTableViewCell
            cell.updateUI(model: model)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        messageTextView.endEditing(true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool
    {
        let model = self.model?[indexPath.row]
        let fromUserID = model?.fromUserId
        
        if fromUserID == Int(userID) {
            return true
        }
        else {
            return false
        }
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { suggestedActions in
            let model = self.model?[indexPath.row]
            let fromUserID = model?.fromUserId
            let messageID = model?.id ?? 0
            if fromUserID == Int(self.userID) {
                let copy = UIAction(title: "Copy", image: UIImage(systemName: "doc.on.clipboard")) { action in
                    UIPasteboard.general.string = "\(self.model?[indexPath.row].message?.removingPercentEncoding ?? "")"
                }
                let delete = UIAction(title: "Delete",  image: UIImage(systemName: "trash"),attributes: .destructive) { action in
                    let alert = UIAlertController(title: "Delete Message", message: "Do you want to delete this message?", preferredStyle: .actionSheet)
                    let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
                        APIHandler.shared.deleteSingleMsg(id: messageID) { success in
                            SwiftMessage.showBanner(title: "Success", message: "Message deleted!", type: .success)
                            self.model?.remove(at: indexPath.row)
                            tableView.reloadData()
                        } failure: { failure in
                            SwiftMessage.showBanner(title: "Error", message: "Could not delete message.", type: .error)
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    
                    alert.addAction(deleteAction)
                    alert.addAction(cancelAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
                return UIMenu(title: "", children: [copy,delete])
                
            }
            else {
                let copy = UIAction(title: "Copy", image: UIImage(systemName: "doc.on.clipboard")) { action in
                    UIPasteboard.general.string = "\(self.model?[indexPath.row].message?.removingPercentEncoding ?? "")"
                }
                return UIMenu(title: "", children: [copy])
                
            }
            
            
        }
    }
    func tableView(_ tableView: UITableView, willDisplayContextMenu configuration: UIContextMenuConfiguration, forRowAt indexPath: IndexPath, animator: UIContextMenuInteractionAnimating?) -> Bool {
        let model = self.model?[indexPath.row]
        let fromUserID = model?.fromUserId
        
        if fromUserID == Int(userID) {
            return true
        }
        else {
            return false
        }
    }
    
    
}

extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
