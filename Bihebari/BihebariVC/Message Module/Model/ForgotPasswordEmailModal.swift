//
//  ForgotPasswordEmailModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/18/21.
//

import Foundation

struct ForgotPasswordEmailModal : Codable {
    let success : String?

    enum CodingKeys: String, CodingKey {

        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
    }

}
