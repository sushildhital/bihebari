//
//  MessageResponseModel.swift
//  WebSocket
//
//  Created by Prakash Shahi on 12/06/2021.
//

import Foundation

struct MessageResponseModel : Codable {
    let result : [MessageModel]?
    
    enum CodingKeys: String, CodingKey {
        
        case result = "result"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        result = try values.decodeIfPresent([MessageModel].self, forKey: .result)
    }
    
}


class MessageModel : Codable {
    let date : String?
    let fileFormat : String?
    let filePath : String?
    let fromUserId : Int?
    let id : Int?
    let message : String?
    let time : String?
    let toUserId : Int?
    let type : String?
    let fcmKey : String?
    
    init(date: String?, fileFormat: String?, filePath: String?, fromUserId: Int?, id: Int?, message: String?, time: String?, toUserId: Int?, type: String?, fcmKey: String?) {
        self.date = date
        self.fileFormat = fileFormat
        self.filePath = filePath
        self.fromUserId = fromUserId
        self.id = id
        self.message = message
        self.time = time
        self.toUserId = toUserId
        self.type = type
        self.fcmKey = fcmKey
    }
    
    enum CodingKeys: String, CodingKey {
        
        case date = "date"
        case fileFormat = "fileFormat"
        case filePath = "filePath"
        case fromUserId = "fromUserId"
        case id = "id"
        case message = "message"
        case time = "time"
        case toUserId = "toUserId"
        case type = "type"
        case fcmKey = "fcmKey"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        fileFormat = try values.decodeIfPresent(String.self, forKey: .fileFormat)
        filePath = try values.decodeIfPresent(String.self, forKey: .filePath)
        fromUserId = try values.decodeIfPresent(Int.self, forKey: .fromUserId)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        toUserId = try values.decodeIfPresent(Int.self, forKey: .toUserId)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        fcmKey = try values.decodeIfPresent(String.self, forKey: .fcmKey)
    }
    
}




class SingleMessageResponseModel : Codable {
    
    let date : String?
    let fcmKey: String?
    let fileFormat:String?
    let filePath: String?
    let fromUserId: Int?
    let message: String?
    let time: String?
    let toSocketId: String?
    let toUserId: Int?
    let type: String?
    
    enum CodingKeys: String, CodingKey {
        
        
        case date = "data"
        case fcmKey = "fcmKey"
        case fileFormat = "fileFormat"
        case filePath = "filePath"
        case fromUserId = "fromUserId"
        case message = "message"
        case time = "time"
        case toSocketId = "toSocketId"
        case toUserId = "toUserId"
        case type = "type"
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        fcmKey = try values.decodeIfPresent(String.self, forKey: .fcmKey)
        fileFormat = try values.decodeIfPresent(String.self, forKey: .fileFormat)
        filePath = try values.decodeIfPresent(String.self, forKey: .filePath)
        fromUserId = try values.decodeIfPresent(Int.self, forKey: .fromUserId)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        toUserId = try values.decodeIfPresent(Int.self, forKey: .toUserId)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        toSocketId = try values.decodeIfPresent(String.self, forKey: .toSocketId)
        
        
    }
    
}
