//
//  ChatResponseModel.swift
//  WebSocket
//
//  Created by Prakash Shahi on 12/06/2021.
//

import Foundation

struct ChatResponseModel : Codable {
    let chatList : [ChatListModel]?

    enum CodingKeys: String, CodingKey {

        case chatList = "chatList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        chatList = try values.decodeIfPresent([ChatListModel].self, forKey: .chatList)
    }

}

struct ChatListModel : Codable {
    let date : String?
    let from_user_id : Int?
    let id : Int?
    let message : String?
    let name : String?
    let online : String?
    let photo : String?
    let socket_id : String?
    let time : String?
    let to_user_id : Int?
    let updated_at : String?
    let user_code : String?
    let msgCount : Int?

    enum CodingKeys: String, CodingKey {

        case date = "date"
        case from_user_id = "from_user_id"
        case id = "id"
        case message = "message"
        case name = "name"
        case online = "online"
        case photo = "photo"
        case socket_id = "socket_id"
        case time = "time"
        case to_user_id = "to_user_id"
        case updated_at = "updated_at"
        case user_code = "user_code"
        case msgCount = "msgCount"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        from_user_id = try values.decodeIfPresent(Int.self, forKey: .from_user_id)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        online = try values.decodeIfPresent(String.self, forKey: .online)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        to_user_id = try values.decodeIfPresent(Int.self, forKey: .to_user_id)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        msgCount = try values.decodeIfPresent(Int.self, forKey: .msgCount)
    }

}
