//
//  OTPVerificationVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/17/21.
//

import UIKit
import IBAnimatable
import AEOTPTextField

class OTPVerificationVC: UIViewController {
    
    var OTPModal: OTPVerifyModal?
    var encryptedData = ""
    var receivedPhone = ""
    let logindata = [ProfileModal]()
    var otptext = ""
    var isPhoneOTP: Bool = false
    var isRedirectToBuild : Bool? = false
    
    @IBOutlet weak var otpTextField: AEOTPTextField!
    @IBOutlet weak var lblOTPTitle: UILabel!
    @IBOutlet weak var lblPhoneOrEmail: UILabel!

    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnVerifyAction(_ sender: Any) {
        verifyOTP()
    }
    
    @IBAction func btnResendAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
   
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otpTextField.otpDelegate = self
        otpTextField.configure(with: 6)
        otpTextField.otpFilledBorderColor = .clear
        navigationController?.navigationBar.isHidden = true
        lblPhoneOrEmail.text = "\(receivedPhone)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension OTPVerificationVC {
    func verifyOTP() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : String] = [
                "data": encryptedData,
                "otp": otptext
            ]
            
            Spinner.start()
            APIClient.shared.fetch(with: APIService.verifyOTP(body: params).request, objectType: .general) { [weak self] (result: Result<OTPVerifyModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.isVerified = true
                    SwiftMessage.showBanner(title: "Success", message: "OTP verified successfully.", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.verifyOTPSuccess(modal: data)
                        self.reloadProfile()
                    }
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func verifyOTPSuccess(modal: OTPVerifyModal?){
        
        // ===== check to see if buildprofile = true =====
        
        if isRedirectToBuild == true{
            let vc = BuildProfileVC.instantiate(fromAppStoryboard: .BuildProfile)
            KundaliHandleAfterLogin.shared.isFromLogin = true
            KundaliHandleAfterLogin.shared.isFromProfileEdit = false
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = ProfileVC.instantiate(fromAppStoryboard: .Profile)
            vc.profileVerified = modal
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
}

extension OTPVerificationVC {
    func reloadProfile() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<ProfileSuccessModal?, APIError>) in
                Spinner.stop()
                switch result {
                case .success(let data):
                    UserDefaultManager.shared.userDetailsModel  = data?.data?.user
                case .failure(let error):
                    Spinner.stop()
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            Spinner.stop()
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension OTPVerificationVC: AEOTPTextFieldDelegate {
    func didUserFinishEnter(the code: String) {
        print(code)
        otptext = code
    }
}
