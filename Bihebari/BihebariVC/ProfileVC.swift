//
//  ProfileVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/14/21.
//

import UIKit
import FittedSheets
import SwiftMessages
import IBAnimatable
import ImageViewer_swift
import StoreKit

class ProfileVC: UIViewController, UINavigationControllerDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var collectionProfileGallery: UICollectionView!
    @IBOutlet weak var collectionAddStory: UICollectionView!
    @IBOutlet weak var hideVerifyOTPView: UIView!
    @IBOutlet weak var btnOTP: UIButton!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var imgProfile: AnimatableImageView!
    @IBOutlet weak var btnAddPhoto: UIButton!
    @IBOutlet weak var galleryCollectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewAddStoryoLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewAddStoryWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewActivity: UIVisualEffectView!
    @IBOutlet weak var imgArrowUp: UIImageView!
    @IBOutlet weak var lblSelect: UILabel!
    
    var imagePicker = UIImagePickerController()
    
    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 0
    let BOTTOM_INSET : CGFloat = 0
    let LEFT_INSET : CGFloat = 0
    let RIGHT_INSET : CGFloat = 0
    let SECTION_INSET : CGFloat = 0
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 10
    
    let NUMBER_OF_COLUMNN : CGFloat = 3
    let TOP_INSETT : CGFloat = 0
    let BOTTOM_INSETT : CGFloat = 0
    let LEFT_INSETT : CGFloat = 2
    let RIGHT_INSETT : CGFloat = 2
    let SECTION_INSETT : CGFloat = 2
    let INTER_ITEM_INSETT : CGFloat = 4
    let LINE_SPACEE : CGFloat = 4

    
    var viewProfileArray: LoginUserModal?
    var storyImages = [String]()
    var galleryImages = [String]()
    var profileVerified: OTPVerifyModal?
    var selectedImage : Int?
    var selectedImaegeType: Int?
    
    @IBAction func btnUploadImageAction(_ sender: Any) {
        selectedImaegeType = 0
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Capture Photo", style: .default, handler: { _ in
               self.openCamera()
           }))

           alert.addAction(UIAlertAction(title: "Select From Gallery", style: .default, handler: { _ in
               self.openGallary()
           }))

           alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAddPhotoAction(_ sender: Any) {
        selectedImaegeType = 1
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Capture Photo", style: .default, handler: { _ in
               self.openCamera()
           }))

           alert.addAction(UIAlertAction(title: "Select From Gallery", style: .default, handler: { _ in
               self.openGallary()
           }))

           alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        
        if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("photo clicked")
            if selectedImaegeType! == 1 {
                CheckImage(image: img)
            }else {
                checkPhoto(image: img)
            }
        }
        
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            let choosenImage1 = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let data = choosenImage1.pngData()! as NSData
            data.write(toFile: localPath!, atomically: true)
        }
    }
    
    func refresh(){
        galleryImages = UserDefaultManager.shared.userDetailsModel?.other_image?.compactMap{$0.photo} ?? []
        collectionProfileGallery.reloadData()
        storyImages = UserDefaultManager.shared.userDetailsModel?.story_image?.compactMap{$0.photo} ?? []
        collectionAddStory.reloadData()
    }
    
    func checkPhoto(image: UIImage){
        let alert = UIAlertController(title: "Upload", message: "Are you sure you want to Add Photo?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.addPhotoServiceCall(image: image)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func CheckImage(image: UIImage){
        let alert = UIAlertController(title: "Upload", message: "Are you sure you want to Add Story?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.addStoryServiceCall(image: image)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func btnProfileSettingsAction(_ sender: Any) {
        let vc = ProfileSettingsVC.instantiate(fromAppStoryboard: .ProfileSettings)
        let sheetController = SheetViewController(controller: vc, sizes: [.fixed(600)])
        sheetController.allowPullingPastMaxHeight = false
        self.present(sheetController, animated: true , completion: nil)
    }
    
    @IBAction func btnViewProfileAction(_ sender: Any) {
        let vc = ViewProfileVC.instantiate(fromAppStoryboard: .ViewProfile)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnVerifyOTPAction(_ sender: Any) {
        let vc = PhoneVerificationVC.instantiate(fromAppStoryboard: .PhoneVerification)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationCentreCalled()
        collectionProfileGallery.register(UINib(nibName: "ProfileGalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProfileGalleryCollectionViewCell")
        updateUI()
        hideViewAfterVerified()
//        print("otherImage:\(UserDefaultManager.shared.userDetailsModel?.other_image?.count ?? 0)")
    }
    
    func updateUI(){
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")"
        if UserDefaultManager.shared.userDetailsModel?.profile_image?.photo == "null" {
            imgProfile.image = UIImage(systemName: "person.crop.circle.fill")
        } else {
            imgProfile.sd_setImage(with: URL(string: url + (UserDefaultManager.shared.userDetailsModel?.profile_image?.photo ?? "")), completed: nil)
        }
        lblProfileName.text = UserDefaultManager.shared.userDetailsModel?.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgArrowUp.transform = CGAffineTransform(translationX: 0, y: 8)
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.6, options: [.repeat,.autoreverse], animations: {
                    self.imgArrowUp.transform = CGAffineTransform.identity
                }, completion: nil)
        refresh()
        reloadProfile()
    }
    
    func hideViewAfterVerified(){
        if UserDefaultManager.shared.userDetailsModel?.otp_status == "active" && UserDefaultManager.shared.userDetailsModel?.email_otp_status == "active"{
            hideVerifyOTPView.isHidden = true
            btnOTP.isEnabled = false
        }else{
            hideVerifyOTPView.isHidden = false
            btnOTP.isEnabled = true
        }
    }
    
    func notificationCentreCalled(){
        NotificationCenter.default.addObserver(self, selector: #selector(buildProfile), name: NSNotification.Name("BuildProfileTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(partnerPreferences), name: NSNotification.Name("PartnerPreferencesTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(myShortlists), name: NSNotification.Name("MyShortlistsTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(myInterests), name: NSNotification.Name("MyInterestsTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(interestedInMe), name: NSNotification.Name("InterestedInMeTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changePassword), name: NSNotification.Name("ChangePasswordTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appSettings), name: NSNotification.Name("AppSettingsTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rateApp), name: NSNotification.Name("RateAppTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deleteAccount), name: NSNotification.Name("DeleteAccountTapped"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(yourPlan), name: NSNotification.Name("YourPlanTapped"), object: nil)
    }
    
    @objc func buildProfile(){
        let vc = BuildProfileVC.instantiate(fromAppStoryboard: .BuildProfile)
        BuildProfileHandler.shared.isFromEditProfile = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func partnerPreferences(){
        let vc = PartnerPreferencesVC.instantiate(fromAppStoryboard: .PartnerPreferences)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func myShortlists(){
        let vc = MatchesVC.instantiate(fromAppStoryboard: .Matches)
        ViewHandler.shared.isFromMatchesVC = false
        ViewHandler.shared.isFromShortList = true
        ViewHandler.shared.isFromMyInterests = false
        ViewHandler.shared.isFromInterestedToMe = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func myInterests(){
        let vc = MyInterestsVC.instantiate(fromAppStoryboard: .MyInterests)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func interestedInMe(){
        let vc = InterestedToMeVC.instantiate(fromAppStoryboard: .InterestedToMe)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func changePassword(){
        let vc = UpdatePasswordVC.instantiate(fromAppStoryboard: .UpdatePassword)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func appSettings(){
        let vc = AppSettingsVC.instantiate(fromAppStoryboard: .AppSettings)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func rateApp(){
        rateApplication()
    }
    
    @objc func yourPlan(){
        let vc = YourPlanVC.instantiate(fromAppStoryboard: .YourPlan)
        BuildProfileHandler.shared.isFromEditProfile = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func rateApplication(){
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            guard let url = URL(string: "itms-apps://itunes.apple.com/ru/app/cosmeteria/id1270174484") else {
                return
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func deleteAccount(){
        let alert = UIAlertController(title: "Delete Account?", message: "Are You Sure You Want To Delete Your Account?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.reqDeleteAccount()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
}

extension ProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionAddStory {
            return storyImages.count
        }else {
            return galleryImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionAddStory {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddStoryViewCell", for: indexPath) as! AddStoryViewCell
            cell.imgPhoto.sd_setImage(with: URL(string: "\(Constants.BASE_URL + "/uploads/users/")" + storyImages[indexPath.row]), completed: nil)
            cell.imgPhoto.setupImageViewer()
            longPressStory()
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileGalleryCollectionViewCell", for: indexPath) as! ProfileGalleryCollectionViewCell
            cell.imgGallery.sd_setImage(with: URL(string: "\(Constants.BASE_URL + "/uploads/users/")" + galleryImages[indexPath.row]), completed: nil)
            cell.imgGallery.setupImageViewer()
            
            cell.btnDelete?.tag = indexPath.row
            cell.btnDelete?.addTarget(self, action: #selector(deletePhoto(sender:)), for: UIControl.Event.touchUpInside)
            cell.btnCross?.addTarget(self, action: #selector(cancelDelete(sender:)), for: UIControl.Event.touchUpInside)
            
            cell.btnDelete.isHidden = true
            cell.btnCross.isHidden = true
            cell.deleteView.isHidden = true
            cell.cancelView.isHidden = true
            longPress()
            return cell
        }
    }

    
    @objc func deletePhoto(sender:UIButton) {
        let i = sender.tag
        UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.viewActivity.alpha = 1
        })
        let alert = UIAlertController(title: "Remove Photo?", message: "Do you want remove your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.removeProfilePhoto(id: UserDefaultManager.shared.userDetailsModel?.other_image?[i].id ?? 0)
            UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.viewActivity.alpha = 0
            })
//            print("imageID:\(UserDefaultManager.shared.userDetailsModel?.other_image?[i].id ?? 0)")
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: {action in
            UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.viewActivity.alpha = 0
            })
        }))
        self.present(alert, animated: true)
    }
    
    @objc func cancelDelete(sender:UIButton) {
        collectionProfileGallery.reloadData()
    }
    
    func longPress(){
        let longPressGR = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGR:)))
        longPressGR.minimumPressDuration = 0.5
        longPressGR.delaysTouchesBegan = true
        self.collectionProfileGallery.addGestureRecognizer(longPressGR)
    }
    
    @objc func handleLongPress(longPressGR: UILongPressGestureRecognizer) {
        if longPressGR.state != .ended {
            return
        }
        
        let point = longPressGR.location(in: self.collectionProfileGallery)
        let indexPath = self.collectionProfileGallery.indexPathForItem(at: point)
        if let indexPath = indexPath {
            var cell = self.collectionProfileGallery.cellForItem(at: indexPath) as! ProfileGalleryCollectionViewCell
            cell.btnDelete.isHidden = false
            cell.deleteView.isHidden = false
            cell.cancelView.isHidden = false
            cell.btnCross.isHidden = false
        } else {
            print("Could not find index path")
        }
    }
    
    func longPressStory(){
        let longPressGR = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressStory(longPressGR:)))
        longPressGR.minimumPressDuration = 0.5
        longPressGR.delaysTouchesBegan = true
        self.collectionAddStory.addGestureRecognizer(longPressGR)
    }
    
    @objc func handleLongPressStory(longPressGR: UILongPressGestureRecognizer) {
        if longPressGR.state != .ended {
            return
        }
        
        let point = longPressGR.location(in: self.collectionAddStory)
        let indexPath = self.collectionAddStory.indexPathForItem(at: point)
        if let indexPath = indexPath {
            var cell = self.collectionAddStory.cellForItem(at: indexPath) as! AddStoryViewCell
            let alert = UIAlertController(title: "Remove Story?", message: "Do you want remove your story?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                self.removeProfilePhoto(id: UserDefaultManager.shared.userDetailsModel?.story_image?[indexPath.row].id ?? 0)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            
        } else {
            print("Could not find index path")
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemWidth = (collectionViewWidth - LEFT_INSETT - RIGHT_INSETT - (INTER_ITEM_INSETT * (NUMBER_OF_COLUMNN - 1))) / NUMBER_OF_COLUMNN
        let itemHeight : CGFloat = 120
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: TOP_INSETT, left: LEFT_INSETT, bottom: BOTTOM_INSETT, right: RIGHT_INSETT)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACEE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSETT
    }
}

extension ProfileVC {
    func addStoryServiceCall(image: UIImage){
        if Reachability.isConnectedToNetwork(){
            guard let data = image.jpegData(compressionQuality: 0.8) else {return}
            let params : [MultipartMediaType] = [ MultipartMediaType(data: data, mimeType: "image/jpeg", keyName: "photo")]
            viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.addStory(files: params).request, objectType: .fromData) { (result: Result<AddStoryResponseModel?, APIError>) in
                UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.viewActivity.alpha = 0
                })
                switch result {
                case .success(let data):
                    SwiftMessage.showBanner(title: "Success", message: "Story Uploaded Successfully.", type: .success)
                    UserDefaultManager.shared.saveNewStory(story: data?.profileImage)
                    self.refresh()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension ProfileVC {
    func addPhotoServiceCall(image: UIImage){
        if Reachability.isConnectedToNetwork(){
            guard let data = image.jpegData(compressionQuality: 0.8) else {return}
            let params : [MultipartMediaType] = [ MultipartMediaType(data: data, mimeType: "image/jpeg", keyName: "photo")]
            viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.addPhoto(files: params).request, objectType: .fromData) { (result: Result<AddPhotoData?, APIError>) in
                UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.viewActivity.alpha = 0
                })
                switch result {
                case .success(let data):
                    SwiftMessage.showBanner(title: "Success", message: "Photo Uploaded Successfully.", type: .success)
                    UserDefaultManager.shared.saveNewPhoto(photo: data?.profileImage)
                    self.refresh()
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
}

extension ProfileVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if collectionAddStory.isDragging{
            if viewAddStoryWidthConstraint.constant != 0{
                if collectionAddStory.contentOffset.x > 1 {
                    self.viewAddStoryoLeadingConstraint.constant = -collectionAddStory.contentOffset.x
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.allowUserInteraction], animations: {
                        if self.view != nil {
                            self.view.layoutIfNeeded()
                        }
                    }, completion: nil)
                }else {
                    self.viewAddStoryoLeadingConstraint.constant = 16
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.allowUserInteraction], animations: {
                        self.view.layoutIfNeeded()
                    }, completion: nil)
                }
            }
        }
    }
}

extension ProfileVC {
    func reqDeleteAccount() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : String] = ["reason": "Bihe Vayo"]
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.deleteAccount(body: params, header: header).request, objectType: .general) {(result: Result<KhaltiModal?, APIError>) in
                UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.viewActivity.alpha = 0
                })
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Account Delete Request Sent Successfully", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    }
                    
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension ProfileVC{
    func removeProfilePhoto(id: Int) {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            self.viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.removeProfilePhoto(header: header, imageID: id).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                UIView.transition(with: self!.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self?.viewActivity.alpha = 0
                })
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Photo Removed Successfully.", type: .success)
                        self?.reloadProfile()
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension ProfileVC{
    func removeStoryPhoto(id: Int) {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            self.viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.removeStoryPhoto(header: header, imageID: id).request, objectType: .general) { [weak self] (result: Result<KhaltiModal?, APIError>) in
                UIView.transition(with: self!.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self?.viewActivity.alpha = 0
                })
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Photo Removed Successfully.", type: .success)
                    self?.collectionAddStory.reloadData()
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension ProfileVC {
    func reloadProfile() {
        if Reachability.isConnectedToNetwork(){
            let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
            self.viewActivity.alpha = 1
            APIClient.shared.fetch(with: APIService.myProfile(header: header).request, objectType: .general) {(result: Result<ProfileSuccessModal?, APIError>) in
                UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                    self.viewActivity.alpha = 0
                })
                switch result {
                case .success(let data):
                    
                    UserDefaultManager.shared.userDetailsModel = data?.data?.user
                    if self.galleryImages.count == 0 {
                        self.lblSelect.isHidden = false
                        self.imgArrowUp.isHidden = false
                    }else {
                        self.imgArrowUp.isHidden = true
                        self.lblSelect.isHidden = true
                        self.updateUI()
                        self.refresh()
                        self.hideViewAfterVerified()
                    }
                    
                case .failure(let error):
                    UIView.transition(with: self.viewActivity, duration: 0.3, options: .transitionCrossDissolve, animations: {
                        self.viewActivity.alpha = 0
                    })
                    SwiftMessage.showBanner(title: "Error", message: "Error \(error.localizedDescription)", type: .error)
                }
            }
        }else{
            Spinner.stop()
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
}

extension UIView {
    func shakeFast(_ dur:Double) {
        let anim = CABasicAnimation(keyPath: "position")
        anim.duration = dur
        anim.repeatCount = 1000
        anim.autoreverses = true
        anim.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 3, y: self.center.y))
        anim.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 3, y: self.center.y))
        self.layer.add(anim, forKey: "position")
    }
}
