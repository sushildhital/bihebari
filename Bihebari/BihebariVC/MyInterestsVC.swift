//
//  MyInterestsVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/11/21.
//

import UIKit
import IBAnimatable

class MyInterestsVC: UIViewController {
    
    @IBOutlet weak var collectionMyInterests: UICollectionView!
    
    @IBOutlet weak var hiddenView: UIView!
    var indexPath: IndexPath?
    var myInterestData: LoginResponseModal?
    
    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 0
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let indexPath = indexPath {
            collectionMyInterests.isPagingEnabled = false
            collectionMyInterests.scrollToItem(at: indexPath, at: .left, animated: false)
            collectionMyInterests.isPagingEnabled = true
        }
        collectionMyInterests.shouldIgnoreContentInsetAdjustment = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionMyInterests.isPagingEnabled = true
        collectionMyInterests.register(UINib(nibName: "MyInterestsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MyInterestsCollectionViewCell")
        
        if UserDefaultManager.shared.userDetailsModel?.interested_in?.count == 0{
            hiddenView.isHidden = false
        }else{
            hiddenView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension MyInterestsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserDefaultManager.shared.userDetailsModel?.interested_in?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyInterestsCollectionViewCell", for: indexPath) as! MyInterestsCollectionViewCell
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].image ?? "")
        
        if UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].image == nil{
            cell.imgProfileImage.image = UIImage(named: "imgNoImage")
        }else{
            cell.imgProfileImage.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }
        
        cell.lblProfileName.text = UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].user_code
        cell.lblAge.text = "\(UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].age ?? 0)\(" yrs")"
        cell.lblLocation.text = "\(UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].address ?? "")"
        cell.lblProfession.text = UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].profession
        cell.lblHeight.text = "\(UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].height_ft ?? 0)'\(UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].height_inch ?? 0)"
                
        if UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].status == "reject" {
            cell.lblRequestStatus.text = "Your Request Has Been Rejected By This User."
        }else if UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].status == "accept" {
            cell.lblRequestStatus.text = "Your Request Has Been Accepted By This User."
        }else if UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].status == "approach" {
            cell.lblRequestStatus.text = "Your Request Is Pending For This User."
        }
        
        cell.cellAction = {action in
            DispatchQueue.main.async {
                collectionView.isPagingEnabled = false
                let row = action == .next ? indexPath.item + 1 : indexPath.item - 1
                collectionView.scrollToItem(at: IndexPath(row: row, section: 0), at: .left, animated: false)
                collectionView.isPagingEnabled = true
            }
        }
        
        cell.btnViewProfileTapped = { [weak self] view in
            ViewHandler.shared.isFromShortList = false
            ViewHandler.shared.isFromMatchesVC = false
            ViewHandler.shared.isFromMyInterests = true
            ViewHandler.shared.isFromInterestedToMe = false
            self?.myInterestProfileDetailsAPI(profile: UserDefaultManager.shared.userDetailsModel?.interested_in?[indexPath.row].user_code ?? "")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? MyInterestsCollectionViewCell else {return}
        let totalNumberOfItems = collectionView.numberOfItems(inSection: 0)
        cell.btnPrevious.isHidden = indexPath.item == 0
        cell.btnNext.isHidden =  indexPath.item == totalNumberOfItems - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemHeight : CGFloat = 670
        return CGSize(width: collectionViewWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSET
    }
    
    //    func moveToDetailVC(data: ProfileList?){
    //        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
    //        vc.profileList = data
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    
}

extension MyInterestsVC{
    func myInterestProfileDetailsAPI(profile: String) {
        if Reachability.isConnectedToNetwork(){
            APIClient.shared.fetch(with: APIService.userInfoProfile(profileID: profile).request, objectType: .general) { [weak self] (result: Result<UserMoreDetailsModel?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    self.moveToMyInterestDetailVC(data: data)
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func moveToMyInterestDetailVC(data: UserMoreDetailsModel?) {
        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
        vc.myInterestedData = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
