//
//  SettingsVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/12/21.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var imgReport: UIImageView!
    @IBOutlet weak var imgBlock: UIImageView!
    @IBOutlet weak var lblReport: UILabel!
    @IBOutlet weak var lblBlock: UILabel!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnAddToShortlistAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("AddToShortlistTapped"), object: nil)
        }
    }
    
    @IBAction func btnHideStoryAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("HideStoryTapped"), object: nil)
        }
    }
    
    @IBAction func btnShareProfileAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("ShareProfileTapped"), object: nil)
        }
    }
    
    @IBAction func btnReportAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("ReportTapped"), object: nil)
        }
    }
    
    @IBAction func btnBlockAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("BlockTapped"), object: nil)
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideButtons()
    }
    
    func hideButtons(){
        if ViewHandler.shared.isFromMatchesVC{
            btnReport.isHidden = true
            imgReport.isHidden = true
            lblReport.isHidden = true
            btnBlock.isHidden = true
            imgBlock.isHidden = true
            lblBlock.isHidden = true
        }else{
            btnReport.isHidden = false
            imgReport.isHidden = false
            lblReport.isHidden = false
            btnBlock.isHidden = false
            imgBlock.isHidden = false
            lblBlock.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}
