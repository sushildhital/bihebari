//
//  ProfileSettingsVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/14/21.
//

import UIKit

class ProfileSettingsVC: UIViewController {
    
    @IBOutlet weak var btnYourPlan: UIButton!
    @IBOutlet weak var imgYourPlan: UIImageView!
    @IBOutlet weak var lblYourPlan: UILabel!
    @IBOutlet weak var topConstraintChangePassword: NSLayoutConstraint!

    var userID: Int?
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBuildProfileAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("BuildProfileTapped"), object: nil)
        }
    }
    
    @IBAction func btnPartnerPreferencesAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("PartnerPreferencesTapped"), object: nil)
        }
    }
    
    @IBAction func btnMyShortlistsAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("MyShortlistsTapped"), object: nil)
        }
    }
    
    @IBAction func btnMyInterestsAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("MyInterestsTapped"), object: nil)
        }
    }
    
    @IBAction func btnInterestedInMeAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("InterestedInMeTapped"), object: nil)
        }
    }
    
    @IBAction func btnYourPlanAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("YourPlanTapped"), object: nil)
        }
    }
    
    @IBAction func btnChangePasswordAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("ChangePasswordTapped"), object: nil)
        }
    }
    
    @IBAction func btnAppSettingsAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("AppSettingsTapped"), object: nil)
        }
    }
    
    @IBAction func btnRateAppAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("RateAppTapped"), object: nil)
        }
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        let alert = UIAlertController(title: "Bihebari", message: "Are You Sure You Want To Logout??", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            UserDefaults.standard.removeObject(forKey: IS_LOGGED_IN_USER)
            let vc = LoginVC.instantiate(fromAppStoryboard: .Login)
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                let nav = UINavigationController(rootViewController: vc)
                appDelegate.window?.rootViewController = nav
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func btnDeleteAccountAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("DeleteAccountTapped"), object: nil)
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let userID = UserDefaultManager.shared.userDetailsModel?.id ?? 0
//        if userID == 114{
//            btnYourPlan.isHidden = true
//            lblYourPlan.isHidden = true
//            imgYourPlan.isHidden = true
//            topConstraintChangePassword.constant = 25
//        }else{
//            btnYourPlan.isHidden = false
//            lblYourPlan.isHidden = false
//            imgYourPlan.isHidden = false
//            topConstraintChangePassword.constant = 75
//       0 }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}
