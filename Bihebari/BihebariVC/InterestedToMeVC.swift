//
//  InterestedToMeVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/12/21.
//

import UIKit
import IBAnimatable

protocol ViewProfileCollectionViewActionProtocol : AnyObject{
    func viewProfileButtonTapped(cell: InterestedToMeCollectionViewCell)
}

class InterestedToMeVC: UIViewController {
    
    @IBOutlet weak var hiddenView: UIView!
    @IBOutlet weak var collectionInterestedToMe: UICollectionView!
    
    var indexPath: IndexPath?
    var userStatus = ""
    var userId = 0
    
    let NUMBER_OF_COLUMN : CGFloat = 1
    let TOP_INSET : CGFloat = 2
    let BOTTOM_INSET : CGFloat = 2
    let LEFT_INSET : CGFloat = 2
    let RIGHT_INSET : CGFloat = 2
    let SECTION_INSET : CGFloat = 2
    let INTER_ITEM_INSET : CGFloat = 0
    let LINE_SPACE : CGFloat = 0
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let indexPath = indexPath {
            collectionInterestedToMe.isPagingEnabled = false
            collectionInterestedToMe.scrollToItem(at: indexPath, at: .left, animated: false)
            collectionInterestedToMe.isPagingEnabled = true
        }
        collectionInterestedToMe.shouldIgnoreContentInsetAdjustment = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionInterestedToMe.isPagingEnabled = true
        collectionInterestedToMe.register(UINib(nibName: "InterestedToMeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "InterestedToMeCollectionViewCell")
        
        if UserDefaultManager.shared.userDetailsModel?.interested_to_me?.count == 0{
            hiddenView.isHidden = false
        }else{
            hiddenView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension InterestedToMeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserDefaultManager.shared.userDetailsModel?.interested_to_me?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestedToMeCollectionViewCell", for: indexPath) as! InterestedToMeCollectionViewCell
        if let interestedToMeNew = UserDefaultManager.shared.userDetailsModel?.interested_to_me{
            cell.loadCell(interestedToMeObj: interestedToMeNew[indexPath.row])
        }
        cell.delegate = self
        
        cell.cellAction = {action in
            DispatchQueue.main.async {
                collectionView.isPagingEnabled = false
                let row = action == .next ? indexPath.item + 1 : indexPath.item - 1
                collectionView.scrollToItem(at: IndexPath(row: row, section: 0), at: .left, animated: false)
                collectionView.isPagingEnabled = true
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? InterestedToMeCollectionViewCell else {return}
        let totalNumberOfItems = collectionView.numberOfItems(inSection: 0)
        cell.btnPrevious.isHidden = indexPath.item == 0
        cell.btnNext.isHidden =  indexPath.item == totalNumberOfItems - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let itemHeight : CGFloat = 670
        return CGSize(width: collectionViewWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LINE_SPACE
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return INTER_ITEM_INSET
    }
    
    //    func moveToDetailVC(data: ProfileList?){
    //        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
    //        vc.profileList = data
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    
}

extension InterestedToMeVC{
    func interestedToMeProfileDetailsAPI(profile: String) {
        if Reachability.isConnectedToNetwork(){
            APIClient.shared.fetch(with: APIService.userInfoProfile(profileID: profile).request, objectType: .general) { [weak self] (result: Result<UserMoreDetailsModel?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success(let data):
                    self.moveToInterestedToMeDetailVC(data: data)
                case .failure(let error):
                    SwiftMessage.showBanner(title: "Error !!", message: error.localizedDescription, type: .error)
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func moveToInterestedToMeDetailVC(data: UserMoreDetailsModel?) {
        let vc = MatchInfoVC.instantiate(fromAppStoryboard: .MatchInfo)
        vc.interestedToMeData = data
        vc.receivedUserStatus = userStatus
        vc.getUserID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension InterestedToMeVC : ViewProfileCollectionViewActionProtocol{
    func viewProfileButtonTapped(cell: InterestedToMeCollectionViewCell) {
        guard let indexPath = collectionInterestedToMe.indexPath(for: cell) else {return}
        self.userStatus = UserDefaultManager.shared.userDetailsModel?.interested_to_me?[indexPath.row].status ?? ""
        ViewHandler.shared.isFromShortList = false
        ViewHandler.shared.isFromMatchesVC = false
        ViewHandler.shared.isFromMyInterests = false
        ViewHandler.shared.isFromInterestedToMe = true
        self.interestedToMeProfileDetailsAPI(profile: UserDefaultManager.shared.userDetailsModel?.interested_to_me?[indexPath.row].user_code ?? "")
        self.userId = (UserDefaultManager.shared.userDetailsModel?.interested_to_me?[indexPath.row].id ?? 0)
    }
}
