//
//  UnlockVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/12/21.
//

import UIKit
import IBAnimatable

class UnlockVC: UIViewController {
    
    var rupees = ""
    var packageID = 0
    var packageViewColors = [UIColor.init(hexString: "#44CBC6"),UIColor.init(hexString: "#273691"),UIColor.init(hexString: "#CC1C29")]
    
    
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgLock: AnimatableImageView!
    @IBOutlet weak var btnProceed: AnimatableButton!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnProceedAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name("ProceedTapped"), object: nil)
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        
        guard let doubleAmount = Double(rupees) else { return }
        let finalAmount = (doubleAmount * 1.13)
        let anotherAmount = formatter.string(for: finalAmount)
        lblAmount.text = "Rs. \(anotherAmount ?? "")"
        
        switch packageID {
        case 0 :
            imgLock.backgroundColor = packageViewColors[0]
            btnProceed.backgroundColor = packageViewColors[0]
            btnProceed.shadowColor = packageViewColors[0]
        case 1 :
            imgLock.backgroundColor = packageViewColors[1]
            btnProceed.backgroundColor = packageViewColors[1]
            btnProceed.shadowColor = packageViewColors[1]

        case 2 :
            imgLock.backgroundColor = packageViewColors[2]
            btnProceed.backgroundColor = packageViewColors[2]
            btnProceed.shadowColor = packageViewColors[2]

        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}
