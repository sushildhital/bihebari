//
//  ForgotPasswordVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/29/21.
//

import UIKit
import IBAnimatable

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var txtEmail: AnimatableTextField!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        if txtEmail.text == ""{
            txtEmail.attributedPlaceholder = NSAttributedString(string: "Email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmail.becomeFirstResponder()
            }
            return false
        }else if !Util.isValidEmail(testStr: txtEmail.text!){
            txtEmail.text = ""
            txtEmail.attributedPlaceholder = NSAttributedString(string: "Valid email address required",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font :UIFont(name: "Arial", size: 14)!])
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.txtEmail.becomeFirstResponder()
            }
            return false
        }else{
            txtEmail.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        }
        
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnSendNowAction(_ sender: Any) {
        if Util.isConnectedToInternet(){
            if isValid(){
                requestForgotPassword()
            }
        }else{
            Util.showAlert(title: "Oops!", message: "No internet connection..", view: self)
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}

extension ForgotPasswordVC {
    func requestForgotPassword() {
        if Reachability.isConnectedToNetwork(){
            let params : [String : String] = ["email": txtEmail.text!]
            Spinner.start()
            APIClient.shared.fetch(with: APIService.forgotPassword(body: params).request, objectType: .general) { [weak self] (result: Result<ForgotPasswordEmailModal?, APIError>) in
                Spinner.stop()
                guard let `self` = self else {return}
                switch result {
                case .success( _):
                    SwiftMessage.showBanner(title: "Success", message: "Password Change Request Is Sent To Your Email.", type: .success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.requestSuccess()
                    }
                case .failure(let error):
                    print("Failed: \(error.localizedDescription)")
                }
            }
        }else{
            Util.showAlert(title: NO_INTERNET_TITLE, message: NO_INTERNET_MSG, view: self)
        }
    }
    
    func requestSuccess(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

