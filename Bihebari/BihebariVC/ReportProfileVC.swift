//
//  ReportProfileVC.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/15/21.
//

import UIKit

class ReportProfileVC: UIViewController {
    
    var delegate : ReportDelegate?
    
    @IBOutlet weak var lblNotInterested: UILabel!
    @IBOutlet weak var lblInapropriate: UILabel!
    @IBOutlet weak var lblFakeProfile: UILabel!
    @IBOutlet weak var lblInapropriatePhoto: UILabel!
    @IBOutlet weak var lblAlreadyMarried: UILabel!
    @IBOutlet weak var lblBeingViolent: UILabel!
    @IBOutlet weak var lblRude: UILabel!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods
    
    @IBAction func btnCrossAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNotInterestedAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblInapropriate.text!)
            }
        }
    }
    
    @IBAction func btnInappropriateMessage(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblInapropriate.text!)
            }
        }
    }
    
    @IBAction func btnFakeProfileAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblFakeProfile.text!)
            }
        }
    }
    
    @IBAction func btnInappropriatePhotoAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblInapropriatePhoto.text!)
            }
        }
    }
    
    @IBAction func btnAlreadyMarriedAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblAlreadyMarried.text!)
            }
        }
    }
    
    @IBAction func btnViolentAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblBeingViolent.text!)
            }
        }
    }
    
    @IBAction func btnRudeAction(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.delegate != nil{
                self.delegate?.reportDidTap(reason: self.lblRude.text!)
            }
        }
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OthersBtnTapped"), object: nil)
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
}
