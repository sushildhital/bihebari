//
//  AppCoordinator.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/20/21.
//

import Foundation

enum APIError: Error {
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure
    case tokenExired
    case errorMessage(message : String)
    
    
    
    var localizedDescription: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        case .tokenExired: return "Token Expired"
        case .errorMessage(let message): return message
        }
    }
    
}

struct BaseResponse<T: Codable> : Codable {
    let status : String?
    let message : String?
    let data : T?

    enum CodingKeys: String, CodingKey {

        case status = "success"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(T.self, forKey: .data)
    }

}


enum ResponseType {
    case general, fromData
}

class APIClient {
    static let shared = APIClient()
    
    private func decodingTask<T: Codable>(with request: URLRequest, object: ResponseType, decodingType: T.Type, completionHandler completion: @escaping (T?, APIError?) -> Void) -> URLSessionDataTask {
        
        
        let task = URLSession(configuration: .default).dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, .errorMessage(message: error?.localizedDescription ?? "Request Failed"))
                return
            }
            
            
            let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
            
//            print(httpResponse.statusCode)
            if (200...299).contains(httpResponse.statusCode) {
                if let data = data {
                    do {
                        if object == .general {
                            let genericModel = try JSONDecoder().decode(decodingType, from: data)
                            completion(genericModel, nil)
                        }else {
                            let genericModel = try JSONDecoder().decode(BaseResponse<T>.self, from: data)
                            completion(genericModel.data, nil)
                        }
                        
                    } catch let error{
                        print(error)
                        completion(nil, .jsonConversionFailure)
                    }
                } else {
                    completion(nil, .invalidData)
                }
            }
            else{
                
                if let json = json as? NSDictionary{
                    
                    if let errorDict  = json["error"] as? NSDictionary{
                        if let firstKey = errorDict.allKeys.first as? String {
                            if let firstValue = errorDict[firstKey] as? [String], firstValue.count > 0 {
                                completion(nil,.errorMessage(message: firstValue[0]))
                            }
                        }
                    }
                    
                    if let message  = json["error"] as? String{
                        completion(nil,.errorMessage(message: message))
                    }
                }else{
                    completion(nil, .requestFailed)
                }
            }
        }
        return task
    }
    
    func fetch<T: Codable>(with request: URLRequest, objectType: ResponseType, completion: @escaping (Result<T, APIError>) -> Void) {
        let task = decodingTask(with: request, object: objectType, decodingType: T.self) { (json , error) in
//            print(request.url?.absoluteString ?? "")
            //MARK: change to main queue
            DispatchQueue.main.async {
                guard let json = json else {
                    
                    if let error = error {
                        completion(.failure(error))
                    } else {
                        completion(.failure(.invalidData))
                    }
                    return
                }
                
                completion(.success(json))

            }
        }
        task.resume()
    }
}

