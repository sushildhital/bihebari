//
//  NetworkHandler.swift
//  Bihebari
//
//  Created by Sushil Dhital on 23/07/2021.
//

import UIKit
import Alamofire

class NetworkHandler: NSObject {

    static let shared = NetworkHandler()
    
    func markInterestedPost(profileId: String, success:@escaping (_ status: String?) -> (), failure: @escaping (_ message:String) -> ()){
        let params = ["profileId": profileId] as [String : Any]
        let header: [String: String] = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
        Alamofire.request(Constants.BASE_URL + Constants.markInterested, method: .post, parameters: params, encoding : URLEncoding.default, headers: header).responseJSON { response in
            guard let responseDict = response.result.value as? [String:Any] else{
                print("Error: \(String(describing: response.result.error))")
                failure(response.result.error?.localizedDescription ?? "Mark interested API failed..")
                return
            }
//            print("response: \(responseDict)")
            success(responseDict["success"] as? String)
        }
    }
}
