//
//  APIHandler.swift
//  Bihebari
//
//  Created by Leoan Ranjit on 10/27/21.
//

import UIKit
import Alamofire

class APIHandler: NSObject {

    static let shared = APIHandler()
    
    func deleteSingleMsg(id:Int, success:@escaping(_ message: String) -> (), failure:@escaping (_ message: String) -> ()){
        let header = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
        Alamofire.request("\(Constants.BASE_URL)/api/v1/delete/\(id)/message", method: .post, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
//            print("respo: \(response.result.value)")
            guard let responseNew = response.result.value as? [String: Any] else {
                SwiftMessage.showBanner(title: "Error", message: "Could not delete message.", type: .error)
                return
            }
            print("KAA HO ******** \(responseNew) EHH MA YA CHOWK TIRA *****")
                        
        }
        success("k xa")
    }
    
    func deleteAllMsg(firstUserID : String, secondUserID: String, success:@escaping(_ message: String) -> (), failure:@escaping (_ message: String) -> ()){
        let header = ["Authorization" : "Bearer \(UserDefaultManager.shared.accessToken ?? "")"]
        let params = ["first_user_id" : "\(firstUserID)", "second_user_id" : "\(secondUserID)"]
        Alamofire.request("\(Constants.BASE_URL)/api/v1/delete/all/message", method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            guard let responseNew = response.result.value as? [String: Any] else {
                SwiftMessage.showBanner(title: "Error", message: "Could not delete message.", type: .error)
                return
            }
//            print("EHH MA YA CHOWK TIRA ******** \(responseNew) CHOWK TIRA KTA HO CHOWK TIRA *****")
                        
        }
        success("MA ABO VR KINCHU")
    }
}
