//
//  AppCoordinator.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/20/21.
//

import Foundation

struct MultipartMediaType{
    var data: Data
    var mimeType: String
    var keyName: String
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol Endpoint {
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var httpHeaders: [String : String]? { get }
    var params: [String : String]? { get }
    var data: [String : Any]? { get }
    var files: [MultipartMediaType]? {get}
}

extension Endpoint {
    var base: String {
        return Constants.BASE_URL
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        var items = [URLQueryItem]()
        
        if let params = self.params{
            for (key,value) in params {
                items.append(URLQueryItem(name: key, value: value))
            }
        }
        
        items = items.filter{!$0.name.isEmpty}
        
        if !items.isEmpty {
            components.queryItems = items
        }
        return components
    }
    
    var request: URLRequest {
        let url = urlComponents.url!
        var request = URLRequest(url: url,timeoutInterval: files == nil ? 60 : 500)
        if httpMethod == .post {
            request.httpMethod = HTTPMethod.post.rawValue
            
            if files == nil {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                if let data = data {
                    let paramsData = try? JSONSerialization.data(withJSONObject: data)
                    request.httpBody = paramsData
                }
            }else {
                ///For multipart
                let boundary = "Boundary-\(UUID().uuidString)"
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                //                let mimeType = "image/jpeg"
                
                var body = Data()
                let boundaryPrefix = "--\(boundary)\r\n"
                
                if let params = self.params{
                    for (key, value) in params {
                        body.append(boundaryPrefix.data(using: .utf8)!)
                        body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                        body.append("\(value)\r\n".data(using: .utf8)!)
                    }
                }
                
                if let data = self.data{
                    for (key, value) in data {
                        if let value = value as? [Any] {
                            value.forEach { (val) in
                                body.append(boundaryPrefix.data(using: .utf8)!)
                                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                                body.append("\(val)\r\n".data(using: .utf8)!)
                            }
                        }else {
                            body.append(boundaryPrefix.data(using: .utf8)!)
                            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                            body.append("\(value)\r\n".data(using: .utf8)!)
                        }
                    }
                }
                
                var filename = String()
                for (index,file) in (files ?? []).enumerated(){
                    filename = "file\(index).\(file.mimeType)"
                    
                    body.append(boundaryPrefix.data(using: .utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(file.keyName)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
                    body.append("Content-Type: \"content-type header\"\r\n\r\n".data(using: .utf8)!)
                    body.append(file.data)
                    body.append("\r\n".data(using: .utf8)!)
                    
                }
                body.append("--\(boundary)--\r\n".data(using: .utf8)!)
                //                body.append(" — ".appending(boundary.appending(" — ")).data(using: .utf8)!)
                request.httpBody = body
            }
            
            
        } else {
            request.httpMethod = HTTPMethod.get.rawValue
        }
        
        //Set headers
        if let requestHeaders = httpHeaders {
            for (key, value) in requestHeaders {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        //        let basicCred = "clientId:secret".data(using: .utf8)!.base64EncodedString()
        //        request.setValue("Basic \(basicCred)", forHTTPHeaderField: "Authorization")
        
        
        if let authorizationCode = UserDefaultManager.shared.accessToken {
            request.setValue("Bearer " + authorizationCode, forHTTPHeaderField: "Authorization")
            print(authorizationCode)
        }
        
        return request
    }
    
    
}

//MARK:- Login
enum APIService {
    case userLogin(body: [String: Any])
    case userRegister(body: [String: Any])
    case findSomeone
    case Home(body: [String: String])
    case country
    case currentlyLivingIn
    case education
    case Kundali
    case myProfile(header: [String : String])
    case accept(header: [String : String], profileID: Int, profileCode: String)
    case reject(header: [String : String], profileID: Int, profileCode: String)
    case userInfoProfile(profileID: String)
    case paymentOption
    case packageUpgrade
    case markShortlist(body: [String : Any], header: [String : String])
    case addToShortlist(body: [String : Any], header: [String : String])
    case markInterested(body: [String : Any], header: [String : String])
    case updateGeneral(body: [String : Any], header: [String : String])
    case updateKundali(body: [String : Any], header: [String : String])
    case updateEducation(body: [String : Any], header: [String : String])
    case updatePartnerPreferences(body: [String : Any], header: [String : String])
    //    case updateDetail(body: [String : Any], header: [String : String], files: [MultipartMediaType]?)
    case verifyYourself
    case verifyOTP(body: [String: Any])
    case notificationToken(body: [String : Any], header: [String : String])
    case getShortlist(header: [String : String])
    case removeShortlist(profileId: Int)
    case changePassword(body: [String: Any])
    case addStory(files: [MultipartMediaType]?)
    case addPhoto(files: [MultipartMediaType]?)
    case addUploadPhoto(files: [MultipartMediaType]?)
    case getPartnerPreferences
    case blockUser(header: [String : String], profileID: Int, profileCode: String)
    case unblockUser(header: [String : String], profileID: Int, profileCode: String)
    case blockedList(header: [String : String])
    case hideStory(body: [String : Any], header: [String : String])
    case forgotPassword(body: [String: Any])
    case reportProfile(body: [String : Any], header: [String : String])
    case deleteAccount(body: [String : Any], header: [String : String])
    case removeProfilePhoto(header: [String : String], imageID: Int)
    case removeStoryPhoto(header: [String : String], imageID: Int)
    case applePay(body: [String: Any])
}

extension APIService : Endpoint {
    var path: String {
        switch  self {
        case .userLogin(_):
            return Constants.Login
        case .findSomeone:
            return Constants.findSomeone
        case .Home:
            return Constants.Home
        case .userRegister(_):
            return Constants.Register
        case .country:
            return Constants.Country
        case .currentlyLivingIn:
            return Constants.GeneralProfile
        case .education:
            return Constants.EducationProfile
        case .Kundali:
            return Constants.Kundali
        case .myProfile(_):
            return Constants.MyProfile
        case .updateEducation:
            return Constants.UpdateEducation
        case .updateKundali:
            return Constants.UpdateKundali
        case .updateGeneral:
            return Constants.UpdateGeneral
        case .updatePartnerPreferences:
            return Constants.updatePartnerPreferences
        case .accept( _, let id, let code):
            return Constants.interested_in_accept + "\(code)" + "/accept/" + "\(id)"
        case .reject( _, let id, let code):
            return Constants.interested_in_reject + "\(code)" + "/reject/" + "\(id)"
        case .userInfoProfile(let id):
            return Constants.interested_user_profile + id
        case .paymentOption:
            return Constants.paymentOption
        case .packageUpgrade:
            return Constants.upgradePackage
        case .markShortlist:
            return Constants.markShortlist
        case .addToShortlist:
            return Constants.markShortlist
        case .markInterested:
            return Constants.markInterested
        case .verifyYourself:
            return Constants.verifyYourself
        case .verifyOTP(_):
            return Constants.verifyOTP
        case .notificationToken:
            return Constants.notificationToken
        case .getShortlist(_):
            return Constants.getShortlist
        case .removeShortlist(let id):
            print("url is: \(Constants.removeFromShortlist + "\(id)" + "/shortlist")")
            return Constants.removeFromShortlist + "\(id)" + "/shortlist"
        case .changePassword(_):
            return Constants.changePassword
        case .addStory:
            return  Constants.addStory
        case .addPhoto:
            return  Constants.addPhoto
        case .addUploadPhoto:
            return Constants.addUplaodPic
        case .getPartnerPreferences:
            return Constants.getPartnerPreferences
        case .blockUser( _, let id, let code):
            print("url: \(Constants.blockUser + "\(code)" + "/block/" + "\(id)")")
            return Constants.blockUser + "\(code)" + "/block/" + "\(id)"
        case .unblockUser( _, let id, let code):
            return Constants.unblockUser + "\(code)" + "/unblock/" + "\(id)"
        case .blockedList(_):
            return Constants.getBlockedList
        case .hideStory:
            return Constants.hideStory
        case .forgotPassword(_):
            return Constants.forgotPassword
        case .reportProfile:
            return Constants.reportProfile
        case .deleteAccount:
            return Constants.deleteAccount
        case .removeProfilePhoto( _, let id):
            return Constants.removeProfilePhoto + "\(id)" + "/photo"
        case .removeStoryPhoto( _, let id):
            return Constants.removeStoryPhoto + "\(id)" + "/photo"
        case .applePay(_):
            return Constants.applePay
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .userLogin(_), .userRegister(_) ,.applePay(_):
            return .post
        case .findSomeone, .Home, .country,.education,.currentlyLivingIn,
             .Kundali,.myProfile,.accept, .reject, .userInfoProfile,.paymentOption,.removeProfilePhoto, .removeStoryPhoto,.packageUpgrade,.getPartnerPreferences,
             .blockUser,.unblockUser:
            return .get
        case .updateEducation:
            return .post
        case .updateKundali:
            return .post
        case .updateGeneral:
            return .post
        case .updatePartnerPreferences:
            return .post
        
        case .markShortlist:
            return .post
        case .addToShortlist:
            return .post
        case .markInterested:
            return .post
        case .hideStory:
            return .post
        case .verifyYourself:
            return .post
        case .verifyOTP(_):
            return .post
        case .notificationToken:
            return .post
        case .getShortlist(_):
            return .get
        case .removeShortlist:
            return .get
        case .changePassword(_), .addStory:
            return .post
        case .addPhoto,.addUploadPhoto:
            return .post
            
        case .blockedList(_):
            return .get
        case .forgotPassword(_):
            return .post
        case .reportProfile:
            return .post
        case .deleteAccount:
            return.post
            
        }
    }
    
    var httpHeaders: [String : String]? {
        switch self {
        case .myProfile(let header), .getShortlist(header: let header), .unblockUser(header: let header, _, _), .blockedList(header: let header), .blockUser(header: let header, _, _), .accept(header: let header, _, _), .reject(header: let header, _, _), .removeProfilePhoto(header: let header, _), .removeStoryPhoto(header: let header, _):
            return header
            
        default:
            return nil
        }
    }
    
    var params: [String : String]? {
        switch self {
        case .Home(let params):
            return params
        default:
            return nil
        }
    }
    
    var data: [String : Any]? {
        switch self {
        case .userLogin(let data), .userRegister(body: let data), .verifyOTP(body: let data), .markShortlist(let data, _), .addToShortlist(let data, _), .hideStory(let data, _), .markInterested(let data, _), .changePassword(body: let data), .updateGeneral(let data, _), .updateKundali(let data, _), .updateEducation(let data, _), .updatePartnerPreferences(let data, _), .notificationToken(body: let data, _), .forgotPassword(body: let data), .reportProfile(let data, _), .deleteAccount(let data, _),.applePay(let data):
            return data
      
        default:
            return nil
        }
    }
    
    var files: [MultipartMediaType]? {
        switch self {
        case .addStory(let files), .addPhoto(files: let files),.addUploadPhoto(files: let files):
            return files
        default:
            return nil
        }
    }
}

