//
//  FindSomeoneModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/20/21.
//

import Foundation

struct FindSomeoneModal : Codable {
    let success : String?
    let data : CasteModal?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(CasteModal.self, forKey: .data)
    }
}

struct CasteModal : Codable {
    let casts : [Casts]?
    let religions : [Religions]?
    let marital_status : [Marital_status]?

    enum CodingKeys: String, CodingKey {

        case casts = "casts"
        case religions = "religions"
        case marital_status = "marital_status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        casts = try values.decodeIfPresent([Casts].self, forKey: .casts)
        religions = try values.decodeIfPresent([Religions].self, forKey: .religions)
        marital_status = try values.decodeIfPresent([Marital_status].self, forKey: .marital_status)
    }
}

struct Marital_status : Codable {
    let name : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case value = "value"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        value = try values.decodeIfPresent(String.self, forKey: .value)
    }

}


struct Religions : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Casts : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}
