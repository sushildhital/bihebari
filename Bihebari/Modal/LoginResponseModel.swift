//
//  LoginResponseModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/19/21.
//

import Foundation
import UIKit

// MARK:- NEW BASE MODEL HERE

struct LoginResponseModal : Codable {
    let success : LoginSuccessModal?
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(LoginSuccessModal.self, forKey: .success)
    }
    
}

struct ProfileSuccessModal : Codable {
    let success : String?
    let data : ProfileModal?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(ProfileModal.self, forKey: .data)
    }

}

struct ProfileModal: Codable{
    let userImagePath: String?
    let user : LoginUserModal?
    let redirect_to_build : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case user = "user"
        case userImagePath = "userImagePath"
        case redirect_to_build = "redirect_to_build"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        user = try values.decodeIfPresent(LoginUserModal.self, forKey: .user)
        redirect_to_build = try values.decodeIfPresent(Bool.self, forKey: .redirect_to_build)
        
    }
}

struct LoginSuccessModal : Codable {
    let token : String?
    let user : LoginUserModal?
    let userImagePath : String?
    let redirect_to_build : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case token = "token"
        case user = "user"
        case userImagePath = "userImagePath"
        case redirect_to_build = "redirect_to_build"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        user = try values.decodeIfPresent(LoginUserModal.self, forKey: .user)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        redirect_to_build = try values.decodeIfPresent(Bool.self, forKey: .redirect_to_build)
    }
    
}

class LoginUserModal : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let email : String?
    let email_verified_at : String?
    let phone : String?
    var photo : String?
    let profile_for : String?
    let otp_code : String?
    let otp_time : String?
    let otp_status : String?
    let email_otp_status : String?
    let approved : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let deactivate : Int?
    let reason : String?
    let deactivate_request_date : String?
    let deleted_at : String?
    let online : String?
    let socket_id : String?
    let notification_token : String?
    let interested_in : [LoginInterested_inModal]?
    let interested_to_me : [Interested_to_me]?
    let profile_favourite : [LoginProfile_favouriteModal]?
    let blocked_profiles : [BlockedProfiles]?
    let check_user_interest : Check_user_interest?
    let get_account_address : LoginGet_account_addressModal?
    let get_current_address : LoginGet_current_addressModal?
    let get_permanent_address : Get_permanent_address?
    let profile_general : LoginProfile_generalModal?
    let profile_profession : Profile_profession?
    let profile_kundalini_detail : LoginProfile_kundalini_detailModal?
    let profile_figure : Profile_figure?
    let account_image : String?
    let profile_image : LoginProfile_imageModal?
    var other_image : [Other_image]?
    var story_image : [LoginStory_image]?
    let package : LoginPackageModal?
    let partner_details : Partner_details?
    let blocked_user : [BlockedUser]?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case phone = "phone"
        case photo = "photo"
        case profile_for = "profile_for"
        case otp_code = "otp_code"
        case otp_time = "otp_time"
        case otp_status = "otp_status"
        case email_otp_status = "email_otp_status"
        case approved = "approved"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case deactivate = "deactivate"
        case reason = "reason"
        case deactivate_request_date = "deactivate_request_date"
        case deleted_at = "deleted_at"
        case online = "online"
        case socket_id = "socket_id"
        case notification_token = "notification_token"
        case interested_in = "interested_in"
        case interested_to_me = "interested_to_me"
        case profile_favourite = "profile_favourite"
        case blocked_profiles = "blocked_profiles"
        case check_user_interest = "check_user_interest"
        case get_account_address = "get_account_address"
        case get_current_address = "get_current_address"
        case get_permanent_address = "get_permanent_address"
        case profile_general = "profile_general"
        case profile_profession = "profile_profession"
        case profile_kundalini_detail = "profile_kundalini_detail"
        case profile_figure = "profile_figure"
        case account_image = "account_image"
        case profile_image = "profile_image"
        case other_image = "other_image"
        case story_image = "story_image"
        case package = "package"
        case partner_details = "partner_details"
        case blocked_user = "blocked_user"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        profile_for = try values.decodeIfPresent(String.self, forKey: .profile_for)
        otp_code = try values.decodeIfPresent(String.self, forKey: .otp_code)
        otp_time = try values.decodeIfPresent(String.self, forKey: .otp_time)
        otp_status = try values.decodeIfPresent(String.self, forKey: .otp_status)
        email_otp_status = try values.decodeIfPresent(String.self, forKey: .email_otp_status)
        approved = try values.decodeIfPresent(String.self, forKey: .approved)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        deactivate = try values.decodeIfPresent(Int.self, forKey: .deactivate)
        reason = try values.decodeIfPresent(String.self, forKey: .reason)
        deactivate_request_date = try values.decodeIfPresent(String.self, forKey: .deactivate_request_date)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        online = try values.decodeIfPresent(String.self, forKey: .online)
        socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
        notification_token = try values.decodeIfPresent(String.self, forKey: .notification_token)
        interested_in = try values.decodeIfPresent([LoginInterested_inModal].self, forKey: .interested_in)
        interested_to_me = try values.decodeIfPresent([Interested_to_me].self, forKey: .interested_to_me)
        profile_favourite = try values.decodeIfPresent([LoginProfile_favouriteModal].self, forKey: .profile_favourite)
        blocked_profiles = try values.decodeIfPresent([BlockedProfiles].self, forKey: .blocked_profiles)
        check_user_interest = try values.decodeIfPresent(Check_user_interest.self, forKey: .check_user_interest)
        get_account_address = try values.decodeIfPresent(LoginGet_account_addressModal.self, forKey: .get_account_address)
        get_current_address = try values.decodeIfPresent(LoginGet_current_addressModal.self, forKey: .get_current_address)
        get_permanent_address = try values.decodeIfPresent(Get_permanent_address.self, forKey: .get_permanent_address)
        profile_general = try values.decodeIfPresent(LoginProfile_generalModal.self, forKey: .profile_general)
        profile_profession = try values.decodeIfPresent(Profile_profession.self, forKey: .profile_profession)
        profile_kundalini_detail = try values.decodeIfPresent(LoginProfile_kundalini_detailModal.self, forKey: .profile_kundalini_detail)
        profile_figure = try values.decodeIfPresent(Profile_figure.self, forKey: .profile_figure)
        account_image = try values.decodeIfPresent(String.self, forKey: .account_image)
        profile_image = try values.decodeIfPresent(LoginProfile_imageModal.self, forKey: .profile_image)
        other_image = try values.decodeIfPresent([Other_image].self, forKey: .other_image)
        story_image = try values.decodeIfPresent([LoginStory_image].self, forKey: .story_image)
        package = try values.decodeIfPresent(LoginPackageModal.self, forKey: .package)
        partner_details = try values.decodeIfPresent(Partner_details.self, forKey: .partner_details)
        blocked_user = try values.decodeIfPresent([BlockedUser].self, forKey: .blocked_user)
    }
    
}


struct LoginInterested_inModal : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let image : String?
    let address : String?
    let profession : String?
    let age : Int?
    let height_ft : Int?
    let height_inch : Int?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case image = "image"
        case address = "address"
        case profession = "profession"
        case age = "age"
        case height_ft = "height_ft"
        case height_inch = "height_inch"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        profession = try values.decodeIfPresent(String.self, forKey: .profession)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
        height_inch = try values.decodeIfPresent(Int.self, forKey: .height_inch)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}

struct Interested_to_me : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let email : String?
    let email_verified_at : String?
    let phone : String?
    let photo : String?
    let profile_for : String?
    let otp_code : String?
    let otp_time : String?
    let otp_status : String?
    let email_otp_status : String?
    let approved : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let deactivate : Int?
    let reason : String?
    let deactivate_request_date : String?
    let deleted_at : String?
    let online : String?
    let socket_id : String?
    let notification_token : String?
    let profile_general : Profile_general?
    let profile_image : Profile_image?
    let get_permanent_address : Get_permanent_address?
    let profile_profession : Profile_profession?
    let profile_figure : Profile_figure?
    let image : String?
    let address : String?
    let profession : String?
    let age : Int?
    let height_ft : Int?
    let height_inch : Int?
    
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case phone = "phone"
        case photo = "photo"
        case profile_for = "profile_for"
        case otp_code = "otp_code"
        case otp_time = "otp_time"
        case otp_status = "otp_status"
        case email_otp_status = "email_otp_status"
        case approved = "approved"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case deactivate = "deactivate"
        case reason = "reason"
        case deactivate_request_date = "deactivate_request_date"
        case deleted_at = "deleted_at"
        case online = "online"
        case socket_id = "socket_id"
        case notification_token = "notification_token"
        case profile_general = "profile_general"
        case profile_image = "profile_image"
        case get_permanent_address = "get_permanent_address"
        case profile_profession = "profile_profession"
        case profile_figure = "profile_figure"
        
        case image = "image"
        case address = "address"
        case profession = "profession"
        case age = "age"
        case height_ft = "height_ft"
        case height_inch = "height_inch"
        
    }

init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    id = try values.decodeIfPresent(Int.self, forKey: .id)
    user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
    name = try values.decodeIfPresent(String.self, forKey: .name)
    email = try values.decodeIfPresent(String.self, forKey: .email)
    email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
    phone = try values.decodeIfPresent(String.self, forKey: .phone)
    photo = try values.decodeIfPresent(String.self, forKey: .photo)
    profile_for = try values.decodeIfPresent(String.self, forKey: .profile_for)
    otp_code = try values.decodeIfPresent(String.self, forKey: .otp_code)
    otp_time = try values.decodeIfPresent(String.self, forKey: .otp_time)
    otp_status = try values.decodeIfPresent(String.self, forKey: .otp_status)
    email_otp_status = try values.decodeIfPresent(String.self, forKey: .email_otp_status)
    approved = try values.decodeIfPresent(String.self, forKey: .approved)
    status = try values.decodeIfPresent(String.self, forKey: .status)
    created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
    updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    deactivate = try values.decodeIfPresent(Int.self, forKey: .deactivate)
    reason = try values.decodeIfPresent(String.self, forKey: .reason)
    deactivate_request_date = try values.decodeIfPresent(String.self, forKey: .deactivate_request_date)
    deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
    online = try values.decodeIfPresent(String.self, forKey: .online)
    socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
    notification_token = try values.decodeIfPresent(String.self, forKey: .notification_token)
    profile_general = try values.decodeIfPresent(Profile_general.self, forKey: .profile_general)
    profile_image = try values.decodeIfPresent(Profile_image.self, forKey: .profile_image)
    get_permanent_address = try values.decodeIfPresent(Get_permanent_address.self, forKey: .get_permanent_address)
    profile_profession = try values.decodeIfPresent(Profile_profession.self, forKey: .profile_profession)
    profile_figure = try values.decodeIfPresent(Profile_figure.self, forKey: .profile_figure)
    
    
    image = try values.decodeIfPresent(String.self, forKey: .image)
    address = try values.decodeIfPresent(String.self, forKey: .address)
    profession = try values.decodeIfPresent(String.self, forKey: .profession)
    age = try values.decodeIfPresent(Int.self, forKey: .age)
    height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
    height_inch = try values.decodeIfPresent(Int.self, forKey: .height_inch)
    
}

}

struct LoginProfile_favouriteModal : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let image : String?
    let address : String?
    let profession : String?
    let age : Int?
    let height_ft : Int?
    let height_inch : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case image = "image"
        case address = "address"
        case profession = "profession"
        case age = "age"
        case height_ft = "height_ft"
        case height_inch = "height_inch"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        profession = try values.decodeIfPresent(String.self, forKey: .profession)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
        height_inch = try values.decodeIfPresent(Int.self, forKey: .height_inch)
    }
    
}


struct LoginGet_account_addressModal : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : String?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : CountriesList?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(String.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(CountriesList.self, forKey: .country)
    }
    
}

struct LoginProfile_kundalini_detailModal : Codable {
    let id : Int?
    let user_id : Int?
    let date_of_birth : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let time_of_birth : String?
    let birth_place : String?
    let rashi_id : Int?
    let gotra_id : Int?
    let religion_id : Int?
    let is_mangalic : String?
    let created_at : String?
    let updated_at : String?
    let rashi_name : String?
    let rashi_nepali_name : String?
    let gotra_name : String?
    let religion_name : String?
    let rashi : Rashi?
    let gotra : Gotra?
    let religion : Religion?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case date_of_birth = "date_of_birth"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case time_of_birth = "time_of_birth"
        case birth_place = "birth_place"
        case rashi_id = "rashi_id"
        case gotra_id = "gotra_id"
        case religion_id = "religion_id"
        case is_mangalic = "is_mangalic"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case rashi_name = "rashi_name"
        case rashi_nepali_name = "rashi_nepali_name"
        case gotra_name = "gotra_name"
        case religion_name = "religion_name"
        case rashi = "rashi"
        case gotra = "gotra"
        case religion = "religion"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        time_of_birth = try values.decodeIfPresent(String.self, forKey: .time_of_birth)
        birth_place = try values.decodeIfPresent(String.self, forKey: .birth_place)
        rashi_id = try values.decodeIfPresent(Int.self, forKey: .rashi_id)
        gotra_id = try values.decodeIfPresent(Int.self, forKey: .gotra_id)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        is_mangalic = try values.decodeIfPresent(String.self, forKey: .is_mangalic)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        rashi_name = try values.decodeIfPresent(String.self, forKey: .rashi_name)
        rashi_nepali_name = try values.decodeIfPresent(String.self, forKey: .rashi_nepali_name)
        gotra_name = try values.decodeIfPresent(String.self, forKey: .gotra_name)
        religion_name = try values.decodeIfPresent(String.self, forKey: .religion_name)
        rashi = try values.decodeIfPresent(Rashi.self, forKey: .rashi)
        gotra = try values.decodeIfPresent(Gotra.self, forKey: .gotra)
        religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
    }
    
}


struct LoginProfile_imageModal : Codable {
    
    let id : Int?
    let user_id : Int?
    let type : String?
    let photo : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case photo = "photo"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}

struct LoginPackageModal : Codable {
    let id : Int?
    let user_id : Int?
    let package_id : Int?
    let payemnt_partner : String?
    let transition_id : String?
    let transition_date : String?
    let expiry_date : String?
    let transition_currency : String?
    let transition_amount : Double?
    let package_amount : Int?
    let service_charge : Double?
    let referance_id : String?
    let particulars : String?
    let remarks : String?
    let payment_status : String?
    let payment_remarks : String?
    let created_at : String?
    let updated_at : String?
    let status : Int?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case package_id = "package_id"
        case payemnt_partner = "payemnt_partner"
        case transition_id = "transition_id"
        case transition_date = "transition_date"
        case expiry_date = "expiry_date"
        case transition_currency = "transition_currency"
        case transition_amount = "transition_amount"
        case package_amount = "package_amount"
        case service_charge = "service_charge"
        case referance_id = "referance_id"
        case particulars = "particulars"
        case remarks = "remarks"
        case payment_status = "payment_status"
        case payment_remarks = "payment_remarks"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case status = "status"
        case type = "type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        package_id = try values.decodeIfPresent(Int.self, forKey: .package_id)
        payemnt_partner = try values.decodeIfPresent(String.self, forKey: .payemnt_partner)
        transition_id = try values.decodeIfPresent(String.self, forKey: .transition_id)
        transition_date = try values.decodeIfPresent(String.self, forKey: .transition_date)
        expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
        transition_currency = try values.decodeIfPresent(String.self, forKey: .transition_currency)
        transition_amount = try values.decodeIfPresent(Double.self, forKey: .transition_amount)
        package_amount = try values.decodeIfPresent(Int.self, forKey: .package_amount)
        service_charge = try values.decodeIfPresent(Double.self, forKey: .service_charge)
        referance_id = try values.decodeIfPresent(String.self, forKey: .referance_id)
        particulars = try values.decodeIfPresent(String.self, forKey: .particulars)
        remarks = try values.decodeIfPresent(String.self, forKey: .remarks)
        payment_status = try values.decodeIfPresent(String.self, forKey: .payment_status)
        payment_remarks = try values.decodeIfPresent(String.self, forKey: .payment_remarks)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
}


struct Partner_details : Codable {
    let id : Int?
    let user_id : Int?
    let caste_id : Int?
    let religion_id : Int?
    let age_from : Int?
    let age_to : Int?
    let education_id : Int?
    let profession_id : Int?
    let address : String?
    let skin_color_id : Int?
    let gotra_id : Int?
    let rashi_id : Int?
    let is_mangalic : String?
    let vegetarian : String?
    let extra : String?
    let created_at : String?
    let updated_at : String?
    let mother_tongue_id : Int?
    let height : Int?
    let education_degree_name : String?
    let income : Int?
    let maritial_status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case caste_id = "caste_id"
        case religion_id = "religion_id"
        case age_from = "age_from"
        case age_to = "age_to"
        case education_id = "education_id"
        case profession_id = "profession_id"
        case address = "address"
        case skin_color_id = "skin_color_id"
        case gotra_id = "gotra_id"
        case rashi_id = "rashi_id"
        case is_mangalic = "is_mangalic"
        case vegetarian = "vegetarian"
        case extra = "extra"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case mother_tongue_id = "mother_tongue_id"
        case height = "height"
        case education_degree_name = "education_degree_name"
        case income = "income"
        case maritial_status = "maritial_status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        caste_id = try values.decodeIfPresent(Int.self, forKey: .caste_id)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        age_from = try values.decodeIfPresent(Int.self, forKey: .age_from)
        age_to = try values.decodeIfPresent(Int.self, forKey: .age_to)
        education_id = try values.decodeIfPresent(Int.self, forKey: .education_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        skin_color_id = try values.decodeIfPresent(Int.self, forKey: .skin_color_id)
        gotra_id = try values.decodeIfPresent(Int.self, forKey: .gotra_id)
        rashi_id = try values.decodeIfPresent(Int.self, forKey: .rashi_id)
        is_mangalic = try values.decodeIfPresent(String.self, forKey: .is_mangalic)
        vegetarian = try values.decodeIfPresent(String.self, forKey: .vegetarian)
        extra = try values.decodeIfPresent(String.self, forKey: .extra)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        mother_tongue_id = try values.decodeIfPresent(Int.self, forKey: .mother_tongue_id)
        height = try values.decodeIfPresent(Int.self, forKey: .height)
        education_degree_name = try values.decodeIfPresent(String.self, forKey: .education_degree_name)
        income = try values.decodeIfPresent(Int.self, forKey: .income)
        maritial_status = try values.decodeIfPresent(String.self, forKey: .maritial_status)
    }
    
}

struct LoginProfile_generalModal : Codable {
    let id : Int?
    let user_id : Int?
    let first_name : String?
    let middle_name : String?
    let last_name : String?
    let nick_name : String?
    let gender : String?
    let dob : String?
    let dob_bs : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let marrital_status : String?
    let religion_id : Int?
    let caste_id : Int?
    let sector_id : Int?
    let mother_tongue_id : Int?
    let profession_id : Int?
    let profession_name : String?
    let education_id : Int?
    let education_degree : String?
    let phone_primary : String?
    let phone_secondary : String?
    let email_address : String?
    let facebook_link : String?
    let linkedin_link : String?
    let instagram_link : String?
    let about : String?
    let extra : String?
    let created_at : String?
    let updated_at : String?
    let relegion_name : String?
    let caste_name : String?
    let mother_tounge_name : String?
    let sectors_name : String?
    let education_name : String?
    let religion : Religion?
    let caste : Caste?
    let mother_tounge : Mother_tongue?
    let sector : Sector?
    let education : Education?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case nick_name = "nick_name"
        case gender = "gender"
        case dob = "dob"
        case dob_bs = "dob_bs"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case marrital_status = "marrital_status"
        case religion_id = "religion_id"
        case caste_id = "caste_id"
        case sector_id = "sector_id"
        case mother_tongue_id = "mother_tongue_id"
        case profession_id = "profession_id"
        case profession_name = "profession_name"
        case education_id = "education_id"
        case education_degree = "education_degree"
        case phone_primary = "phone_primary"
        case phone_secondary = "phone_secondary"
        case email_address = "email_address"
        case facebook_link = "facebook_link"
        case linkedin_link = "linkedin_link"
        case instagram_link = "instagram_link"
        case about = "about"
        case extra = "extra"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case relegion_name = "relegion_name"
        case caste_name = "caste_name"
        case mother_tounge_name = "mother_tounge_name"
        case sectors_name = "sectors_name"
        case education_name = "education_name"
        case religion = "religion"
        case caste = "caste"
        case mother_tounge = "mother_tounge"
        case sector = "sector"
        case education = "education"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        middle_name = try values.decodeIfPresent(String.self, forKey: .middle_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        nick_name = try values.decodeIfPresent(String.self, forKey: .nick_name)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        dob_bs = try values.decodeIfPresent(String.self, forKey: .dob_bs)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        marrital_status = try values.decodeIfPresent(String.self, forKey: .marrital_status)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        caste_id = try values.decodeIfPresent(Int.self, forKey: .caste_id)
        sector_id = try values.decodeIfPresent(Int.self, forKey: .sector_id)
        mother_tongue_id = try values.decodeIfPresent(Int.self, forKey: .mother_tongue_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
        education_id = try values.decodeIfPresent(Int.self, forKey: .education_id)
        education_degree = try values.decodeIfPresent(String.self, forKey: .education_degree)
        phone_primary = try values.decodeIfPresent(String.self, forKey: .phone_primary)
        phone_secondary = try values.decodeIfPresent(String.self, forKey: .phone_secondary)
        email_address = try values.decodeIfPresent(String.self, forKey: .email_address)
        facebook_link = try values.decodeIfPresent(String.self, forKey: .facebook_link)
        linkedin_link = try values.decodeIfPresent(String.self, forKey: .linkedin_link)
        instagram_link = try values.decodeIfPresent(String.self, forKey: .instagram_link)
        about = try values.decodeIfPresent(String.self, forKey: .about)
        extra = try values.decodeIfPresent(String.self, forKey: .extra)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        relegion_name = try values.decodeIfPresent(String.self, forKey: .relegion_name)
        caste_name = try values.decodeIfPresent(String.self, forKey: .caste_name)
        mother_tounge_name = try values.decodeIfPresent(String.self, forKey: .mother_tounge_name)
        sectors_name = try values.decodeIfPresent(String.self, forKey: .sectors_name)
        education_name = try values.decodeIfPresent(String.self, forKey: .education_name)
        religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
        caste = try values.decodeIfPresent(Caste.self, forKey: .caste)
        mother_tounge = try values.decodeIfPresent(Mother_tongue.self, forKey: .mother_tounge)
        sector = try values.decodeIfPresent(Sector.self, forKey: .sector)
        education = try values.decodeIfPresent(Education.self, forKey: .education)
    }
    
}


struct  LoginGet_current_addressModal : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : Int?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : CountriesList?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(Int.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(CountriesList.self, forKey: .country)
    }
    
}

struct Profile_profession : Codable {
    let id : Int?
    let user_id : Int?
    let profession_id : Int?
    let company_type : String?
    let company_type_other : String?
    let job_title : String?
    let profession_type : String?
    let monthly_income : Double?
    let company_name : String?
    let company_address : String?
    let start_date : String?
    let end_date : String?
    let currently_working : String?
    let created_at : String?
    let updated_at : String?
    let currency : String?
    let profession_name : String?
    let profession : Profession?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case profession_id = "profession_id"
        case company_type = "company_type"
        case company_type_other = "company_type_other"
        case job_title = "job_title"
        case profession_type = "profession_type"
        case monthly_income = "monthly_income"
        case company_name = "company_name"
        case company_address = "company_address"
        case start_date = "start_date"
        case end_date = "end_date"
        case currently_working = "currently_working"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case currency = "currency"
        case profession_name = "profession_name"
        case profession = "profession"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        company_type = try values.decodeIfPresent(String.self, forKey: .company_type)
        company_type_other = try values.decodeIfPresent(String.self, forKey: .company_type_other)
        job_title = try values.decodeIfPresent(String.self, forKey: .job_title)
        profession_type = try values.decodeIfPresent(String.self, forKey: .profession_type)
        monthly_income = try values.decodeIfPresent(Double.self, forKey: .monthly_income)
        company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
        company_address = try values.decodeIfPresent(String.self, forKey: .company_address)
        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
        end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
        currently_working = try values.decodeIfPresent(String.self, forKey: .currently_working)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
        profession = try values.decodeIfPresent(Profession.self, forKey: .profession)
    }
    
}

struct BlockedProfiles : Codable {
    
    let id : Int?
    let user_code : String?
    let name : String?
    let image : String?
    let address : String?
    let profession : String?
    let age : Int?
    let height_ft : Int?
    let height_inch : Int?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case image = "image"
        case address = "address"
        case profession = "profession"
        case age = "age"
        case height_ft = "height_ft"
        case height_inch = "height_inch"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        profession = try values.decodeIfPresent(String.self, forKey: .profession)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
        height_inch = try values.decodeIfPresent(Int.self, forKey: .height_inch)
        status = try values.decodeIfPresent(String.self, forKey: .status)
    }
    
}


struct CountriesList : Codable {
    let id : Int?
    let country_name : String?
    let country_short_name : String?
    let country_phone_code : String?
    let country_currency : String?
    let country_image : String?
    let country_short_desc : String?
    let slug : String?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case country_name = "country_name"
        case country_short_name = "country_short_name"
        case country_phone_code = "country_phone_code"
        case country_currency = "country_currency"
        case country_image = "country_image"
        case country_short_desc = "country_short_desc"
        case slug = "slug"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country_short_name = try values.decodeIfPresent(String.self, forKey: .country_short_name)
        country_phone_code = try values.decodeIfPresent(String.self, forKey: .country_phone_code)
        country_currency = try values.decodeIfPresent(String.self, forKey: .country_currency)
        country_image = try values.decodeIfPresent(String.self, forKey: .country_image)
        country_short_desc = try values.decodeIfPresent(String.self, forKey: .country_short_desc)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}


struct BlockedUser : Codable {
    let id : Int?
    let user_id : Int?
    let interested_in : Int?
    let remarks : String?
    let ip_address : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let interested_to_me : Interested_to_me?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case interested_in = "interested_in"
        case remarks = "remarks"
        case ip_address = "ip_address"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case interested_to_me = "interested_to_me"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        interested_in = try values.decodeIfPresent(Int.self, forKey: .interested_in)
        remarks = try values.decodeIfPresent(String.self, forKey: .remarks)
        ip_address = try values.decodeIfPresent(String.self, forKey: .ip_address)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        interested_to_me = try values.decodeIfPresent(Interested_to_me.self, forKey: .interested_to_me)
    }
    
}


struct Check_user_interest: Codable {
    let id : Int?
    let user_id : Int?
    let interested_in : Int?
    let remarks : String?
    let ip_address : String?
    let status : String?
    let created_at : String?
    let updated_at : String?

    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case interested_in = "interested_in"
        case remarks = "remarks"
        case ip_address = "ip_address"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        interested_in = try values.decodeIfPresent(Int.self, forKey: .interested_in)
        remarks = try values.decodeIfPresent(String.self, forKey: .remarks)
        ip_address = try values.decodeIfPresent(String.self, forKey: .ip_address)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}
