//
//  UnblockUserModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/12/21.
//

import Foundation

struct UnblockUserModal : Codable {
    let success : String?
    let data : UnblockData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(UnblockData.self, forKey: .data)
    }

}

struct UnblockData : Codable {
    let id : Int?
    let user_id : Int?
    let interested_in : Int?
    let remarks : String?
    let ip_address : String?
    let status : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case interested_in = "interested_in"
        case remarks = "remarks"
        case ip_address = "ip_address"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        interested_in = try values.decodeIfPresent(Int.self, forKey: .interested_in)
        remarks = try values.decodeIfPresent(String.self, forKey: .remarks)
        ip_address = try values.decodeIfPresent(String.self, forKey: .ip_address)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


