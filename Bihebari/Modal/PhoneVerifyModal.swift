//
//  PhoneVerifyModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/1/21.
//

import Foundation

struct PhoneVerifyModal : Codable {
    let success : String?
    let data : PhoneVerifyData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(PhoneVerifyData.self, forKey: .data)
    }

}

struct PhoneVerifyData : Codable {
    let encrypted_data : String?

    enum CodingKeys: String, CodingKey {

        case encrypted_data = "encrypted_data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        encrypted_data = try values.decodeIfPresent(String.self, forKey: .encrypted_data)
    }

}
