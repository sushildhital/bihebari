//
//  ReportProfileModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/18/21.
//

import Foundation

struct ReportProfileModal : Codable {
    let success : String?
    let data : ReportData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(ReportData.self, forKey: .data)
    }

}

struct ReportData : Codable {
    let to_user_id : Int?
    let by_user_id : Int?
    let reason : String?
    let updated_at : String?
    let created_at : String?
    let id : Int?

    enum CodingKeys: String, CodingKey {

        case to_user_id = "to_user_id"
        case by_user_id = "by_user_id"
        case reason = "reason"
        case updated_at = "updated_at"
        case created_at = "created_at"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        to_user_id = try values.decodeIfPresent(Int.self, forKey: .to_user_id)
        by_user_id = try values.decodeIfPresent(Int.self, forKey: .by_user_id)
        reason = try values.decodeIfPresent(String.self, forKey: .reason)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }

}
