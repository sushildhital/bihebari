//
//  MarkInterestedModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/4/21.
//

import Foundation
struct MarkInterestedModal : Codable {
    let success : String?
    let data : MarkInterestedData?
    

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(MarkInterestedData.self, forKey: .data)
    }

}

struct MarkInterestedData : Codable {
    let user_id : Int?
    let interested_in : Interested_in?
    let remarks : String?
    let ip_address : String?
    let status : String?
    let updated_at : String?
    let created_at : String?
    let id : Int?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case interested_in = "interested_in"
        case remarks = "remarks"
        case ip_address = "ip_address"
        case status = "status"
        case updated_at = "updated_at"
        case created_at = "created_at"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        interested_in = try values.decodeIfPresent(Interested_in.self, forKey: .interested_in)
        remarks = try values.decodeIfPresent(String.self, forKey: .remarks)
        ip_address = try values.decodeIfPresent(String.self, forKey: .ip_address)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }

}

struct Interested_in : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let email : String?
    let email_verified_at : String?
    let phone : String?
    let photo : String?
    let profile_for : String?
    let otp_code : String?
    let otp_time : String?
    let otp_status : String?
    let email_otp_status : String?
    let approved : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let deactivate : Int?
    let reason : String?
    let deactivate_request_date : String?
    let deleted_at : String?
    let online : String?
    let socket_id : String?
    let notification_token : String?
    let isCheckTemporary : Bool?
    let isCheckPermanent : Bool?
    let isCheckProfileGeneralDateAd : Bool?
    let isCheckProfileGeneralDateBs : Bool?
    let isCheckProfileGeneralGender : Bool?
    let isCheckProfileProfession : Bool?
    let isCheckProfileKundali : Bool?
    let profile_general : ProfileGeneral?
    let get_permanent_address : GetPermanentAddress?
    let profile_profession : ProfileProfession?
    let get_current_address : GetCurrentAddress?
    let profile_kundalini_detail : ProfileKundaliniDetail?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case phone = "phone"
        case photo = "photo"
        case profile_for = "profile_for"
        case otp_code = "otp_code"
        case otp_time = "otp_time"
        case otp_status = "otp_status"
        case email_otp_status = "email_otp_status"
        case approved = "approved"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case deactivate = "deactivate"
        case reason = "reason"
        case deactivate_request_date = "deactivate_request_date"
        case deleted_at = "deleted_at"
        case online = "online"
        case socket_id = "socket_id"
        case notification_token = "notification_token"
        case isCheckTemporary = "isCheckTemporary"
        case isCheckPermanent = "isCheckPermanent"
        case isCheckProfileGeneralDateAd = "isCheckProfileGeneralDateAd"
        case isCheckProfileGeneralDateBs = "isCheckProfileGeneralDateBs"
        case isCheckProfileGeneralGender = "isCheckProfileGeneralGender"
        case isCheckProfileProfession = "isCheckProfileProfession"
        case isCheckProfileKundali = "isCheckProfileKundali"
        case profile_general = "profile_general"
        case get_permanent_address = "get_permanent_address"
        case profile_profession = "profile_profession"
        case get_current_address = "get_current_address"
        case profile_kundalini_detail = "profile_kundalini_detail"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        profile_for = try values.decodeIfPresent(String.self, forKey: .profile_for)
        otp_code = try values.decodeIfPresent(String.self, forKey: .otp_code)
        otp_time = try values.decodeIfPresent(String.self, forKey: .otp_time)
        otp_status = try values.decodeIfPresent(String.self, forKey: .otp_status)
        email_otp_status = try values.decodeIfPresent(String.self, forKey: .email_otp_status)
        approved = try values.decodeIfPresent(String.self, forKey: .approved)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        deactivate = try values.decodeIfPresent(Int.self, forKey: .deactivate)
        reason = try values.decodeIfPresent(String.self, forKey: .reason)
        deactivate_request_date = try values.decodeIfPresent(String.self, forKey: .deactivate_request_date)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        online = try values.decodeIfPresent(String.self, forKey: .online)
        socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
        notification_token = try values.decodeIfPresent(String.self, forKey: .notification_token)
        isCheckTemporary = try values.decodeIfPresent(Bool.self, forKey: .isCheckTemporary)
        isCheckPermanent = try values.decodeIfPresent(Bool.self, forKey: .isCheckPermanent)
        isCheckProfileGeneralDateAd = try values.decodeIfPresent(Bool.self, forKey: .isCheckProfileGeneralDateAd)
        isCheckProfileGeneralDateBs = try values.decodeIfPresent(Bool.self, forKey: .isCheckProfileGeneralDateBs)
        isCheckProfileGeneralGender = try values.decodeIfPresent(Bool.self, forKey: .isCheckProfileGeneralGender)
        isCheckProfileProfession = try values.decodeIfPresent(Bool.self, forKey: .isCheckProfileProfession)
        isCheckProfileKundali = try values.decodeIfPresent(Bool.self, forKey: .isCheckProfileKundali)
        profile_general = try values.decodeIfPresent(ProfileGeneral.self, forKey: .profile_general)
        get_permanent_address = try values.decodeIfPresent(GetPermanentAddress.self, forKey: .get_permanent_address)
        profile_profession = try values.decodeIfPresent(ProfileProfession.self, forKey: .profile_profession)
        get_current_address = try values.decodeIfPresent(GetCurrentAddress.self, forKey: .get_current_address)
        profile_kundalini_detail = try values.decodeIfPresent(ProfileKundaliniDetail.self, forKey: .profile_kundalini_detail)
    }

}

struct Profile_kundalini_detail : Codable {
    let id : Int?
    let user_id : Int?
    let date_of_birth : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let time_of_birth : String?
    let birth_place : String?
    let rashi_id : Int?
    let gotra_id : Int?
    let religion_id : Int?
    let is_mangalic : String?
    let created_at : String?
    let updated_at : String?
    let rashi_name : String?
    let rashi_nepali_name : String?
    let gotra_name : String?
    let religion_name : String?
    let rashi : Rashi?
    let gotra : Gotra?
    let religion : Religion?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case date_of_birth = "date_of_birth"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case time_of_birth = "time_of_birth"
        case birth_place = "birth_place"
        case rashi_id = "rashi_id"
        case gotra_id = "gotra_id"
        case religion_id = "religion_id"
        case is_mangalic = "is_mangalic"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case rashi_name = "rashi_name"
        case rashi_nepali_name = "rashi_nepali_name"
        case gotra_name = "gotra_name"
        case religion_name = "religion_name"
        case rashi = "rashi"
        case gotra = "gotra"
        case religion = "religion"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        time_of_birth = try values.decodeIfPresent(String.self, forKey: .time_of_birth)
        birth_place = try values.decodeIfPresent(String.self, forKey: .birth_place)
        rashi_id = try values.decodeIfPresent(Int.self, forKey: .rashi_id)
        gotra_id = try values.decodeIfPresent(Int.self, forKey: .gotra_id)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        is_mangalic = try values.decodeIfPresent(String.self, forKey: .is_mangalic)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        rashi_name = try values.decodeIfPresent(String.self, forKey: .rashi_name)
        rashi_nepali_name = try values.decodeIfPresent(String.self, forKey: .rashi_nepali_name)
        gotra_name = try values.decodeIfPresent(String.self, forKey: .gotra_name)
        religion_name = try values.decodeIfPresent(String.self, forKey: .religion_name)
        rashi = try values.decodeIfPresent(Rashi.self, forKey: .rashi)
        gotra = try values.decodeIfPresent(Gotra.self, forKey: .gotra)
        religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
    }

}

struct ProfileGeneral : Codable {
    let id : Int?
    let user_id : Int?
    let first_name : String?
    let middle_name : String?
    let last_name : String?
    let nick_name : String?
    let gender : String?
    let dob : String?
    let dob_bs : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let marrital_status : String?
    let religion_id : Int?
    let caste_id : Int?
    let sector_id : Int?
    let mother_tongue_id : Int?
    let profession_id : Int?
    let profession_name : String?
    let education_id : Int?
    let education_degree : String?
    let phone_primary : String?
    let phone_secondary : String?
    let email_address : String?
    let facebook_link : String?
    let linkedin_link : String?
    let instagram_link : String?
    let about : String?
    let extra : String?
    let created_at : String?
    let updated_at : String?
    let relegion_name : String?
    let caste_name : String?
    let mother_tounge_name : String?
    let sectors_name : String?
    let education_name : String?
    let religion : Religion?
    let mother_tounge : Mother_tounge?
    let caste : Caste?
    let sector : Sector?
    let education : Education?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case nick_name = "nick_name"
        case gender = "gender"
        case dob = "dob"
        case dob_bs = "dob_bs"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case marrital_status = "marrital_status"
        case religion_id = "religion_id"
        case caste_id = "caste_id"
        case sector_id = "sector_id"
        case mother_tongue_id = "mother_tongue_id"
        case profession_id = "profession_id"
        case profession_name = "profession_name"
        case education_id = "education_id"
        case education_degree = "education_degree"
        case phone_primary = "phone_primary"
        case phone_secondary = "phone_secondary"
        case email_address = "email_address"
        case facebook_link = "facebook_link"
        case linkedin_link = "linkedin_link"
        case instagram_link = "instagram_link"
        case about = "about"
        case extra = "extra"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case relegion_name = "relegion_name"
        case caste_name = "caste_name"
        case mother_tounge_name = "mother_tounge_name"
        case sectors_name = "sectors_name"
        case education_name = "education_name"
        case religion = "religion"
        case mother_tounge = "mother_tounge"
        case caste = "caste"
        case sector = "sector"
        case education = "education"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        middle_name = try values.decodeIfPresent(String.self, forKey: .middle_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        nick_name = try values.decodeIfPresent(String.self, forKey: .nick_name)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        dob_bs = try values.decodeIfPresent(String.self, forKey: .dob_bs)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        marrital_status = try values.decodeIfPresent(String.self, forKey: .marrital_status)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        caste_id = try values.decodeIfPresent(Int.self, forKey: .caste_id)
        sector_id = try values.decodeIfPresent(Int.self, forKey: .sector_id)
        mother_tongue_id = try values.decodeIfPresent(Int.self, forKey: .mother_tongue_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
        education_id = try values.decodeIfPresent(Int.self, forKey: .education_id)
        education_degree = try values.decodeIfPresent(String.self, forKey: .education_degree)
        phone_primary = try values.decodeIfPresent(String.self, forKey: .phone_primary)
        phone_secondary = try values.decodeIfPresent(String.self, forKey: .phone_secondary)
        email_address = try values.decodeIfPresent(String.self, forKey: .email_address)
        facebook_link = try values.decodeIfPresent(String.self, forKey: .facebook_link)
        linkedin_link = try values.decodeIfPresent(String.self, forKey: .linkedin_link)
        instagram_link = try values.decodeIfPresent(String.self, forKey: .instagram_link)
        about = try values.decodeIfPresent(String.self, forKey: .about)
        extra = try values.decodeIfPresent(String.self, forKey: .extra)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        relegion_name = try values.decodeIfPresent(String.self, forKey: .relegion_name)
        caste_name = try values.decodeIfPresent(String.self, forKey: .caste_name)
        mother_tounge_name = try values.decodeIfPresent(String.self, forKey: .mother_tounge_name)
        sectors_name = try values.decodeIfPresent(String.self, forKey: .sectors_name)
        education_name = try values.decodeIfPresent(String.self, forKey: .education_name)
        religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
        mother_tounge = try values.decodeIfPresent(Mother_tounge.self, forKey: .mother_tounge)
        caste = try values.decodeIfPresent(Caste.self, forKey: .caste)
        sector = try values.decodeIfPresent(Sector.self, forKey: .sector)
        education = try values.decodeIfPresent(Education.self, forKey: .education)
    }

}

struct GetPermanentAddress : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : Int?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : Country?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(Int.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(Country.self, forKey: .country)
    }

}

struct GetCurrentAddress : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : Int?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : Country?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(Int.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(Country.self, forKey: .country)
    }

}

struct ProfileKundaliniDetail : Codable {
    let id : Int?
    let user_id : Int?
    let date_of_birth : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let time_of_birth : String?
    let birth_place : String?
    let rashi_id : Int?
    let gotra_id : Int?
    let religion_id : Int?
    let is_mangalic : String?
    let created_at : String?
    let updated_at : String?
    let rashi_name : String?
    let rashi_nepali_name : String?
    let gotra_name : String?
    let religion_name : String?
    let rashi : Rashi?
    let gotra : Gotra?
    let religion : Religion?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case date_of_birth = "date_of_birth"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case time_of_birth = "time_of_birth"
        case birth_place = "birth_place"
        case rashi_id = "rashi_id"
        case gotra_id = "gotra_id"
        case religion_id = "religion_id"
        case is_mangalic = "is_mangalic"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case rashi_name = "rashi_name"
        case rashi_nepali_name = "rashi_nepali_name"
        case gotra_name = "gotra_name"
        case religion_name = "religion_name"
        case rashi = "rashi"
        case gotra = "gotra"
        case religion = "religion"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        time_of_birth = try values.decodeIfPresent(String.self, forKey: .time_of_birth)
        birth_place = try values.decodeIfPresent(String.self, forKey: .birth_place)
        rashi_id = try values.decodeIfPresent(Int.self, forKey: .rashi_id)
        gotra_id = try values.decodeIfPresent(Int.self, forKey: .gotra_id)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        is_mangalic = try values.decodeIfPresent(String.self, forKey: .is_mangalic)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        rashi_name = try values.decodeIfPresent(String.self, forKey: .rashi_name)
        rashi_nepali_name = try values.decodeIfPresent(String.self, forKey: .rashi_nepali_name)
        gotra_name = try values.decodeIfPresent(String.self, forKey: .gotra_name)
        religion_name = try values.decodeIfPresent(String.self, forKey: .religion_name)
        rashi = try values.decodeIfPresent(Rashi.self, forKey: .rashi)
        gotra = try values.decodeIfPresent(Gotra.self, forKey: .gotra)
        religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
    }

}

struct ProfileProfession : Codable {
    let id : Int?
    let user_id : Int?
    let profession_id : Int?
    let company_type : String?
    let company_type_other : String?
    let job_title : String?
    let profession_type : String?
    let monthly_income : Int?
    let company_name : String?
    let company_address : String?
    let start_date : String?
    let end_date : String?
    let currently_working : String?
    let created_at : String?
    let updated_at : String?
    let currency : String?
    let profession_name : String?
    let profession : Profession?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case profession_id = "profession_id"
        case company_type = "company_type"
        case company_type_other = "company_type_other"
        case job_title = "job_title"
        case profession_type = "profession_type"
        case monthly_income = "monthly_income"
        case company_name = "company_name"
        case company_address = "company_address"
        case start_date = "start_date"
        case end_date = "end_date"
        case currently_working = "currently_working"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case currency = "currency"
        case profession_name = "profession_name"
        case profession = "profession"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        company_type = try values.decodeIfPresent(String.self, forKey: .company_type)
        company_type_other = try values.decodeIfPresent(String.self, forKey: .company_type_other)
        job_title = try values.decodeIfPresent(String.self, forKey: .job_title)
        profession_type = try values.decodeIfPresent(String.self, forKey: .profession_type)
        monthly_income = try values.decodeIfPresent(Int.self, forKey: .monthly_income)
        company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
        company_address = try values.decodeIfPresent(String.self, forKey: .company_address)
        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
        end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
        currently_working = try values.decodeIfPresent(String.self, forKey: .currently_working)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
        profession = try values.decodeIfPresent(Profession.self, forKey: .profession)
    }

}
