//
//  EducationModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/5/21.
//

import Foundation

struct EducationModal : Codable {
    let success : String?
    let data : EducationData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(EducationData.self, forKey: .data)
    }

}

struct EducationData : Codable {
    let education_degree : [Education_degree]?
    let profession : [Professionn]?
    let company_type : [String]?
    let profession_type : [String]?
    let currency : [Currency]?

    enum CodingKeys: String, CodingKey {

        case education_degree = "education_degree"
        case profession = "profession"
        case company_type = "company_type"
        case profession_type = "profession_type"
        case currency = "currency"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        education_degree = try values.decodeIfPresent([Education_degree].self, forKey: .education_degree)
        profession = try values.decodeIfPresent([Professionn].self, forKey: .profession)
        company_type = try values.decodeIfPresent([String].self, forKey: .company_type)
        profession_type = try values.decodeIfPresent([String].self, forKey: .profession_type)
        currency = try values.decodeIfPresent([Currency].self, forKey: .currency)
    }

}

struct Education_degree : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Professionn : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Currency : Codable {
    let name : String?
    let code : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case code = "code"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        code = try values.decodeIfPresent(String.self, forKey: .code)
    }

}
