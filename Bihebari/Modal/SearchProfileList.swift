
//
//  SwitchProfileList.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/24/21.
//

import Foundation

struct SearchProfileList : Codable {
    let success : String?
    let data : SearchProfileData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(SearchProfileData.self, forKey: .data)
    }

}

struct SearchProfileData : Codable {
    let userImagePath : String?
    var profileList : [SearchProfile]?
    let formSelected : SearchFormSelected?

    enum CodingKeys: String, CodingKey {

        case userImagePath = "userImagePath"
        case profileList = "profileList"
        case formSelected = "formSelected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        profileList = try values.decodeIfPresent([SearchProfile].self, forKey: .profileList)
        formSelected = try values.decodeIfPresent(SearchFormSelected.self, forKey: .formSelected)
    }

}

struct SearchProfile : Codable {
    let user_code : String?
    let profile_image : String?
    let user_id : Int?
    let age : Int?
    let address : String?
    let online_status : String?
    let profession : String?
    let height_ft : Int?
    let height_inch : Int?

    enum CodingKeys: String, CodingKey {

        case user_code = "user_code"
        case profile_image = "profile_image"
        case user_id = "user_id"
        case age = "age"
        case address = "address"
        case online_status = "online_status"
        case profession = "profession"
        case height_ft = "height_ft"
        case height_inch = "height_inch"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        online_status = try values.decodeIfPresent(String.self, forKey: .online_status)
        profession = try values.decodeIfPresent(String.self, forKey: .profession)
        height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
        height_inch = try values.decodeIfPresent(Int.self, forKey: .height_inch)
    }

}

struct SearchFormSelected : Codable {
    let gender : String?
    let age_from : String?
    let age_to : String?
    let caste : String?

    enum CodingKeys: String, CodingKey {

        case gender = "gender"
        case age_from = "age_from"
        case age_to = "age_to"
        case caste = "caste"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        age_from = try values.decodeIfPresent(String.self, forKey: .age_from)
        age_to = try values.decodeIfPresent(String.self, forKey: .age_to)
        caste = try values.decodeIfPresent(String.self, forKey: .caste)
    }

}
