//
//  AddToShortlistsModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/2/21.
//

import Foundation
struct AddToShortlistsModal : Codable {
    let success : String?
    let data : ShortlistData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(ShortlistData.self, forKey: .data)
    }

}

struct ShortlistData : Codable {
    let user_id : Int?
    let to_user_id : Int?
    let updated_at : String?
    let created_at : String?
    let id : Int?

    enum CodingKeys: String, CodingKey {

        case user_id = "user_id"
        case to_user_id = "to_user_id"
        case updated_at = "updated_at"
        case created_at = "created_at"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        to_user_id = try values.decodeIfPresent(Int.self, forKey: .to_user_id)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }

}


