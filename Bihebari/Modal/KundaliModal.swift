//
//  KundaliModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/5/21.
//

import Foundation

struct KundaliModal : Codable {
    let success : String?
    let data : KundaliData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(KundaliData.self, forKey: .data)
    }

}

struct KundaliData : Codable {
    let gotra : [Gotra]?
    let rashi : [Rashi]?
    let manglik : [Manglik]?

    enum CodingKeys: String, CodingKey {

        case gotra = "gotra"
        case rashi = "rashi"
        case manglik = "manglik"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gotra = try values.decodeIfPresent([Gotra].self, forKey: .gotra)
        rashi = try values.decodeIfPresent([Rashi].self, forKey: .rashi)
        manglik = try values.decodeIfPresent([Manglik].self, forKey: .manglik)
    }

}

struct Gotra : Codable {
    let id : Int?
       let name : String?
       let nepali_name : String?
       let ordering : Int?
       let status : String?
       let deleted_at : String?
       let created_at : String?
       let updated_at : String?
   
       enum CodingKeys: String, CodingKey {
   
           case id = "id"
           case name = "name"
           case nepali_name = "nepali_name"
           case ordering = "ordering"
           case status = "status"
           case deleted_at = "deleted_at"
           case created_at = "created_at"
           case updated_at = "updated_at"
       }
   
       init(from decoder: Decoder) throws {
           let values = try decoder.container(keyedBy: CodingKeys.self)
           id = try values.decodeIfPresent(Int.self, forKey: .id)
           name = try values.decodeIfPresent(String.self, forKey: .name)
           nepali_name = try values.decodeIfPresent(String.self, forKey: .nepali_name)
           ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
           status = try values.decodeIfPresent(String.self, forKey: .status)
           deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
           created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
           updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
       }
   
   }

struct Manglik : Codable {
    let name : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case value = "value"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        value = try values.decodeIfPresent(String.self, forKey: .value)
    }

}

struct Rashi : Codable {
    let id : Int?
     let name : String?
     let nepali_name : String?
     let ordering : Int?
     let status : String?
     let deleted_at : String?
     let created_at : String?
     let updated_at : String?
 
     enum CodingKeys: String, CodingKey {
 
         case id = "id"
         case name = "name"
         case nepali_name = "nepali_name"
         case ordering = "ordering"
         case status = "status"
         case deleted_at = "deleted_at"
         case created_at = "created_at"
         case updated_at = "updated_at"
     }
 
     init(from decoder: Decoder) throws {
         let values = try decoder.container(keyedBy: CodingKeys.self)
         id = try values.decodeIfPresent(Int.self, forKey: .id)
         name = try values.decodeIfPresent(String.self, forKey: .name)
         nepali_name = try values.decodeIfPresent(String.self, forKey: .nepali_name)
         ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
         status = try values.decodeIfPresent(String.self, forKey: .status)
         deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
         created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
         updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
     }
 
 }
