//
//  PartnerPreferencesModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/10/21.
//

import Foundation

struct PartnerPreferencesModal : Codable {
    let success : String?
    let data : PartnerData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(PartnerData.self, forKey: .data)
    }

}

struct PartnerData : Codable {
    let religions : [Partner_Religions]?
    let casts : [PartnerCasts]?
    let education_degree : [Partner_Education_degree]?
    let profession : [Partner_Profession]?
    let mother_tongue : [Mother_tongue]?
    let marital_status : [String]?
    let vegetarian_options : [Vegetarian_options]?
    let height_options : [Height_options]?

    enum CodingKeys: String, CodingKey {

        case religions = "religions"
        case casts = "casts"
        case education_degree = "education_degree"
        case profession = "profession"
        case mother_tongue = "mother_tongue"
        case marital_status = "marital_status"
        case vegetarian_options = "vegetarian_options"
        case height_options = "height_options"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        religions = try values.decodeIfPresent([Partner_Religions].self, forKey: .religions)
        casts = try values.decodeIfPresent([PartnerCasts].self, forKey: .casts)
        education_degree = try values.decodeIfPresent([Partner_Education_degree].self, forKey: .education_degree)
        profession = try values.decodeIfPresent([Partner_Profession].self, forKey: .profession)
        mother_tongue = try values.decodeIfPresent([Mother_tongue].self, forKey: .mother_tongue)
        marital_status = try values.decodeIfPresent([String].self, forKey: .marital_status)
        vegetarian_options = try values.decodeIfPresent([Vegetarian_options].self, forKey: .vegetarian_options)
        height_options = try values.decodeIfPresent([Height_options].self, forKey: .height_options)
    }

}

struct PartnerCasts : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Partner_Education_degree : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Height_options : Codable {
    let name : String?
    let value : Int?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case value = "value"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        value = try values.decodeIfPresent(Int.self, forKey: .value)
    }

}

struct Mother_tongue : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Partner_Profession : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Partner_Religions : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Vegetarian_options : Codable {
    let name : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case value = "value"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        value = try values.decodeIfPresent(String.self, forKey: .value)
    }

}

