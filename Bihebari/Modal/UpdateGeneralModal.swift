//
//  UpdateGeneralModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/6/21.
//

import Foundation

struct UpdateGeneralModal : Codable {
    let success : String?
    let data : [String]?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent([String].self, forKey: .data)
    }

}
