//
//  GetShortlistModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/3/21.
//

import Foundation

struct GetShortlistModal : Codable {
    let success : String?
    let data : [GetShortlistData]?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent([GetShortlistData].self, forKey: .data)
    }

}

struct GetShortlistData : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let image : String?
    let address : String?
    let profession : String?
    let age : Int?
    let height_ft : Int?
    let height_inch : Int?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case image = "image"
        case address = "address"
        case profession = "profession"
        case age = "age"
        case height_ft = "height_ft"
        case height_inch = "height_inch"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        profession = try values.decodeIfPresent(String.self, forKey: .profession)
        age = try values.decodeIfPresent(Int.self, forKey: .age)
        height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
        height_inch = try values.decodeIfPresent(Int.self, forKey: .height_inch)
    }

}
