//
//  UpgradePackageModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/31/21.
//

import Foundation

struct UpgradePackageModal : Codable {
    let success : String?
    let data : UpgradePackageData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(UpgradePackageData.self, forKey: .data)
    }

}

struct UpgradePackageData : Codable {
    let packages : [Packages]?

    enum CodingKeys: String, CodingKey {

        case packages = "packages"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        packages = try values.decodeIfPresent([Packages].self, forKey: .packages)
    }

}

struct Packages : Codable {
    let id : Int?
    let name : String?
    let order : Int?
    let status : Int?
    let created_at : String?
    let updated_at : String?
    let number_of_months : Int?
    let amount : Int?
    let features : [Features]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case order = "order"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case number_of_months = "number_of_months"
        case amount = "amount"
        case features = "features"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        order = try values.decodeIfPresent(Int.self, forKey: .order)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        number_of_months = try values.decodeIfPresent(Int.self, forKey: .number_of_months)
        amount = try values.decodeIfPresent(Int.self, forKey: .amount)
        features = try values.decodeIfPresent([Features].self, forKey: .features)
    }

}

struct Features : Codable {
    let id : Int?
    let package_id : Int?
    let value : String?
    let created_at : String?
    let updated_at : String?
    let color_code : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case package_id = "package_id"
        case value = "value"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case color_code = "color_code"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        package_id = try values.decodeIfPresent(Int.self, forKey: .package_id)
        value = try values.decodeIfPresent(String.self, forKey: .value)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        color_code = try values.decodeIfPresent(String.self, forKey: .color_code)
    }

}
