//
//  CurrentlyLivingModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/28/21.
//

import Foundation

struct CurrentlyLivingModal : Codable {
    let success : String?
    let data : CurrentLivingData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(CurrentLivingData.self, forKey: .data)
    }

}

struct CurrentLivingData : Codable {
    let marital_status : [String]?
    let religions : [ReligionsData]?
    let casts : [CastsData]?
    let mother_tongues : [Mother_tongues]?
    let countries : [Countries]?
    let gender : [Gender]?
    let skin : [Skin]?
    let hair_color : [Hair_color]?
    let eye_color : [Eye_color]?
    let provincesData : [ProvincesData]?

    enum CodingKeys: String, CodingKey {

        case marital_status = "marital_status"
        case religions = "religions"
        case casts = "casts"
        case mother_tongues = "mother_tongues"
        case countries = "countries"
        case gender = "gender"
        case skin = "skin"
        case hair_color = "hair_color"
        case eye_color = "eye_color"
        case provincesData = "provincesData"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        marital_status = try values.decodeIfPresent([String].self, forKey: .marital_status)
        religions = try values.decodeIfPresent([ReligionsData].self, forKey: .religions)
        casts = try values.decodeIfPresent([CastsData].self, forKey: .casts)
        mother_tongues = try values.decodeIfPresent([Mother_tongues].self, forKey: .mother_tongues)
        countries = try values.decodeIfPresent([Countries].self, forKey: .countries)
        gender = try values.decodeIfPresent([Gender].self, forKey: .gender)
        skin = try values.decodeIfPresent([Skin].self, forKey: .skin)
        hair_color = try values.decodeIfPresent([Hair_color].self, forKey: .hair_color)
        eye_color = try values.decodeIfPresent([Eye_color].self, forKey: .eye_color)
        provincesData = try values.decodeIfPresent([ProvincesData].self, forKey: .provincesData)
    }

}

struct Skin : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}


struct Gender : Codable {
    let name : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case value = "value"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        value = try values.decodeIfPresent(String.self, forKey: .value)
    }

}


struct CastsData : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}


struct Countries : Codable {
    let id : Int?
    let country_name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case country_name = "country_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
    }

}

struct Districts : Codable {
    let id : Int?
    let province_id : Int?
    let name : String?
    let area_sq_km : String?
    let website : String?
    let headquarter : String?
    let municipalities : [Municipalities]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case province_id = "province_id"
        case name = "name"
        case area_sq_km = "area_sq_km"
        case website = "website"
        case headquarter = "headquarter"
        case municipalities = "municipalities"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        province_id = try values.decodeIfPresent(Int.self, forKey: .province_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        area_sq_km = try values.decodeIfPresent(String.self, forKey: .area_sq_km)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        headquarter = try values.decodeIfPresent(String.self, forKey: .headquarter)
        municipalities = try values.decodeIfPresent([Municipalities].self, forKey: .municipalities)
    }

}

struct Mother_tongues : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

struct Municipalities : Codable {
    let id : Int?
    let district_id : Int?
    let category_id : Int?
    let name : String?
    let area_sq_km : String?
    let website : String?
    let wards : [Int]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case district_id = "district_id"
        case category_id = "category_id"
        case name = "name"
        case area_sq_km = "area_sq_km"
        case website = "website"
        case wards = "wards"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        district_id = try values.decodeIfPresent(Int.self, forKey: .district_id)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        area_sq_km = try values.decodeIfPresent(String.self, forKey: .area_sq_km)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        wards = try values.decodeIfPresent([Int].self, forKey: .wards)
    }

}

struct ProvincesData : Codable {
    let id : Int?
    let name : String?
    let area_sq_km : String?
    let website : String?
    let headquarter : String?
    let districts : [Districts]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case area_sq_km = "area_sq_km"
        case website = "website"
        case headquarter = "headquarter"
        case districts = "districts"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        area_sq_km = try values.decodeIfPresent(String.self, forKey: .area_sq_km)
        website = try values.decodeIfPresent(String.self, forKey: .website)
        headquarter = try values.decodeIfPresent(String.self, forKey: .headquarter)
        districts = try values.decodeIfPresent([Districts].self, forKey: .districts)
    }

}

struct ReligionsData : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}
