//
//  CountryModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/19/21.
//

import UIKit

struct CountryModal : Codable {
    let success : String?
    let data : [CountryData]?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent([CountryData].self, forKey: .data)
    }

}

struct CountryData : Codable {
    let id : Int?
    let country_name : String?
    let country_phone_code : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case country_name = "country_name"
        case country_phone_code = "country_phone_code"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country_phone_code = try values.decodeIfPresent(String.self, forKey: .country_phone_code)
    }

}
