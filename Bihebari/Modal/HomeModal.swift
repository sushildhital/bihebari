//
//  HomeModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/20/21.
//

import Foundation
import UIKit

struct HomeModal : Codable {
    let success : String?
    var data : ProfileDataModal?
    
    enum CodingKeys: String, CodingKey {
        
        case success = "success"
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(ProfileDataModal.self, forKey: .data)
    }
    
}

struct Caste : Codable {
    let id : Int?
    let name : String?
    let ordering : Int?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case ordering = "ordering"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
}

struct Country : Codable {
    let id : Int?
    let country_name : String?
    let country_short_name : String?
    let country_phone_code : String?
    let country_currency : String?
    let country_image : String?
    let country_short_desc : String?
    let slug : String?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case country_name = "country_name"
        case country_short_name = "country_short_name"
        case country_phone_code = "country_phone_code"
        case country_currency = "country_currency"
        case country_image = "country_image"
        case country_short_desc = "country_short_desc"
        case slug = "slug"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country_short_name = try values.decodeIfPresent(String.self, forKey: .country_short_name)
        country_phone_code = try values.decodeIfPresent(String.self, forKey: .country_phone_code)
        country_currency = try values.decodeIfPresent(String.self, forKey: .country_currency)
        country_image = try values.decodeIfPresent(String.self, forKey: .country_image)
        country_short_desc = try values.decodeIfPresent(String.self, forKey: .country_short_desc)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}


struct ProfileDataModal : Codable {
    let userImagePath : String?
    var profileList : [ProfileList]?
    let formSelected : FormSelected?

    enum CodingKeys: String, CodingKey {

        case userImagePath = "userImagePath"
        case profileList = "profileList"
        case formSelected = "formSelected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        profileList = try values.decodeIfPresent([ProfileList].self, forKey: .profileList)
        formSelected = try values.decodeIfPresent(FormSelected.self, forKey: .formSelected)
    }

}

struct Education : Codable {
    
    let id : Int?
    let name : String?
    let ordering : Int?
    let status : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case ordering = "ordering"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}


struct Eye_color : Codable {
    
    let id : Int?
    let name : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }
    
}

struct FormSelected : Codable {
    let gender : String?
    let age_from : String?
    let age_to : String?
    let caste : String?
    let marital_status : String?

    enum CodingKeys: String, CodingKey {

        case gender = "gender"
        case age_from = "age_from"
        case age_to = "age_to"
        case caste = "caste"
        case marital_status = "marital_status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        age_from = try values.decodeIfPresent(String.self, forKey: .age_from)
        age_to = try values.decodeIfPresent(String.self, forKey: .age_to)
        caste = try values.decodeIfPresent(String.self, forKey: .caste)
        marital_status = try values.decodeIfPresent(String.self, forKey: .marital_status)
    }

}

struct Get_permanent_address : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : Int?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : CountriesList?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(Int.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(CountriesList.self, forKey: .country)
    }
}
    

struct Hair_color : Codable {
        let id : Int?
        let name : String?
        let status : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case name = "name"
            case status = "status"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
        
    }
    
    struct Mother_tounge : Codable {
        let id : Int?
        let name : String?
        let ordering : Int?
        let status : String?
        let deleted_at : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case name = "name"
            case ordering = "ordering"
            case status = "status"
            case deleted_at = "deleted_at"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
    }
    
    
    struct Profession : Codable {
        let id : Int?
        let name : String?
        let ordering : Int?
        let status : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case name = "name"
            case ordering = "ordering"
            case status = "status"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
    }
    
    struct Profile_figure : Codable {
        
        let id : Int?
        let user_id : Int?
        let height_ft : Int?
        let height_in : Int?
        let weight_kg : Int?
        let skin_color_id : Int?
        let eye_color_id : Int?
        let hair_color_id : Int?
        let vegetarian : String?
        let created_at : String?
        let updated_at : String?
        let skin_color_name : String?
        let hair_color_name : String?
        let eye_color_name : String?
        let skin_color : Skin_color?
        let hair_color : Hair_color?
        let eye_color : Eye_color?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case height_ft = "height_ft"
            case height_in = "height_in"
            case weight_kg = "weight_kg"
            case skin_color_id = "skin_color_id"
            case eye_color_id = "eye_color_id"
            case hair_color_id = "hair_color_id"
            case vegetarian = "vegetarian"
            case created_at = "created_at"
            case updated_at = "updated_at"
            case skin_color_name = "skin_color_name"
            case hair_color_name = "hair_color_name"
            case eye_color_name = "eye_color_name"
            case skin_color = "skin_color"
            case hair_color = "hair_color"
            case eye_color = "eye_color"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
            height_in = try values.decodeIfPresent(Int.self, forKey: .height_in)
            weight_kg = try values.decodeIfPresent(Int.self, forKey: .weight_kg)
            skin_color_id = try values.decodeIfPresent(Int.self, forKey: .skin_color_id)
            eye_color_id = try values.decodeIfPresent(Int.self, forKey: .eye_color_id)
            hair_color_id = try values.decodeIfPresent(Int.self, forKey: .hair_color_id)
            vegetarian = try values.decodeIfPresent(String.self, forKey: .vegetarian)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
            skin_color_name = try values.decodeIfPresent(String.self, forKey: .skin_color_name)
            hair_color_name = try values.decodeIfPresent(String.self, forKey: .hair_color_name)
            eye_color_name = try values.decodeIfPresent(String.self, forKey: .eye_color_name)
            skin_color = try values.decodeIfPresent(Skin_color.self, forKey: .skin_color)
            hair_color = try values.decodeIfPresent(Hair_color.self, forKey: .hair_color)
            eye_color = try values.decodeIfPresent(Eye_color.self, forKey: .eye_color)
        }
        
    }
    
    struct Profile_general : Codable {
        let id : Int?
        let user_id : Int?
        let first_name : String?
        let middle_name : String?
        let last_name : String?
        let nick_name : String?
        let gender : String?
        let dob : String?
        let current_age_year : Int?
        let current_age_month : Int?
        let current_age_day : Int?
        let marrital_status : String?
        let religion_id : Int?
        let caste_id : Int?
        let sector_id : Int?
        let mother_tongue_id : Int?
        let profession_id : Int?
        let profession_name : String?
        let education_id : Int?
        let education_degree : String?
        let phone_primary : String?
        let phone_secondary : String?
        let email_address : String?
        let facebook_link : String?
        let linkedin_link : String?
        let instagram_link : String?
        let about : String?
        let extra : String?
        let created_at : String?
        let updated_at : String?
        let relegion_name : String?
        let caste_name : String?
        let mother_tounge_name : String?
        let sector_name : String?
        let education_name : String?
        let profession : Profession?
        let religion : Religion?
        let caste : Caste?
        let mother_tounge : Mother_tounge?
        let sector : Sector?
        let education : Education?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case first_name = "first_name"
            case middle_name = "middle_name"
            case last_name = "last_name"
            case nick_name = "nick_name"
            case gender = "gender"
            case dob = "dob"
            case current_age_year = "current_age_year"
            case current_age_month = "current_age_month"
            case current_age_day = "current_age_day"
            case marrital_status = "marrital_status"
            case religion_id = "religion_id"
            case caste_id = "caste_id"
            case sector_id = "sector_id"
            case mother_tongue_id = "mother_tongue_id"
            case profession_id = "profession_id"
            case profession_name = "profession_name"
            case education_id = "education_id"
            case education_degree = "education_degree"
            case phone_primary = "phone_primary"
            case phone_secondary = "phone_secondary"
            case email_address = "email_address"
            case facebook_link = "facebook_link"
            case linkedin_link = "linkedin_link"
            case instagram_link = "instagram_link"
            case about = "about"
            case extra = "extra"
            case created_at = "created_at"
            case updated_at = "updated_at"
            case relegion_name = "relegion_name"
            case caste_name = "caste_name"
            case mother_tounge_name = "mother_tounge_name"
            case sector_name = "sector_name"
            case education_name = "education_name"
            case profession = "profession"
            case religion = "religion"
            case caste = "caste"
            case mother_tounge = "mother_tounge"
            case sector = "sector"
            case education = "education"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
            middle_name = try values.decodeIfPresent(String.self, forKey: .middle_name)
            last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
            nick_name = try values.decodeIfPresent(String.self, forKey: .nick_name)
            gender = try values.decodeIfPresent(String.self, forKey: .gender)
            dob = try values.decodeIfPresent(String.self, forKey: .dob)
            current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
            current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
            current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
            marrital_status = try values.decodeIfPresent(String.self, forKey: .marrital_status)
            religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
            caste_id = try values.decodeIfPresent(Int.self, forKey: .caste_id)
            sector_id = try values.decodeIfPresent(Int.self, forKey: .sector_id)
            mother_tongue_id = try values.decodeIfPresent(Int.self, forKey: .mother_tongue_id)
            profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
            profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
            education_id = try values.decodeIfPresent(Int.self, forKey: .education_id)
            education_degree = try values.decodeIfPresent(String.self, forKey: .education_degree)
            phone_primary = try values.decodeIfPresent(String.self, forKey: .phone_primary)
            phone_secondary = try values.decodeIfPresent(String.self, forKey: .phone_secondary)
            email_address = try values.decodeIfPresent(String.self, forKey: .email_address)
            facebook_link = try values.decodeIfPresent(String.self, forKey: .facebook_link)
            linkedin_link = try values.decodeIfPresent(String.self, forKey: .linkedin_link)
            instagram_link = try values.decodeIfPresent(String.self, forKey: .instagram_link)
            about = try values.decodeIfPresent(String.self, forKey: .about)
            extra = try values.decodeIfPresent(String.self, forKey: .extra)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
            relegion_name = try values.decodeIfPresent(String.self, forKey: .relegion_name)
            caste_name = try values.decodeIfPresent(String.self, forKey: .caste_name)
            mother_tounge_name = try values.decodeIfPresent(String.self, forKey: .mother_tounge_name)
            sector_name = try values.decodeIfPresent(String.self, forKey: .sector_name)
            education_name = try values.decodeIfPresent(String.self, forKey: .education_name)
            profession = try values.decodeIfPresent(Profession.self, forKey: .profession)
            religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
            caste = try values.decodeIfPresent(Caste.self, forKey: .caste)
            mother_tounge = try values.decodeIfPresent(Mother_tounge.self, forKey: .mother_tounge)
            sector = try values.decodeIfPresent(Sector.self, forKey: .sector)
            education = try values.decodeIfPresent(Education.self, forKey: .education)
        }
        
    }
    
    struct ProfileList : Codable {
        let id : Int?
        let user_code : String?
        let name : String?
        let email : String?
        let email_verified_at : String?
        let phone : String?
        let photo : String?
        let profile_for : String?
        let otp_code : String?
        let otp_time : String?
        let otp_status : String?
        let approved : String?
        let status : String?
        let created_at : String?
        let updated_at : String?
        let deactivate : Int?
        let reason : String?
        let deactivate_request_date : String?
        let deleted_at : String?
        let online : String?
        let socket_id : String?
        let get_permanent_address : Get_permanent_address?
        let profile_general : Profile_general?
        let profile_figure : Profile_figure?
        let other_image : [Other_image]?
        let story_image : [Story_image]?
        let profile_image : Profile_image?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_code = "user_code"
            case name = "name"
            case email = "email"
            case email_verified_at = "email_verified_at"
            case phone = "phone"
            case photo = "photo"
            case profile_for = "profile_for"
            case otp_code = "otp_code"
            case otp_time = "otp_time"
            case otp_status = "otp_status"
            case approved = "approved"
            case status = "status"
            case created_at = "created_at"
            case updated_at = "updated_at"
            case deactivate = "deactivate"
            case reason = "reason"
            case deactivate_request_date = "deactivate_request_date"
            case deleted_at = "deleted_at"
            case online = "online"
            case socket_id = "socket_id"
            case get_permanent_address = "get_permanent_address"
            case profile_general = "profile_general"
            case profile_figure = "profile_figure"
            case other_image = "other_image"
            case story_image = "story_image"
            case profile_image = "profile_image"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            email = try values.decodeIfPresent(String.self, forKey: .email)
            email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
            phone = try values.decodeIfPresent(String.self, forKey: .phone)
            photo = try values.decodeIfPresent(String.self, forKey: .photo)
            profile_for = try values.decodeIfPresent(String.self, forKey: .profile_for)
            otp_code = try values.decodeIfPresent(String.self, forKey: .otp_code)
            otp_time = try values.decodeIfPresent(String.self, forKey: .otp_time)
            otp_status = try values.decodeIfPresent(String.self, forKey: .otp_status)
            approved = try values.decodeIfPresent(String.self, forKey: .approved)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
            deactivate = try values.decodeIfPresent(Int.self, forKey: .deactivate)
            reason = try values.decodeIfPresent(String.self, forKey: .reason)
            deactivate_request_date = try values.decodeIfPresent(String.self, forKey: .deactivate_request_date)
            deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
            online = try values.decodeIfPresent(String.self, forKey: .online)
            socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
            get_permanent_address = try values.decodeIfPresent(Get_permanent_address.self, forKey: .get_permanent_address)
            profile_general = try values.decodeIfPresent(Profile_general.self, forKey: .profile_general)
            profile_figure = try values.decodeIfPresent(Profile_figure.self, forKey: .profile_figure)
            other_image = try values.decodeIfPresent([Other_image].self, forKey: .other_image)
            story_image = try values.decodeIfPresent([Story_image].self, forKey: .story_image)
            profile_image = try values.decodeIfPresent(Profile_image.self, forKey: .profile_image)
        }

    }
    
    struct Profile_image : Codable {
        let id : Int?
        let user_id : Int?
        let type : String?
        let photo : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case type = "type"
            case photo = "photo"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            type = try values.decodeIfPresent(String.self, forKey: .type)
            photo = try values.decodeIfPresent(String.self, forKey: .photo)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
        
    }
    
    struct LoginStory_image : Codable {
        let id : Int?
        let user_id : Int?
        let type : String?
        let photo : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case type = "type"
            case photo = "photo"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            type = try values.decodeIfPresent(String.self, forKey: .type)
            photo = try values.decodeIfPresent(String.self, forKey: .photo)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
        
    }
    
    struct Other_image : Codable {
        let id : Int?
        let user_id : Int?
        let type : String?
        let photo : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case type = "type"
            case photo = "photo"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            type = try values.decodeIfPresent(String.self, forKey: .type)
            photo = try values.decodeIfPresent(String.self, forKey: .photo)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
        
    }
    
    struct Story_image : Codable {
        let id : Int?
        let user_id : Int?
        let type : String?
        let photo : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case type = "type"
            case photo = "photo"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            type = try values.decodeIfPresent(String.self, forKey: .type)
            photo = try values.decodeIfPresent(String.self, forKey: .photo)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
        
    }
    
    
    struct Religion : Codable {
        let id : Int?
        let name : String?
        let ordering : Int?
        let status : String?
        let deleted_at : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case name = "name"
            case ordering = "ordering"
            case status = "status"
            case deleted_at = "deleted_at"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
    }
    
    
    struct Sector : Codable {
        
        let id : Int?
        let religion_id : Int?
        let name : String?
        let status : String?
        let deleted_at : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case religion_id = "religion_id"
            case name = "name"
            case status = "status"
            case deleted_at = "deleted_at"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
    }
    
    
    
    struct Skin_color : Codable {
        let id : Int?
        let name : String?
        let status : String?
        let created_at : String?
        let updated_at : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case name = "name"
            case status = "status"
            case created_at = "created_at"
            case updated_at = "updated_at"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        }
        
    }
    
    
    struct Get_current_address : Codable {
        
        let id : Int?
        let user_id : Int?
        let type : String?
        let country_id : Int?
        let state : String?
        let district : String?
        let city : String?
        let muncipality_vdc : String?
        let ward_number : String?
        let status : String?
        let created_at : String?
        let updated_at : String?
        let country_name : String?
        let country : Country?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "id"
            case user_id = "user_id"
            case type = "type"
            case country_id = "country_id"
            case state = "state"
            case district = "district"
            case city = "city"
            case muncipality_vdc = "muncipality_vdc"
            case ward_number = "ward_number"
            case status = "status"
            case created_at = "created_at"
            case updated_at = "updated_at"
            case country_name = "country_name"
            case country = "country"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
            type = try values.decodeIfPresent(String.self, forKey: .type)
            country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
            state = try values.decodeIfPresent(String.self, forKey: .state)
            district = try values.decodeIfPresent(String.self, forKey: .district)
            city = try values.decodeIfPresent(String.self, forKey: .city)
            muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
            ward_number = try values.decodeIfPresent(String.self, forKey: .ward_number)
            status = try values.decodeIfPresent(String.self, forKey: .status)
            created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
            updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
            country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
            country = try values.decodeIfPresent(Country.self, forKey: .country)
        }
        
    }

