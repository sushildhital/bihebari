//
//  AddPhotoResponseModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/7/21.
//

import Foundation

struct AddPhotoResponseModal : Codable {
    let success : String?
    let data : AddPhotoData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(AddPhotoData.self, forKey: .data)
    }

}

struct AddPhotoData : Codable {
    let userImagePath : String?
    let profileImage : Other_image?

    enum CodingKeys: String, CodingKey {

        case userImagePath = "userImagePath"
        case profileImage = "ProfileImage"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        profileImage = try values.decodeIfPresent(Other_image.self, forKey: .profileImage)
    }

}

//struct ProfileImageData : Codable {
//    let user_id : Int?
//    let type : String?
//    let photo : String?
//    let updated_at : String?
//    let created_at : String?
//    let id : Int?
//
//    enum CodingKeys: String, CodingKey {
//
//        case user_id = "user_id"
//        case type = "type"
//        case photo = "photo"
//        case updated_at = "updated_at"
//        case created_at = "created_at"
//        case id = "id"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
//        type = try values.decodeIfPresent(String.self, forKey: .type)
//        photo = try values.decodeIfPresent(String.self, forKey: .photo)
//        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
//        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
//        id = try values.decodeIfPresent(Int.self, forKey: .id)
//    }
//
//}
