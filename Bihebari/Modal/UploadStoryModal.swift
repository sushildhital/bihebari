//
//  UploadStoryModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/4/21.
//

import Foundation

struct AddStoryResponseModel: Codable {
    let userImagePath : String?
    let profileImage : LoginStory_image?
    
    enum CodingKeys: String, CodingKey {
        
        case userImagePath = "userImagePath"
        case profileImage = "ProfileImage"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        profileImage = try values.decodeIfPresent(LoginStory_image.self, forKey: .profileImage)
    }
}
