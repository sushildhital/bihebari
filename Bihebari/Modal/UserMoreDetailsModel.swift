//
//  UserMoreDetailsModel.swift
//  Bihebari
//
//  Created by Newarpunk on 5/25/21.
//

import Foundation
import ObjectMapper



// MARK:- Codable Model
struct UserMoreDetailsModel : Codable {
    let success : String?
    let data : UserMoreDetailsModelData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(UserMoreDetailsModelData.self, forKey: .data)
    }

}

struct UserMoreDetailsModelData : Codable {
    let userImagePath : String?
    let user : UserMoreDetailsModelUser?
    let redirect_to_build : Bool?

    enum CodingKeys: String, CodingKey {

        case userImagePath = "userImagePath"
        case user = "user"
        case redirect_to_build = "redirect_to_build"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userImagePath = try values.decodeIfPresent(String.self, forKey: .userImagePath)
        user = try values.decodeIfPresent(UserMoreDetailsModelUser.self, forKey: .user)
        redirect_to_build = try values.decodeIfPresent(Bool.self, forKey: .redirect_to_build)
    }

}

struct UserMoreDetailsModelUser : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let email : String?
    let email_verified_at : String?
    let phone : String?
    let photo : String?
    let profile_for : String?
    let otp_code : String?
    let otp_time : String?
    let otp_status : String?
    let email_otp_status : String?
    let approved : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let deactivate : Int?
    let reason : String?
    let deactivate_request_date : String?
    let deleted_at : String?
    let online : String?
    let socket_id : String?
    let get_current_address : UserMoreDetailsModelGet_current_address?
    let get_permanent_address : UserMoreDetailsModelGet_permanent_address?
    let profile_general : UserMoreDetailsModelProfile_general?
    let profile_profession : UserMoreDetailsModelProfile_profession?
    let profile_kundalini_detail : UserMoreDetailsModelProfile_kundalini_detail?
    let profile_figure : UserMoreDetailsModelProfile_figure?
    let account_image : String?
    let profile_image : UserMoreDetailsModelProfile_image?
    let other_image : [UserMoreDetailsModelOther_image]?
    let story_image : [UserMoreDetailsModelStory_image]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case phone = "phone"
        case photo = "photo"
        case profile_for = "profile_for"
        case otp_code = "otp_code"
        case otp_time = "otp_time"
        case otp_status = "otp_status"
        case email_otp_status = "email_otp_status"
        case approved = "approved"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case deactivate = "deactivate"
        case reason = "reason"
        case deactivate_request_date = "deactivate_request_date"
        case deleted_at = "deleted_at"
        case online = "online"
        case socket_id = "socket_id"
        case get_current_address = "get_current_address"
        case get_permanent_address = "get_permanent_address"
        case profile_general = "profile_general"
        case profile_profession = "profile_profession"
        case profile_kundalini_detail = "profile_kundalini_detail"
        case profile_figure = "profile_figure"
        case account_image = "account_image"
        case profile_image = "profile_image"
        case other_image = "other_image"
        case story_image = "story_image"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        profile_for = try values.decodeIfPresent(String.self, forKey: .profile_for)
        otp_code = try values.decodeIfPresent(String.self, forKey: .otp_code)
        otp_time = try values.decodeIfPresent(String.self, forKey: .otp_time)
        otp_status = try values.decodeIfPresent(String.self, forKey: .otp_status)
        email_otp_status = try values.decodeIfPresent(String.self, forKey: .email_otp_status)
        approved = try values.decodeIfPresent(String.self, forKey: .approved)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        deactivate = try values.decodeIfPresent(Int.self, forKey: .deactivate)
        reason = try values.decodeIfPresent(String.self, forKey: .reason)
        deactivate_request_date = try values.decodeIfPresent(String.self, forKey: .deactivate_request_date)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        online = try values.decodeIfPresent(String.self, forKey: .online)
        socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
        get_current_address = try values.decodeIfPresent(UserMoreDetailsModelGet_current_address.self, forKey: .get_current_address)
        get_permanent_address = try values.decodeIfPresent(UserMoreDetailsModelGet_permanent_address.self, forKey: .get_permanent_address)
        profile_general = try values.decodeIfPresent(UserMoreDetailsModelProfile_general.self, forKey: .profile_general)
        profile_profession = try values.decodeIfPresent(UserMoreDetailsModelProfile_profession.self, forKey: .profile_profession)
        profile_kundalini_detail = try values.decodeIfPresent(UserMoreDetailsModelProfile_kundalini_detail.self, forKey: .profile_kundalini_detail)
        profile_figure = try values.decodeIfPresent(UserMoreDetailsModelProfile_figure.self, forKey: .profile_figure)
        account_image = try values.decodeIfPresent(String.self, forKey: .account_image)
        profile_image = try values.decodeIfPresent(UserMoreDetailsModelProfile_image.self, forKey: .profile_image)
        other_image = try values.decodeIfPresent([UserMoreDetailsModelOther_image].self, forKey: .other_image)
        story_image = try values.decodeIfPresent([UserMoreDetailsModelStory_image].self, forKey: .story_image)
    }

}

struct UserMoreDetailsModelProfile_kundalini_detail : Codable {
    let id : Int?
    let user_id : Int?
    let date_of_birth : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let time_of_birth : String?
    let birth_place : String?
    let rashi_id : Int?
    let gotra_id : Int?
    let religion_id : Int?
    let is_mangalic : String?
    let created_at : String?
    let updated_at : String?
    let rashi_name : String?
    let rashi_nepali_name : String?
    let gotra_name : String?
    let religion_name : String?
    let rashi : UserMoreDetailsModelRashi?
    let gotra : UserMoreDetailsModelGotra?
    let religion : UserMoreDetailsModelReligion?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case date_of_birth = "date_of_birth"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case time_of_birth = "time_of_birth"
        case birth_place = "birth_place"
        case rashi_id = "rashi_id"
        case gotra_id = "gotra_id"
        case religion_id = "religion_id"
        case is_mangalic = "is_mangalic"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case rashi_name = "rashi_name"
        case rashi_nepali_name = "rashi_nepali_name"
        case gotra_name = "gotra_name"
        case religion_name = "religion_name"
        case rashi = "rashi"
        case gotra = "gotra"
        case religion = "religion"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        date_of_birth = try values.decodeIfPresent(String.self, forKey: .date_of_birth)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        time_of_birth = try values.decodeIfPresent(String.self, forKey: .time_of_birth)
        birth_place = try values.decodeIfPresent(String.self, forKey: .birth_place)
        rashi_id = try values.decodeIfPresent(Int.self, forKey: .rashi_id)
        gotra_id = try values.decodeIfPresent(Int.self, forKey: .gotra_id)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        is_mangalic = try values.decodeIfPresent(String.self, forKey: .is_mangalic)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        rashi_name = try values.decodeIfPresent(String.self, forKey: .rashi_name)
        rashi_nepali_name = try values.decodeIfPresent(String.self, forKey: .rashi_nepali_name)
        gotra_name = try values.decodeIfPresent(String.self, forKey: .gotra_name)
        religion_name = try values.decodeIfPresent(String.self, forKey: .religion_name)
        rashi = try values.decodeIfPresent(UserMoreDetailsModelRashi.self, forKey: .rashi)
        gotra = try values.decodeIfPresent(UserMoreDetailsModelGotra.self, forKey: .gotra)
        religion = try values.decodeIfPresent(UserMoreDetailsModelReligion.self, forKey: .religion)
    }

}

struct UserMoreDetailsModelProfile_image : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let photo : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case photo = "photo"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}

struct UserMoreDetailsModelStory_image : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let photo : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case photo = "photo"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelGet_current_address : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : Int?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : Country?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(Int.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(Country.self, forKey: .country)
    }

}


struct UserMoreDetailsModelGet_permanent_address : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let country_id : Int?
    let state : String?
    let district : String?
    let city : String?
    let muncipality_vdc : String?
    let ward_number : Int?
    let zip_code : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let country_name : String?
    let country : UserMoreDetailsModelCountry?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case country_id = "country_id"
        case state = "state"
        case district = "district"
        case city = "city"
        case muncipality_vdc = "muncipality_vdc"
        case ward_number = "ward_number"
        case zip_code = "zip_code"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case country_name = "country_name"
        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        muncipality_vdc = try values.decodeIfPresent(String.self, forKey: .muncipality_vdc)
        ward_number = try values.decodeIfPresent(Int.self, forKey: .ward_number)
        zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country = try values.decodeIfPresent(UserMoreDetailsModelCountry.self, forKey: .country)
    }

}


struct UserMoreDetailsModelProfile_general : Codable {
    let id : Int?
    let user_id : Int?
    let first_name : String?
    let middle_name : String?
    let last_name : String?
    let nick_name : String?
    let gender : String?
    let dob : String?
    let dob_bs : String?
    let current_age_year : Int?
    let current_age_month : Int?
    let current_age_day : Int?
    let marrital_status : String?
    let religion_id : Int?
    let caste_id : Int?
    let sector_id : Int?
    let mother_tongue_id : Int?
    let profession_id : Int?
    let profession_name : String?
    let education_id : Int?
    let education_degree : String?
    let phone_primary : String?
    let phone_secondary : String?
    let email_address : String?
    let facebook_link : String?
    let linkedin_link : String?
    let instagram_link : String?
    let about : String?
    let extra : String?
    let created_at : String?
    let updated_at : String?
    let relegion_name : String?
    let caste_name : String?
    let mother_tounge_name : String?
    let sectors_name : String?
    let education_name : String?
    let profession : Profession?
    let religion : Religion?
    let caste : UserMoreDetailsModelCaste?
    let mother_tounge : UserMoreDetailsModelMother_tounge?
    let sector : UserMoreDetailsModelSector?
    let education : UserMoreDetailsModelEducation?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case nick_name = "nick_name"
        case gender = "gender"
        case dob = "dob"
        case dob_bs = "dob_bs"
        case current_age_year = "current_age_year"
        case current_age_month = "current_age_month"
        case current_age_day = "current_age_day"
        case marrital_status = "marrital_status"
        case religion_id = "religion_id"
        case caste_id = "caste_id"
        case sector_id = "sector_id"
        case mother_tongue_id = "mother_tongue_id"
        case profession_id = "profession_id"
        case profession_name = "profession_name"
        case education_id = "education_id"
        case education_degree = "education_degree"
        case phone_primary = "phone_primary"
        case phone_secondary = "phone_secondary"
        case email_address = "email_address"
        case facebook_link = "facebook_link"
        case linkedin_link = "linkedin_link"
        case instagram_link = "instagram_link"
        case about = "about"
        case extra = "extra"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case relegion_name = "relegion_name"
        case caste_name = "caste_name"
        case mother_tounge_name = "mother_tounge_name"
        case sectors_name = "sectors_name"
        case education_name = "education_name"
        case profession = "profession"
        case religion = "religion"
        case caste = "caste"
        case mother_tounge = "mother_tounge"
        case sector = "sector"
        case education = "education"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        middle_name = try values.decodeIfPresent(String.self, forKey: .middle_name)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        nick_name = try values.decodeIfPresent(String.self, forKey: .nick_name)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        dob = try values.decodeIfPresent(String.self, forKey: .dob)
        dob_bs = try values.decodeIfPresent(String.self, forKey: .dob_bs)
        current_age_year = try values.decodeIfPresent(Int.self, forKey: .current_age_year)
        current_age_month = try values.decodeIfPresent(Int.self, forKey: .current_age_month)
        current_age_day = try values.decodeIfPresent(Int.self, forKey: .current_age_day)
        marrital_status = try values.decodeIfPresent(String.self, forKey: .marrital_status)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        caste_id = try values.decodeIfPresent(Int.self, forKey: .caste_id)
        sector_id = try values.decodeIfPresent(Int.self, forKey: .sector_id)
        mother_tongue_id = try values.decodeIfPresent(Int.self, forKey: .mother_tongue_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
        education_id = try values.decodeIfPresent(Int.self, forKey: .education_id)
        education_degree = try values.decodeIfPresent(String.self, forKey: .education_degree)
        phone_primary = try values.decodeIfPresent(String.self, forKey: .phone_primary)
        phone_secondary = try values.decodeIfPresent(String.self, forKey: .phone_secondary)
        email_address = try values.decodeIfPresent(String.self, forKey: .email_address)
        facebook_link = try values.decodeIfPresent(String.self, forKey: .facebook_link)
        linkedin_link = try values.decodeIfPresent(String.self, forKey: .linkedin_link)
        instagram_link = try values.decodeIfPresent(String.self, forKey: .instagram_link)
        about = try values.decodeIfPresent(String.self, forKey: .about)
        extra = try values.decodeIfPresent(String.self, forKey: .extra)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        relegion_name = try values.decodeIfPresent(String.self, forKey: .relegion_name)
        caste_name = try values.decodeIfPresent(String.self, forKey: .caste_name)
        mother_tounge_name = try values.decodeIfPresent(String.self, forKey: .mother_tounge_name)
        sectors_name = try values.decodeIfPresent(String.self, forKey: .sectors_name)
        education_name = try values.decodeIfPresent(String.self, forKey: .education_name)
        profession = try values.decodeIfPresent(Profession.self, forKey: .profession)
        religion = try values.decodeIfPresent(Religion.self, forKey: .religion)
        caste = try values.decodeIfPresent(UserMoreDetailsModelCaste.self, forKey: .caste)
        mother_tounge = try values.decodeIfPresent(UserMoreDetailsModelMother_tounge.self, forKey: .mother_tounge)
        sector = try values.decodeIfPresent(UserMoreDetailsModelSector.self, forKey: .sector)
        education = try values.decodeIfPresent(UserMoreDetailsModelEducation.self, forKey: .education)
    }

}

struct UserMoreDetailsModelProfile_profession : Codable {
    let id : Int?
    let user_id : Int?
    let profession_id : Int?
    let company_type : String?
    let company_type_other : String?
    let job_title : String?
    let profession_type : String?
    let monthly_income : Int?
    let company_name : String?
    let company_address : String?
    let start_date : String?
    let end_date : String?
    let currently_working : String?
    let created_at : String?
    let updated_at : String?
    let currency : String?
    let profession_name : String?
    let profession : Profession?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case profession_id = "profession_id"
        case company_type = "company_type"
        case company_type_other = "company_type_other"
        case job_title = "job_title"
        case profession_type = "profession_type"
        case monthly_income = "monthly_income"
        case company_name = "company_name"
        case company_address = "company_address"
        case start_date = "start_date"
        case end_date = "end_date"
        case currently_working = "currently_working"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case currency = "currency"
        case profession_name = "profession_name"
        case profession = "profession"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        profession_id = try values.decodeIfPresent(Int.self, forKey: .profession_id)
        company_type = try values.decodeIfPresent(String.self, forKey: .company_type)
        company_type_other = try values.decodeIfPresent(String.self, forKey: .company_type_other)
        job_title = try values.decodeIfPresent(String.self, forKey: .job_title)
        profession_type = try values.decodeIfPresent(String.self, forKey: .profession_type)
        monthly_income = try values.decodeIfPresent(Int.self, forKey: .monthly_income)
        company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
        company_address = try values.decodeIfPresent(String.self, forKey: .company_address)
        start_date = try values.decodeIfPresent(String.self, forKey: .start_date)
        end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
        currently_working = try values.decodeIfPresent(String.self, forKey: .currently_working)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        profession_name = try values.decodeIfPresent(String.self, forKey: .profession_name)
        profession = try values.decodeIfPresent(Profession.self, forKey: .profession)
    }

}



struct UserMoreDetailsModelProfile_figure : Codable {
    let id : Int?
    let user_id : Int?
    let height_ft : Int?
    let height_in : Int?
    let weight_kg : Int?
    let skin_color_id : Int?
    let eye_color_id : Int?
    let hair_color_id : Int?
    let vegetarian : String?
    let created_at : String?
    let updated_at : String?
    let skin_color_name : String?
    let hair_color_name : String?
    let eye_color_name : String?
    let skin_color : UserMoreDetailsModelSkin_color?
    let hair_color : UserMoreDetailsModelHair_color?
    let eye_color : UserMoreDetailsModelEye_color?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case height_ft = "height_ft"
        case height_in = "height_in"
        case weight_kg = "weight_kg"
        case skin_color_id = "skin_color_id"
        case eye_color_id = "eye_color_id"
        case hair_color_id = "hair_color_id"
        case vegetarian = "vegetarian"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case skin_color_name = "skin_color_name"
        case hair_color_name = "hair_color_name"
        case eye_color_name = "eye_color_name"
        case skin_color = "skin_color"
        case hair_color = "hair_color"
        case eye_color = "eye_color"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        height_ft = try values.decodeIfPresent(Int.self, forKey: .height_ft)
        height_in = try values.decodeIfPresent(Int.self, forKey: .height_in)
        weight_kg = try values.decodeIfPresent(Int.self, forKey: .weight_kg)
        skin_color_id = try values.decodeIfPresent(Int.self, forKey: .skin_color_id)
        eye_color_id = try values.decodeIfPresent(Int.self, forKey: .eye_color_id)
        hair_color_id = try values.decodeIfPresent(Int.self, forKey: .hair_color_id)
        vegetarian = try values.decodeIfPresent(String.self, forKey: .vegetarian)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        skin_color_name = try values.decodeIfPresent(String.self, forKey: .skin_color_name)
        hair_color_name = try values.decodeIfPresent(String.self, forKey: .hair_color_name)
        eye_color_name = try values.decodeIfPresent(String.self, forKey: .eye_color_name)
        skin_color = try values.decodeIfPresent(UserMoreDetailsModelSkin_color.self, forKey: .skin_color)
        hair_color = try values.decodeIfPresent(UserMoreDetailsModelHair_color.self, forKey: .hair_color)
        eye_color = try values.decodeIfPresent(UserMoreDetailsModelEye_color.self, forKey: .eye_color)
    }

}


struct UserMoreDetailsModelOther_image : Codable {
    let id : Int?
    let user_id : Int?
    let type : String?
    let photo : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case type = "type"
        case photo = "photo"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelRashi : Codable {
    let id : Int?
    let name : String?
    let nepali_name : String?
    let ordering : Int?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case nepali_name = "nepali_name"
        case ordering = "ordering"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        nepali_name = try values.decodeIfPresent(String.self, forKey: .nepali_name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelGotra : Codable {
    let id : Int?
    let name : String?
    let nepali_name : String?
    let ordering : Int?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case nepali_name = "nepali_name"
        case ordering = "ordering"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        nepali_name = try values.decodeIfPresent(String.self, forKey: .nepali_name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelReligion : Codable {
    let id : Int?
    let name : String?
    let ordering : Int?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case ordering = "ordering"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelCountry : Codable {
    let id : Int?
    let country_name : String?
    let country_short_name : String?
    let country_phone_code : String?
    let country_currency : String?
    let country_image : String?
    let country_short_desc : String?
    let slug : String?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case country_name = "country_name"
        case country_short_name = "country_short_name"
        case country_phone_code = "country_phone_code"
        case country_currency = "country_currency"
        case country_image = "country_image"
        case country_short_desc = "country_short_desc"
        case slug = "slug"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name)
        country_short_name = try values.decodeIfPresent(String.self, forKey: .country_short_name)
        country_phone_code = try values.decodeIfPresent(String.self, forKey: .country_phone_code)
        country_currency = try values.decodeIfPresent(String.self, forKey: .country_currency)
        country_image = try values.decodeIfPresent(String.self, forKey: .country_image)
        country_short_desc = try values.decodeIfPresent(String.self, forKey: .country_short_desc)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelCaste : Codable {
    let id : Int?
    let name : String?
    let ordering : Int?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case ordering = "ordering"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelMother_tounge : Codable {
    let id : Int?
    let name : String?
    let ordering : Int?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case ordering = "ordering"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelSector : Codable {
    let id : Int?
    let religion_id : Int?
    let name : String?
    let status : String?
    let deleted_at : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case religion_id = "religion_id"
        case name = "name"
        case status = "status"
        case deleted_at = "deleted_at"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        religion_id = try values.decodeIfPresent(Int.self, forKey: .religion_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelEducation : Codable {
    let id : Int?
    let name : String?
    let ordering : Int?
    let status : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case ordering = "ordering"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        ordering = try values.decodeIfPresent(Int.self, forKey: .ordering)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelSkin_color : Codable {
    let id : Int?
    let name : String?
    let status : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelHair_color : Codable {
    let id : Int?
    let name : String?
    let status : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}


struct UserMoreDetailsModelEye_color : Codable {
    let id : Int?
    let name : String?
    let status : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}





struct InterestedBaseModel : Codable {
    let success : String?
    let data : Data?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(Data.self, forKey: .data)
    }

}


struct InterestedDataModel : Codable {
    let id : Int?
    let user_id : Int?
    let interested_in : Int?
    let remarks : String?
    let ip_address : String?
    let status : String?
    let created_at : String?
    let updated_at : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_id = "user_id"
        case interested_in = "interested_in"
        case remarks = "remarks"
        case ip_address = "ip_address"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        interested_in = try values.decodeIfPresent(Int.self, forKey: .interested_in)
        remarks = try values.decodeIfPresent(String.self, forKey: .remarks)
        ip_address = try values.decodeIfPresent(String.self, forKey: .ip_address)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}
