//
//  OTPVerifyModal.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/2/21.
//

import Foundation

struct OTPVerifyModal : Codable {
    let success : String?
    let data : OTPData?

    enum CodingKeys: String, CodingKey {

        case success = "success"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decodeIfPresent(String.self, forKey: .success)
        data = try values.decodeIfPresent(OTPData.self, forKey: .data)
    }

}

struct OTPData : Codable {
    let id : Int?
    let user_code : String?
    let name : String?
    let email : String?
    let email_verified_at : String?
    let phone : String?
    let photo : String?
    let profile_for : String?
    let otp_code : String?
    let otp_time : String?
    let otp_status : String?
    let email_otp_status : String?
    let approved : String?
    let status : String?
    let created_at : String?
    let updated_at : String?
    let deactivate : Int?
    let reason : String?
    let deactivate_request_date : String?
    let deleted_at : String?
    let online : String?
    let socket_id : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case user_code = "user_code"
        case name = "name"
        case email = "email"
        case email_verified_at = "email_verified_at"
        case phone = "phone"
        case photo = "photo"
        case profile_for = "profile_for"
        case otp_code = "otp_code"
        case otp_time = "otp_time"
        case otp_status = "otp_status"
        case email_otp_status = "email_otp_status"
        case approved = "approved"
        case status = "status"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case deactivate = "deactivate"
        case reason = "reason"
        case deactivate_request_date = "deactivate_request_date"
        case deleted_at = "deleted_at"
        case online = "online"
        case socket_id = "socket_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_code = try values.decodeIfPresent(String.self, forKey: .user_code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
        profile_for = try values.decodeIfPresent(String.self, forKey: .profile_for)
        otp_code = try values.decodeIfPresent(String.self, forKey: .otp_code)
        otp_time = try values.decodeIfPresent(String.self, forKey: .otp_time)
        otp_status = try values.decodeIfPresent(String.self, forKey: .otp_status)
        email_otp_status = try values.decodeIfPresent(String.self, forKey: .email_otp_status)
        approved = try values.decodeIfPresent(String.self, forKey: .approved)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        deactivate = try values.decodeIfPresent(Int.self, forKey: .deactivate)
        reason = try values.decodeIfPresent(String.self, forKey: .reason)
        deactivate_request_date = try values.decodeIfPresent(String.self, forKey: .deactivate_request_date)
        deleted_at = try values.decodeIfPresent(String.self, forKey: .deleted_at)
        online = try values.decodeIfPresent(String.self, forKey: .online)
        socket_id = try values.decodeIfPresent(String.self, forKey: .socket_id)
    }

}
