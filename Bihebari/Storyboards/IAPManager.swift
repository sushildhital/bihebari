//
//  IAPManager.swift
//  Bihebari
//
//  Created by Leoan Ranjit on 11/17/21.
//

import Foundation
import StoreKit


final class IAPManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    static let shared = IAPManager()
    
    private var products = [SKProduct]()
    private var completion:  ((Int) -> Void)?
    private var productBeingPurchased : SKProduct?

    enum Product : String, CaseIterable {
        case threeMonths = "bihebari_1499_3m"
        case sixMonths = "bihebari_2499_6m"
        case oneYear = "bihebari_4499_1y"
        
        var productID: Int {
            switch self {
            case .threeMonths:
                return 6
            case .sixMonths:
                return 7
            case .oneYear:
                return 8
            default:
                break
            }
        }
    }
    
    //fetch product objects from apple
    
    public func fetchProducts() {
        let request = SKProductsRequest(productIdentifiers: Set(Product.allCases.compactMap({ $0.rawValue })))
        request.delegate = self
        request.start()
    }
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        
        print ("COunt :: \(response.products)")
        guard let product = products.first else {
            return
        }
//        purchase(product: product)
    }
    func requestDidFinish(_ request: SKRequest) {
        guard request is SKProductsRequest else {
            return
        }
        print("Product request failed.")
    }
    
    
    //prompt a product payment transaction
    
    public func purchase(product: Product, completion: @escaping ((Int) -> Void)){
        guard SKPaymentQueue.canMakePayments() else{
            return
        }
        self.completion = completion
//        productBeingPurchased = product
        guard let storeKitProduct = products.first(where: { $0.productIdentifier == product.rawValue}) else {
            return
        }
        
        let paymentRequest = SKPayment(product: storeKitProduct)
        
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(paymentRequest)
        
    }
    
    //observe the transaction state
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        transactions.forEach({
            //            guard transaction.payment.productIdentifier == self.productBeingPurchased?.productIdentifier else {
            //                continue
            //            }
            
            switch $0.transactionState {
            case .purchasing:
                SwiftMessage.showBanner(title: "Processing", message: "Initializing Transaction", type: .info)
                break
            case .purchased:
                if let product = Product(rawValue: $0.payment.productIdentifier){
                    completion?(product.productID)
                }
                SKPaymentQueue.default().finishTransaction($0)
                SKPaymentQueue.default().remove(self)
            case .restored:
                break
            case .failed:
                Spinner.stop()
                SwiftMessage.showBanner(title: "Error", message: "Transaction Failed.", type: .error)
                break
            case .deferred:
                break
            @unknown default :
                break
            }
        })
    }
    
    private func handlePurchase(_ id: String){
        //
    }
    
}
