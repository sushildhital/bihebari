//
//  GalleryCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/12/21.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgGallery: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
