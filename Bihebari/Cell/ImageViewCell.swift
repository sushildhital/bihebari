//
//  ImageViewCell.swift
//  Bihebari
//
//  Created by Leoan Ranjit on 11/9/21.
//

import UIKit

class ImageViewCell: UICollectionViewCell {

    @IBOutlet weak var imgImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
