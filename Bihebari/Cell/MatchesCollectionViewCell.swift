//
//  MatchesCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/9/21.
//

import UIKit
import IBAnimatable

enum MatchesCellAction {
    case next
    case previous
}

class MatchesCollectionViewCell: UICollectionViewCell {
    
    var btnViewProfileTapped: ((_ cell: MatchesCollectionViewCell) -> Void)?
    var btnRemoveFromShortlistTapped: ((_ cell: MatchesCollectionViewCell) -> Void)?
    var btnMarkInterestedTapped: ((_ cell: MatchesCollectionViewCell) -> Void)?
    var cellAction: ((_ action: MatchesCellAction) -> Void)?
    
    @IBOutlet weak var imgProfile: AnimatableImageView!
    @IBOutlet weak var imgActive: UIImageView!
    @IBOutlet weak var lblActiveStatus: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var imgDate: UIImageView!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var imgHeight: UIImageView!
    @IBOutlet weak var imgProfession: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var imgShortlist: UIImageView!
    @IBOutlet weak var btnShortlist: UIButton!
    @IBOutlet weak var btnRemoveFromShortlist: AnimatableButton!
    @IBOutlet weak var nameTopConstraintForShortlistVC: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnRemoveFromShortlistAction(_ sender: Any) {
        btnRemoveFromShortlistTapped?(self)
    }
    
    @IBAction func btnViewProfileAction(_ sender: Any) {
        btnViewProfileTapped?(self)
    }
    
    
    @IBAction func btnNextAction(_ sender: Any) {
        cellAction?(.next)
    }
    
    @IBAction func btnPreviousAction(_ sender: Any) {
        cellAction?(.previous)
    }
    
    @IBAction func btnMarkInterestedAction(_ sender: Any) {
        btnMarkInterestedTapped?(self)
    }
    
}
