//
//  AddStoryViewCell.swift
//  Bihebari
//
//  Created by Prakash Shahi on 04/06/2021.
//

import UIKit

class AddStoryViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgPhoto.layer.cornerRadius = 6.0
    }
    
}

