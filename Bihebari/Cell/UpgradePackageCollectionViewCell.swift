//
//  UpgradePackageCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/31/21.
//

import UIKit
import IBAnimatable

class UpgradePackageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: AnimatableView!
    @IBOutlet weak var packageView: AnimatableView!
    @IBOutlet weak var imgPackage: UIImageView!
    @IBOutlet weak var lblPackageName: UILabel!
    @IBOutlet weak var lblMonths: UILabel!
    @IBOutlet weak var lblRupees: UILabel!
    @IBOutlet weak var btnPayNow: AnimatableButton!
    @IBOutlet weak var cellUpgradePackage: UITableView!
    @IBOutlet weak var imgCheckBox: UIImageView!
    
    var btnPayNowTapped: ((_ cell: UpgradePackageCollectionViewCell) -> Void)?
    var upgradeModal: UpgradePackageModal?
    var selectedIndex = 0
    var colorList = [UIColor]()
    
    var isCheckBoxTapped: Bool = false{
        didSet{
            if isCheckBoxTapped{
                imgCheckBox.image = UIImage(named: "imgCheckBox")
            }else{
                imgCheckBox.image = UIImage(named: "imgBox")
            }
        }
    }
    
    override func awakeFromNib() {
        packageUpgrade()
        cellUpgradePackage.register(UINib(nibName: "CellUpgradePackage", bundle: nil), forCellReuseIdentifier: "CellUpgradePackage")
        cellUpgradePackage.delegate = self
        cellUpgradePackage.dataSource = self
        super.awakeFromNib()
    }
    
    func loadColor(color: [UIColor]){
        colorList = color
    }
    
    @IBAction func btnCheckBoxAction(_ sender: Any) {
        isCheckBoxTapped = !isCheckBoxTapped
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
        if let url = NSURL(string: "https://bihebari.com/payment-policy"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    @IBAction func btnPayNowAction(_ sender: Any) {
        if isCheckBoxTapped{
        
            btnPayNowTapped?(self)
        }else{
            SwiftMessage.showBanner(title: "Error", message: "You Haven't Agreed To Our Privacy Policy.", type: .error)
        }
    }
}

extension UpgradePackageCollectionViewCell: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return upgradeModal?.data?.packages?[selectedIndex].features?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellUpgradePackage", for: indexPath) as! CellUpgradePackage
        
        cell.lblFeatureName.text = upgradeModal?.data?.packages?[selectedIndex].features?[indexPath.row].value
        cell.selectionStyle = .none
        let itemselected = upgradeModal?.data?.packages?[selectedIndex].features?[indexPath.row].value
        cell.lblFeatureName.text = itemselected
        if let colorCode = upgradeModal?.data?.packages?[selectedIndex].features?[indexPath.row].color_code{
            if colorCode == "cyan"{
                cell.viewImage.backgroundColor = UIColor.init(hexString: "##44CBC6")
                
            }else if colorCode == "purple"{
                cell.viewImage.backgroundColor = UIColor.init(hexString: "#273691")
            }else{
                cell.viewImage.backgroundColor = UIColor.init(hexString: "#CC1C29")
            }
        }
        return cell
    }
}

extension UpgradePackageCollectionViewCell {
    func packageUpgrade() {
        APIClient.shared.fetch(with: APIService.packageUpgrade.request, objectType: .general) { [weak self] (result: Result<UpgradePackageModal?, APIError>) in
            switch result {
            case .success(let data):
                self?.upgradeModal = data
                self?.cellUpgradePackage.reloadData()
            case .failure(let error):
                print("Failed: \(error.localizedDescription)")
            }
        }
    }
}
