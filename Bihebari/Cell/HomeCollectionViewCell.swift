//
//  HomeCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/8/21.
//

import UIKit
import BlurKit
import SDWebImage

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgDate: UIImageView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

extension UIImage {
    func blurImage(radius: CGFloat = 10) -> UIImage? {
        let inputImage = CIImage(cgImage: (self.cgImage)!)
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(inputImage, forKey: "inputImage")
        filter?.setValue(radius, forKey: "inputRadius")
        let blurred = filter?.outputImage

        var newImageSize: CGRect = (blurred?.extent)!
        newImageSize.origin.x += (newImageSize.size.width - (self.size.width)) / 2
        newImageSize.origin.y += (newImageSize.size.height - (self.size.height)) / 2
        newImageSize.size = (self.size)

        let resultImage: CIImage = filter?.value(forKey: "outputImage") as! CIImage
        let context: CIContext = CIContext.init(options: nil)
        let cgimg: CGImage = context.createCGImage(resultImage, from: newImageSize)!
        let blurredImage: UIImage = UIImage.init(cgImage: cgimg)
        return blurredImage
    }

}


