//
//  ProfileGalleryCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/14/21.
//

import UIKit
import IBAnimatable

class ProfileGalleryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgGallery: AnimatableImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var cancelView: AnimatableView!
    @IBOutlet weak var deleteView: AnimatableView!
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var imgDelete: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnDeleteAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CellDeleteButtonTapped"), object: nil)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CellCancelButtonTapped"), object: nil)
    }
}
