//
//  SenderTableViewCell.swift
//  mDabali
//
//  Created by Prakash Shahi on 1/16/20.
//  Copyright © 2020 Prakash Shahi. All rights reserved.
//

import UIKit
import IBAnimatable

class SenderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: AnimatableImageView!
    @IBOutlet weak var messageBackground: AnimatableView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var FirstNameLabel: UILabel!
    
    @IBOutlet weak var bgView: AnimatableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        circularRounded()
    }
    
    func  circularRounded(){
        bgView.layer.cornerRadius = bgView.frame.size.width/2
        bgView.clipsToBounds = true
        bgView.layer.borderWidth = 5.0
    }
    

}
