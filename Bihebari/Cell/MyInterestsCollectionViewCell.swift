//
//  MyInterestsCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/11/21.
//

import UIKit
import IBAnimatable

enum MyInterestsCellAction {
    case next
    case previous
}

class MyInterestsCollectionViewCell: UICollectionViewCell {
    
    var btnViewProfileTapped: ((_ cell: MyInterestsCollectionViewCell) -> Void)?
    var cellAction: ((_ action: MyInterestsCellAction) -> Void)?
    
    @IBOutlet weak var imgProfileImage: AnimatableImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblRequestStatus: AnimatableLabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var imgActive: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        cellAction?(.next)
    }
    
    @IBAction func btnPreviousAction(_ sender: Any) {
        cellAction?(.previous)
    }
    
    @IBAction func btnViewProfileAction(_ sender: Any) {
        btnViewProfileTapped?(self)
    }
    
}
