//
//  CellYourPlan.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/12/21.
//

import UIKit
import IBAnimatable

class CellYourPlan: UITableViewCell {

    @IBOutlet weak var viewImage: AnimatableView!
    @IBOutlet weak var lblPlanName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
