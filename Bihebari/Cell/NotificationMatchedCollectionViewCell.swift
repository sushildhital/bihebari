//
//  NotificationMatchedCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/16/21.
//

import UIKit
import IBAnimatable

class NotificationMatchedCollectionViewCell: UICollectionViewCell {
    
    var btnViewProfileTapped: ((_ cell: NotificationMatchedCollectionViewCell) -> Void)?
    
    var accepTapped: (() -> Void)?
    var rejectTapped: (() -> Void)?

    @IBOutlet weak var profileImage: AnimatableImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var professionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnViewProfileAction(_ sender: Any) {
        btnViewProfileTapped?(self)
    }
    
    @IBAction func rejectBtnTapped(_ sender: UIButton) {
        self.rejectTapped?()
    }
    
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        self.accepTapped?()
    }
    
    func updateUI(modelData: Interested_to_me?, indexPath: IndexPath) {
        let url = Constants.BASE_URL + "/\(UserDefaultManager.shared.imagePath ?? "")"
        self.profileImage.sd_setImage(with: URL(string: url + "\(modelData?.image ?? "")"), completed: nil)
        self.nameLabel.text = modelData?.user_code
        self.addressLabel.text = modelData?.address
        self.ageLabel.text = "\(modelData?.age ?? 0) Years"
        self.heightLabel.text = "\(modelData?.height_ft ?? 0)'\(modelData?.height_inch ?? 0) ft"
        self.professionLabel.text = modelData?.profession
    }
}
