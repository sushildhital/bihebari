//
//  ReceiveTableViewCell.swift
//  mDabali
//
//  Created by Prakash Shahi on 1/16/20.
//  Copyright © 2020 Prakash Shahi. All rights reserved.
//

import UIKit
import IBAnimatable

class ReceiveTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var messageBackground: AnimatableView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
