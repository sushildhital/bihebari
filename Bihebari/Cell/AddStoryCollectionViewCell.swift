//
//  AddStoryCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/14/21.
//

import UIKit
import IBAnimatable

class AddStoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgAddStory: AnimatableImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
