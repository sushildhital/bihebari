//
//  StoriesCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 4/12/21.
//

import UIKit
import IBAnimatable

class StoriesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgStory: AnimatableImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
