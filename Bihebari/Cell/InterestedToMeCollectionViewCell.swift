//
//  InterestedToMeCollectionViewCell.swift
//  Bihebari
//
//  Created by Sushil Dhital on 6/12/21.
//

import UIKit
import IBAnimatable

enum InterestedToMeCellAction {
    case next
    case previous
}

class InterestedToMeCollectionViewCell: UICollectionViewCell {

    var cellAction: ((_ action: InterestedToMeCellAction) -> Void)?
    var delegate: ViewProfileCollectionViewActionProtocol?
    
    @IBOutlet weak var imgProfileImage: AnimatableImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblRequestStatus: AnimatableLabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var imgActive: UIImageView!
    @IBOutlet weak var btnViewProfile: UIButton!
    
    @objc func viewProfileTapped(){
        delegate?.viewProfileButtonTapped(cell: self)
    }
    
    func loadCell(interestedToMeObj: Interested_to_me){
        btnViewProfile.addTarget(self, action: #selector(self.viewProfileTapped), for: .touchUpInside)
        let url = Constants.BASE_URL +  "/\(UserDefaultManager.shared.imagePath ?? "")" + (interestedToMeObj.image ?? "")
        
        if interestedToMeObj.image == nil{
            imgProfileImage.image = UIImage(named: "imgNoImage")
        }else{
            imgProfileImage.sd_setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""), completed: nil)
        }
        
        lblProfileName.text = interestedToMeObj.user_code
        lblAge.text = "\(interestedToMeObj.age ?? 0)\(" yrs")"
        lblLocation.text = "\(interestedToMeObj.address ?? "")"
        lblProfession.text = interestedToMeObj.profession
        lblHeight.text = "\(interestedToMeObj.height_ft ?? 0)'\(interestedToMeObj.height_inch ?? 0)"
        
        if interestedToMeObj.status == "reject" {
            lblRequestStatus.text = "You Have Rejected The Request."
        }else if interestedToMeObj.status == "accept" {
            lblRequestStatus.text = "You Have Accepted The Request"
        }else if interestedToMeObj.status == "approach" {
            lblRequestStatus.text = "This User Has Sent You Matching Request."
        }else if interestedToMeObj.status == "block" {
            lblRequestStatus.text = "You Have Blocked This User."
        }else if interestedToMeObj.status == "cancel" {
            lblRequestStatus.text = "You Have Cancelled The Request."
        }else if interestedToMeObj.status == "initial" {
            lblRequestStatus.text = "This User Is Interested In You."
        }
        
        if interestedToMeObj.online == "Y"{
            imgActive.image = UIImage(named: "imgActive")
        }else if interestedToMeObj.online == "N"{
            imgActive.image = UIImage(named: "imgNotActive")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        cellAction?(.next)
    }
    
    @IBAction func btnPreviousAction(_ sender: Any) {
        cellAction?(.previous)
    }
    
}
