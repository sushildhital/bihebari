//
//  CellUpgradePackage.swift
//  Bihebari
//
//  Created by Sushil Dhital on 5/31/21.
//

import UIKit
import IBAnimatable

class CellUpgradePackage: UITableViewCell {

    @IBOutlet weak var imgImageView: AnimatableView!
    @IBOutlet weak var lblFeatureName: UILabel!
    @IBOutlet weak var viewImage: AnimatableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
